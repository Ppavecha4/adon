import * as firebase from 'firebase'
import React, { Component } from 'react'
import { AppRegistry } from 'react-native';
import {createStore} from 'redux'
import { Provider } from 'react-redux'
import store from './src/store'
import App from './src/App';
import { name as appName } from './app.json';
import PushNotificationTask from './src/tasks/pushNotificationTask'

firebase.initializeApp({
    apiKey: "AIzaSyB1Ue5hKErC29_r2ZGxB1w4cla77SwRi7o",
    authDomain: "adon-users.firebaseapp.com",
    databaseURL: "https://adon-users.firebaseio.com",
    projectId: "adon-users",
    storageBucket: "adon-users.appspot.com",
    messagingSenderId: "20332306587",
    appId: "1:20332306587:web:06a1ed57f232ce57"
})

export default class Application extends Component {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        )
    }
}

AppRegistry.registerComponent(appName, () => Application);
