package com.Fixmap;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.fixmap.services.PushNotificationService;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ServiceModule extends ReactContextBaseJavaModule {

    public static final int DEFAULT_USER_ID = -1;

    ServiceModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put("userId", DEFAULT_USER_ID);
        return constants;
    }

    @Nonnull
    @Override
    public String getName() {
        return "ServiceModule";
    }

    @ReactMethod
    public void start(int userId) {
        Intent intent = new Intent(getReactApplicationContext(), PushNotificationService.class);
        intent.putExtra("userId", userId);
        ContextCompat.startForegroundService(getReactApplicationContext(), intent);
    }

    @ReactMethod
    public void stop() {
        Intent intent = new Intent(getReactApplicationContext(), PushNotificationService.class);
        getReactApplicationContext().stopService(intent);
    }

    @ReactMethod
    public void isLocationPermissionGranted(Promise promise) {
        try {
            boolean isGranted = ContextCompat.checkSelfPermission(getReactApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getReactApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
            promise.resolve(isGranted);
        }catch (Exception err) {
            promise.reject("Permission Check Error", err);
        }
    }

    @ReactMethod
    public void requestLocationPermission() {
        if (ContextCompat.checkSelfPermission(getReactApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (getCurrentActivity() != null) {
                ActivityCompat.requestPermissions(getCurrentActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        999);
            }
        }
    }
}
