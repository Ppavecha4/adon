package com.fixmap.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.Fixmap.MainActivity;
import com.Fixmap.R;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

public class PushNotificationService extends Service {

    private static final int ID_SERVICE = 100;
    private static final int ID_REQUEST = 101;
    private Socket mSocket;

    public PushNotificationService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            mSocket = IO.socket("https://app-server-map.herokuapp.com");
            mSocket.connect();
            Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        }catch (Exception e) {
            Toast.makeText(this, "Fixmap app server issue", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int userId = intent.getIntExtra("userId", -1);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "Service");
        Notification notification = notificationBuilder
                .setContentTitle("Adon")
                .setContentText("now you can receive request from users")
                .setSmallIcon(R.drawable.fixmap)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(ID_SERVICE, notification);
        try {
            if (!mSocket.connected()) {
                mSocket.connect();
            }
            mSocket.emit("join", userId);
            mSocket.on("request", args -> showRequestNotification(args[0].toString()));
        }catch (Exception e) {
            Toast.makeText(this, "App server maintenance issue", Toast.LENGTH_LONG).show();
        }

        return Service.START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void showRequestNotification(String user) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "Push");
        Notification notification = notificationBuilder
                .setContentTitle("Fixmap")
                .setContentText("Got a new request from "+user)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.fixmap)
                .setPriority(Notification.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setContentIntent(pendingIntent)
                .build();
        NotificationManagerCompat.from(this).notify(ID_REQUEST, notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
    }
}
