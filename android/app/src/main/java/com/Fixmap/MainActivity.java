package com.Fixmap;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;


public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "Adon";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected ReactRootView createRootView() {
                return new RNGestureHandlerEnabledRootView(MainActivity.this);
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    	if (requestCode == 999) {
    		if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
	            getReactInstanceManager().getCurrentReactContext()
	                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
	                    .emit("permissionGranted", null);
	        }else {
	            Toast.makeText(this, "You have to give permission in order to run this app", Toast.LENGTH_LONG).show();
	            finish();
	        }
    	}
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
