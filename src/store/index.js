import { compose, createStore, applyMiddleware } from 'redux'
import reducers from './reducers/index'
import AsyncStorage from '@react-native-community/async-storage'
import { LOGIN_SESSION } from './actions/types'
import thunk from 'redux-thunk'

const setInit = (user, userType, local) => {
  return {
    type: LOGIN_SESSION,
    user: user,
    userType: userType,
    local
  }
}

const middlewares = [thunk]

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

let store = createStore(reducers,
  composeEnhancers(applyMiddleware(...middlewares)))

const getAsyncStorage = () => {
  return async (dispatch) => {
    const user = await AsyncStorage.getItem('user')
    const userType = await AsyncStorage.getItem('UserType')
    const local = await AsyncStorage.getItem('Local')
    dispatch(setInit(JSON.parse(user), userType, local))
  }
}

store.dispatch(getAsyncStorage())

export default store