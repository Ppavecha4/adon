import {
    FETCH_SERVICES,
    FETCH_SUB_SERVICES,
    SERVICE_FETCHING_ERROR,
    SELECT_SERVICE,
    REQUEST_ACCEPT,
    REQUEST_DECLINE,
    REQUEST_SEND,
    REQUEST_PROVIDER_NOT_FOUND,
    REQUEST_RECEIVED,
    NO_REQUEST_RECEIVED,
    REQUEST_ACCEPTED,
    REQUEST_DECLINED,
    REQUEST_COMPLETED,
    REQUEST_PRICE,
    UPDATE_WALKER_CORDS,
    REQUEST_REJECTED,
    RATING_DONE,
    UPDATE_REQUEST_ID,
    CLEAR_PENDING_REQUEST,
    PENDING_REQUEST_PROVIDER,
    UPDATE_DISANCE_AND_TIME,
    HISTORY_FETCHED,
    UPDATE_WALKERS,
    PRICE_ADDED,
    TOGGLE_SERVICE_SELECTION,
    RESERVATIONLIST
} from '../actions/types'

import { deepCloneState } from '../../utils/utils'

const initialState = {
    services: [],
    subServices: {},
    userSelectedService: null,
    error: null,
    requestStatus: REQUEST_DECLINE,
    walker: null,
    serviceStatus: 0,
    pendingRequest: null,
    histories: [],
    walkers: [],
    reserve: []
}

export default serviceReducer = (state = initialState, action) => {
    const newState = deepCloneState(state)
    switch (action.type) {
        case FETCH_SERVICES:
            newState.services = action.services
            return newState
        case FETCH_SUB_SERVICES:
            return {
                ...state,
                subServices: {
                    ...state.subServices,
                    [action.id]: action.services
                }
            }
        case SERVICE_FETCHING_ERROR:
            newState.error = action.error
            return newState
        case SELECT_SERVICE:
            newState.userSelectedService = action.id
            return newState
        case TOGGLE_SERVICE_SELECTION:
            if (newState.userSelectedService === action.id) {
                newState.userSelectedService = null
            }else {
                newState.userSelectedService = action.id
            }
            return newState
        case REQUEST_PROVIDER_NOT_FOUND:
            newState.requestStatus = REQUEST_PROVIDER_NOT_FOUND
            return newState
        case REQUEST_ACCEPT:
            newState.requestStatus = REQUEST_ACCEPT
            newState.serviceStatus = action.serviceStatus
            newState.walker = action.walker
            return newState
        case REQUEST_DECLINE:
            newState.requestStatus = REQUEST_DECLINE
            return newState
        case REQUEST_SEND:
            newState.requestStatus = REQUEST_SEND
            newState.walker = action.walker
            newState.requestId = action.requestId
            return newState
        case REQUEST_RECEIVED:
            newState.request = action.payload
            newState.requestStatus = REQUEST_RECEIVED
            return newState
        case PENDING_REQUEST_PROVIDER:
            newState.request = action.payload
            newState.request.request_id = action.requestId
            newState.requestStatus = REQUEST_ACCEPTED
            return newState
        case REQUEST_COMPLETED:
            newState.requestStatus = REQUEST_COMPLETED
            return newState
        case REQUEST_PRICE:
            newState.requestStatus = REQUEST_PRICE
            newState.servicePrice = action.price
            newState.serviceStatus = action.serviceStatus
            return newState
        case NO_REQUEST_RECEIVED:
            newState.requestStatus = NO_REQUEST_RECEIVED
            return newState
        case UPDATE_REQUEST_ID:
            newState.requestId = action.requestId
            newState.pendingRequest = action.requestId
            return newState
        case CLEAR_PENDING_REQUEST:
            newState.pendingRequest = null
            return newState
        case UPDATE_DISANCE_AND_TIME:
            newState.request.time = action.time
            newState.request.distance = action.distance
            return newState
        case REQUEST_ACCEPTED:
            newState.requestStatus = REQUEST_ACCEPTED
            return newState
        case REQUEST_DECLINED:
            newState.requestStatus = REQUEST_DECLINED
            return newState
        case REQUEST_REJECTED:
            newState.requestStatus = REQUEST_REJECTED
            newState.serviceStatus = action.serviceStatus
        case UPDATE_WALKER_CORDS:
            newState.walker = action.walker
            return newState
        case RATING_DONE:
            newState.rating_done = true
            return newState
        case HISTORY_FETCHED:
            newState.histories = action.histories
            return newState
        case RESERVATIONLIST:
            newState.reserve = action.reserve
            return newState    
        case UPDATE_WALKERS:
            newState.walkers = action.walkers
            return newState
        case PRICE_ADDED:
            newState.requestStatus = PRICE_ADDED
            return newState
        default:
            return state
    }
}
