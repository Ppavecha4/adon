
import {
    ADD_PRICE, BILL_READY
} from '../actions/types'
import { deepCloneState } from '../../utils/utils';

const initialState = {
    price: 0,
    bill: null
}

export default transationReducer = (state=initialState, action) => {
    const newState = deepCloneState(state)
    switch(action.type) {
        case ADD_PRICE:
            newState.price = action.price
        case BILL_READY:
            newState.bill = action.bill
        default:
            return newState
    }
}
