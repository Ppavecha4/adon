import { combineReducers } from 'redux'
import authReducer from './auth'
import serviceReducer from './services'
import transationReducer from './transaction'
import locationReducer from './location'
import { LOG_OUT, REQUEST_DECLINE } from '../actions/types'


const initialState = {
    auth: {
        isAuthenticated: false,
        user: null
    },
    service: {
        services: [],
        subServices: {},
        userSelectedService: null,
        error: null,
        requestStatus: REQUEST_DECLINE,
        walker: null,
        serviceStatus: 0,
        pendingRequest: null,
        histories: [],
        walkers: [],
        reserve: []
    },
    location: {
        region: null
    },
    transation: {
        price: 0,
        bill: null
    }
}
const rootReducer = combineReducers({
    auth: authReducer,
    service: serviceReducer,
    transation: transationReducer,
    location: locationReducer
})

const reducers = (state, action) => {
    if (action.type === LOG_OUT) {
        state = initialState
    }
    return rootReducer(state, action)
}

export default reducers
