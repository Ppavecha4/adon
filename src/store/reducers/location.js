import { UPDATE_CURRENT_LOCATION } from "../actions/types";
import { deepCloneState } from "../../utils/utils";

const initialState = {
    region: null
}

export default locationReducer = (state=initialState, action) => {
    const newState = deepCloneState(state)
    switch(action.type) {
        case UPDATE_CURRENT_LOCATION:
            newState.region = action.region
            return newState
        default:
            return newState
    }
}