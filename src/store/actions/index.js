import { Platform, ToastAndroid } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios from '../../Server/DefaultServer'
import { resetAction, providerAuthSuccess, providerRegistrationSuccess } from '../../utils/navigation'
import {
    LOGIN_USER,
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    LOG_OUT,
    LOGIN_SESSION,
    SET_USER_TYPE,
    UPDATE_FAILED,
    CLEAR_LOGIN_ERROR,
    UPDATE_SUCCESS
} from './types'
import { getMainServiceTypes, getAllServices } from '../../utils/utils'
import {locals} from '../../utils/locals'
import Toast from 'react-native-simple-toast'
export const loginUser = (credentials, navigation) => async (dispatch, getState) => {
    console.log(credentials)

    const local = locals[getState().auth.local].toast

    const body = {
        device_type: Platform.OS,
        device_token: 'sass',
        login_by: credentials.socialUniqueId === '' ? 'manual' : 'facebook',
    }

    if (credentials.socialUniqueId !== '') {
        body.social_unique_id = credentials.socialUniqueId.toString()
    }else {
        body.email = credentials.email
        body.password = credentials.password
    }

    const userType = getState().auth.userType

    try {
        dispatch({ type: LOGIN_USER })
        const response = await axios.post(`/${userType}/login`, body)
        if (response.data.success) {
            console.log('Login Success', response)
            try {
                await AsyncStorage.setItem('token', response.data.token)
                await AsyncStorage.setItem('user', JSON.stringify(response.data))
            } catch (err) {
                console.log('Error in token store')
            }
            dispatch(loginSuccess(response.data))
            if (response.data.is_approved > 0 && userType !== 'user') {
                navigation.dispatch(navigation.dispatch(userType === 'provider'? providerAuthSuccess : resetAction))
            }else if (response.data.is_approved <= 0) {
                ToastAndroid.show(local.adminNotApproved, ToastAndroid.LONG)
            }else {
                navigation.dispatch(navigation.dispatch(userType === 'provider'? providerAuthSuccess : resetAction))
            }
        } else {
            ToastAndroid.show(local.loginFailed, ToastAndroid.LONG)
            console.log('Error', response.data.error)
            dispatch(loginFailed(response.data.error))
            setTimeout(() => {
                dispatch({
                    type: CLEAR_LOGIN_ERROR
                })
            }, 3000)
        }

    } catch (err) {
        console.log('Login Failed', err)
        dispatch(loginFailed(err))
        setTimeout(() => {
            dispatch({
                type: CLEAR_LOGIN_ERROR
            })
        }, 3000)
    }
}

export const logOut = () => async (dispatch) => {
    await AsyncStorage.removeItem('user')
    dispatch({
        type: LOG_OUT
    })
}

export const registerUser = (credentials, navigation) => async (dispatch, getState) => {
    console.log('Registering user', credentials)
    const local = locals[getState().auth.local].toast
    const userType = getState().auth.userType
    const body = {
        first_name: credentials.firstName,
        last_name: credentials.lastName,
        email: credentials.email,
        password: credentials.socialUniqueId === '' ? credentials.password : '',
        address: credentials.address,
        phone: credentials.number,
        bio: credentials.bio,
        device_type: Platform.OS,
        device_token: 'sass',
        login_by: credentials.socialUniqueId === '' ? 'manual' : 'facebook',
        zipcode: '',
        country: '',
        state: credentials.profileImage
    }

    if (credentials.socialUniqueId !== '') {
        body.social_unique_id = credentials.socialUniqueId.toString()
    }

    if (userType === 'provider') {
        body.provider_type = credentials.providerType.toString()
        body.subscription_type = credentials.subscriptionType.toString()
        body.maintype = getMainServiceTypes(credentials.services).join(',')
        body.type = getAllServices(credentials.services).join(',')
    }

    console.log(body)

    try {
         

        dispatch({ type: LOGIN_USER })
      //  console.log("I am  here ");
        const response = await axios.post(`/${getState().auth.userType}/register`, body)

        console.log("response", response);

        if (response.data.success) {
            console.log('Register Success', response)
            try {
                await AsyncStorage.setItem('token', response.data.token)
                await AsyncStorage.setItem('user', JSON.stringify(response.data))
            } catch (err) {
                console.log('Error in token store')
            }
            dispatch(loginSuccess(response.data, local.registartionSuccess))
             console.log('Step 1')
            if (Platform.OS === 'ios' && userType === 'provider') {
                alert(local.registartionSuccess)
            }
            console.log('Step 2')
              
              navigation.dispatch(navigation.dispatch(userType === 'provider'? providerRegistrationSuccess : resetAction))

            // if(userType === 'provider'){
               
            //    console.log(" i am provider ")
            //        // navigation.dispatch(providerRegistrationSuccess)
            //       //  navigation.dispatch(navigation.dispatch(userType === 'provider'? providerRegistrationSuccess : resetAction))
            // }else{

            //      console.log("i am not provider")
            //     //   navigation.dispatch(resetAction)
                 
            // }
             if (userType === 'provider') {
               console.log('Step 3')
               ToastAndroid.show(local.registartionSuccess, ToastAndroid.LONG)
            }
            

            console.log('Step 4')
        } else {
            console.log('Error In Registration', response.data.error)
            dispatch(loginFailed(response.data.error))
        }

    } catch (err) {
        console.log('Registartion Failed e4', err)
        //dispatch(loginFailed(err))
    }
}

export const updateProfile = (credentials, navigation) => async (dispatch, getState) => {
    console.log('Update Profile', credentials)
    const local = locals[getState().auth.local].toast
    const userType = getState().auth.userType
    const { id, token } = getState().auth.user
    const body = {
        id, token,
        first_name: credentials.firstName,
        last_name: credentials.lastName,
        email: credentials.email,
        address: credentials.address,
        phone: credentials.number,
        bio: credentials.bio,
        zipcode: '',
        country: '',
        state: credentials.profileImage
    }

    if (credentials.old_password !== '') {
        body.new_password = credentials.new_password
        body.old_password = credentials.old_password
    }

    if (userType === 'provider') {
        body.maintype = getMainServiceTypes(credentials.services).join(',')
        body.type = getAllServices(credentials.services).join(',')
    }

    console.log(body)

    try {
        dispatch({ type: LOGIN_USER })
        const response = await axios.post(`/${getState().auth.userType}/update`, body)
        console.log("response",response.data)
        if (response.data.success) {
            console.log('Update Success', response)
            try {
                await AsyncStorage.setItem('token', response.data.token)
                await AsyncStorage.setItem('user', JSON.stringify(response.data))
            } catch (err) {
                console.log('Error in token store')
            }
            dispatch(loginSuccess(response.data))
            dispatch(updateSuccess())
            setTimeout(() => {
                dispatch({
                    type: CLEAR_LOGIN_ERROR
                })
            }, 5000)
            ToastAndroid.show(local.profileUpdated, ToastAndroid.LONG)
        } else {
            console.log('Error In Update Profile', response.data.error)
            ToastAndroid.show(local.profileUpdateFailed, ToastAndroid.LONG)
            dispatch(updateFailed())
            setTimeout(() => {
                dispatch({
                    type: CLEAR_LOGIN_ERROR
                })
            }, 5000)
        }

    } catch (err) {
        console.log('Registartion Failed', err)
        dispatch(loginFailed(err))
        dispatch(updateFailed())
        setTimeout(() => {
            dispatch({
                type: CLEAR_LOGIN_ERROR
            })
        }, 5000)
    }
}

export const restoreLoginSession = () => async (dispatch) => {
    console.log("i'm running here")
    let user = await AsyncStorage.getItem('user')
    user = JSON.parse(user)
    const userType = await AsyncStorage.getItem('UserType')
    dispatch({
        type: LOGIN_SESSION,
        user: user,
        userType: userType
    })
}

const loginFailed = (error) => ({
    type: LOGIN_FAILED,
    error: error
})

const loginSuccess = (user, msg) => ({
    type: LOGIN_SUCCESS,
    user: user,
    msg
})

const updateSuccess = () => ({
    type: UPDATE_SUCCESS
})

const updateFailed = () => ({
    type: UPDATE_FAILED
})

export const setUserType = (userType, nav) => async (dispatch) => {
    await AsyncStorage.setItem('UserType', userType)
    dispatch({
        type: SET_USER_TYPE,
        userType: userType
    })
    nav.navigate('Login')
}

export const reservation = (reservation_data) => async (dispatch) => {

    console.log(reservation_data);

     const rdata = {
         user_id: reservation_data.userid,
         provider_id: reservation_data.selected_provider,
         reservedate: reservation_data.datetime,
         paymethod: reservation_data.paymenType,
         amount: reservation_data.budget,
         service: reservation_data.service,
         serviceid: reservation_data.serviceid
    }

     const response = await axios.post(`/user/reservation`,rdata);
      
     if(response.data.success){
       
            Toast.show("Reservation Completed", Toast.LONG, Toast.TOP)
      
     }else{

          Toast.show("Reservation Failed", Toast.LONG, Toast.TOP)
       }    
}

 export const cancelrequest = (data) => async (dispatch, getState) => {

    const { id, token } = getState().auth.user

     const rdata = {
         cancelid: data.cancelid,
    }

     const response = await axios.post(`/user/reservationlist/cancel`,rdata)

     console.log(response)
      
     if(response.data.success){
       
            Toast.show("Reservation Cancel ", Toast.LONG, Toast.TOP)
      
     }else{

          Toast.show("Reservation Cancel Failed", Toast.LONG, Toast.TOP)
       }   
}

export const acceptrequest = (data) => async (dispatch, getState) => {

    const { id, token } = getState().auth.user

    console.log("acceptid id is =", data.acceptid);

     const rdata = {
         acceptid: data.acceptid,
    }

     const response = await axios.post(`/user/reservationlist/accept`,rdata)

     console.log(response)
      
     if(response.data.success){
       
            Toast.show("Reservation Accept ", Toast.LONG, Toast.TOP)
      
     }else{

          Toast.show("Reservation Accept Failed", Toast.LONG, Toast.TOP)
       }   
}