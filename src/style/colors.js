
export const colorPrimary = '#008577'
export const colorAccent = '#86C232'
export const colorDark = '#000000'
export const colorDarkText = '#4f4f4f'
export const colorWhite = '#ffffff'
export const colorLight  = '#f1f1f1'


// {/* <color name="colorPrimary">#008577</color>
//     <color name="colorPrimaryDark">#000000</color>
//     <color name="colorAccent">#86C232</color>

//     <color name="white">#FFFFFF</color>
//     <color name="black">#000000</color>
//     <color name="peachish">#F3D5D1</color>
//     <color name="peachishhint">#D0EED3D0</color>
//     <color name="viewColor">#E7E6E6</color> */}