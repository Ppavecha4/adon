import React, { Component } from 'react'
import {
	View,
	StyleSheet,
	Text,
	Image,
	FlatList,
	TouchableOpacity,
	ActivityIndicator,
	Animated,
	Easing,
	Modal, 
   TextInput,
   Platform,
   ScrollView,
   KeyboardAvoidingView

} from 'react-native'
import { Button } from 'native-base'
import Icon from 'react-native-ionicons'
import axios from '../Server/DefaultServer'
import { connect } from 'react-redux'
import { colorWhite, colorDarkText, colorPrimary, colorDark,colorLight } from '../style/colors'
import { fetchHistory } from '../store/actions/services'
import {
    reservation
} from '../store/actions'
import { AirbnbRating, Rating } from 'react-native-ratings'
import NestedServiceList from '../components/nestedList/NestedServiceList'
import ActionButton from '../components/buttons/ActionButton'
import { locals } from '../utils/locals'
import { NavigationActions } from 'react-navigation'
import DateTimePicker from '@react-native-community/datetimepicker'
import TextFieldIcon from '../components/input/TextFieldIcon'
import Toast from 'react-native-simple-toast'
class ProfessionalsScreen extends Component {
   
    state = {
    date: new Date('2020-06-12T14:42:42'),
    mode: 'date',
    show: false,
    showDate:'',
    paymentMethods: [
            {
                id: 1,
                title: 'CASH',
                icon: require('../../assets/cash.png')
            },
            // {
            //     id: 3,
            //     title: 'MERCADO PAGO',
            //     icon: require('../../assets/marcadopago.png')
            // },
            {
                id: 3,
                title: 'CREDIT/DEBIT CARD',
                icon: require('../../assets/credit-card.png')
            },
            {
                id: 2,
                title: 'PAYPAL',
                icon: require('../../assets/2019-06-19.png')
            }
              
        ],
         selectedSubService: null
  }

	constructor(props) {
		super(props)

		this.state = {
			professionals: [],
			isLoading: false,
			isPriceModalOpen: false,
			show: false,
			date: new Date('2020-06-12T14:42:42'),
			mode: "date",
			showDate:'',
			selected_provider: '',
			selected_provider_name: '',
			budget:'',
			payment:'',
			paymentMethods: [
            {
                id: 1,
                title: 'CASH',
                icon: require('../../assets/cash.png')
            },
            // {
            //     id: 3,
            //     title: 'MERCADO PAGO',
            //     icon: require('../../assets/marcadopago.png')
            // },
            {
                id: 3,
                title: 'CREDIT/DEBIT CARD',
                icon: require('../../assets/credit-card.png')
            },
            {
                id: 2,
                title: 'PAYPAL',
                icon: require('../../assets/2019-06-19.png')
            }
              
        ],
           priceType: 1,
           image_url:'',
           rating: 0,
           service: 'plumber',
           selectedSubService: null,
           selectedServiceid: 1
           


		}

		this.filterViewHeight = 150

		this.transformValue = new Animated.Value(0)

		this.rotateValue = this.transformValue.interpolate({
			inputRange: [-150, 0],
			outputRange: ['180deg', '0deg']
		})

		this.isFilterViewClose = false

		this.offset = 0
		this.local = locals[this.props.auth.local].userHome


		console.log("current user ID", this.props.auth)
	}

 
 setDate = (event, date) => {
    date = date || this.state.date;
 
    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      date,
      showDate: new Date(date).toLocaleString()
    });
  }
 
  show = mode => {
    this.setState({
      show: true,
      mode,
    });
  }
 
  datepicker = () => {
    this.show('date');
  }
 
  timepicker = () => {
    this.show('time');
  }


	componentDidMount() {
    console.log(this.state.lectedSubService)
	}

	toggleFilterView = () => {
		if (this.isFilterViewClose) {
			this.openFilterView()
		} else {
			this.closeFilterView()
		}
	}

	openFilterView = () => {
		console.log("Opening filter")
		Animated.spring(this.transformValue, {
			toValue: this.filterViewHeight,
			duration: 700,
			easing: Easing.inOut
		}).start(() => {
			this.isFilterViewClose = false
		})
	}

	closeFilterView = () => {
		console.log("Close filter")
		Animated.spring(this.transformValue, {
			toValue: 0,
			duration: 700,
			easing: Easing.inOut
		}).start(() => {
			this.isFilterViewClose = true
		})
	}
   
       navigateToScreen = (route) => {

       	 console.log("i am here");

    	     const navigateAction = NavigationActions.navigate({
                routeName: 'Reservation'
            })
            this.props.navigation.dispatch(navigateAction)
            return
       }
         onValueChange = (data) => {
   	
         	this.setState({
         		budget : data.budget
         	})
            
            console.log( "Enter Value is ", this.state.budget )
      }

      validateAmount = (amount) => {

        

      }
        myCallback = (dataFromChild) => {
            
             this.setState({
                    selectedSubService: dataFromChild.sub_type_name,
                    selectedServiceid: dataFromChild.id
                  })
            console.log("Service Name ===> ", dataFromChild.sub_type_name)
            console.log("Service id ===> ", dataFromChild.id)
        }

      renderReservation = () => {

       return (
       	<Modal
                animationType='slide'
                visible={ this.state.isPriceModalOpen }
                onRequestClose={() => {
                    this.setState({
                        isPriceModalOpen: false
                    })
                }}>
       	<ScrollView style={styles.box}>
       
           <View>
             <View style={styles.card}>
				<View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
					<Image style={styles.profileImg} source={{ uri: this.state.image_url ? this.state.image_url : 'https://randomuser.me/api/portraits/men/32.jpg' }} />
					<View style={{ flex: 1, paddingLeft: 10 }}>
						<Text style={styles.profileName}>{this.state.selected_provider_name}</Text>
						<Text style={styles.phone}><Icon name='ios-location' />location </Text>
					</View>
				</View>
					
				</View>
				  <TextFieldIcon
                                    
                                    icon='cash'
                                    placeholder={this.local.budgetPlaceholder}
                                    style={styles.input}
                                    value={this.state.budget}
                                    keyboardType={'numeric'}
                                    onChange={(event) => this.onValueChange({ budget: event })} />
         
          </View> 

          <View style={{
                    width: '100%',
                    height: 30,
                    backgroundColor: colorWhite,
                    justifyContent: 'center',
                    alignItems: 'center',
                    elevation: 2,
                }}>
                    <Text style={{
                        fontSize: 18,
                        letterSpacing: 1
                    }}>{this.local.choosePaymentMethod}</Text>
                </View>
               
                <View style={{
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    height: '100%',
                    flex: 1,
                    justifyContent: 'space-between'
                }}>
                    <View style={{
                        
                    }}>
                         {this.state.paymentMethods.map(method => (
                            <TouchableOpacity
                                key={method.id}
                                onPress={() => this.setState({
                                    priceType: method.id
                                })}>
                                <View
                                    style={{
                                        backgroundColor: colorWhite,
                                        elevation: 2,
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-around',
                                        paddingVertical: 0,
                                        paddingHorizontal: 0,
                                        borderRadius: 10,
                                        marginBottom: 5
                                    }}>
                                    <Image source={method.icon} style={{
                                        width: 50,
                                        height: 60,
                                        resizeMode: 'center'
                                    }} />
                                    <Text style={{
                                        flex: 1,
                                        fontSize: 18,
                                        color: colorDarkText,
                                        marginLeft: 10
                                    }}>{method.title}</Text>
                                    {this.state.priceType === method.id ? <Image source={require('../../assets/selected-icon.png')} style={{
                                        width: 30,
                                        height: 30
                                    }} /> : null}
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                    
                </View>                         

         <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%',backgroundColor: '#fff' }}>
          <TouchableOpacity  style={ styles.reserveNow }  onPress={this.datepicker}  >
              <Text style={{
                    color: colorWhite,
                    fontSize: 18,
                    letterSpacing: 1
                }}> { this.local.selectdate } </Text> 
            </TouchableOpacity>
            <TouchableOpacity  style={ styles.reserveNow }  onPress={this.timepicker}>
              <Text style={{
                    color: colorWhite,
                    fontSize: 18,
                    letterSpacing: 1
                }}> { this.local.selecttime } </Text>
            </TouchableOpacity>

           
        </View>
         <Text style={styles.datetext}> { `${this.state.showDate}` } </Text>

        { this.state.show && <DateTimePicker value={this.state.date}
                    mode={this.state.mode}
                    is24Hour={true}
                    display="default"
                    onChange={this.setDate} />
        }

       <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%', backgroundColor: '#fff' }}>
           <TouchableOpacity style={ styles.reserveNow }  onPress={() => {

                        
                 console.log(this.state.lectedSubService)

                  this.props.reservation({selected_provider: this.state.selected_provider, userid:this.props.auth.user.id, paymenType: this.state.priceType, budget: this.state.budget, datetime: this.state.date, service:this.state.selectedSubService, serviceid:this.state.selectedServiceid })
       
                  this.setState({
                    isPriceModalOpen: false,
                    priceType: 1,
                    image_url:'',
                    rating: 0,
                    selected_provider: '',
                    selected_provider_name: '',
                    budget:'',

                  })

                      }} >
                        <View style={{
                            width: '100%',
                            height: 50,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: colorDark
                        }}>
                            <Text style={{
                                fontSize: 25,
                                color: colorWhite,
                                letterSpacing: 1
                            }}> <Icon name="done-all"/></Text>
                        </View>
                    </TouchableOpacity>
               <TouchableOpacity style={ styles.reserveNow }  onPress={() => {
                        this.setState({
                            isPriceModalOpen: false,
							priceType: 1,
							image_url:'',
							rating: 0,
							selected_provider: '',
							selected_provider_name: '',
							budget:'',
                            
                        })
                        console.log('Creating Request')
                       // this.props.createRequest(this.state.region, this.state.priceType, this.socket, this.state.userLocation)
                    }}>
                        <View style={{
                            width: '100%',
                            height: 50,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: colorDark
                        }}>
                            <Text style={{
                                fontSize: 25,
                                color: colorWhite,
                                letterSpacing: 1
                            }}> <Icon name="close" /></Text>
                        </View>
                    </TouchableOpacity>     
              </View>       
          </ScrollView> 
         </Modal> 
        )
      }


	renderCard = (item, index) => {

		//console.log("Items", item);
		return (
			<View style={[styles.card, { marginBottom: index === this.props.service.walkers.length - 1 ? 40 : 10 }]}>
				<View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
					<Image style={styles.profileImg} source={{ uri: item.state ? item.state : 'https://randomuser.me/api/portraits/men/32.jpg' }} />
					<View style={{ flex: 1, paddingLeft: 10 }}>
						<Text style={styles.profileName}>{`${item.first_name} ${item.last_name} `}</Text>
						<Text style={styles.phone}><Icon name='ios-location' />location</Text>
					</View>
					 { this.props.auth.userType === 'user' && <TouchableOpacity  style={ styles.reserveNow } onPress={() => {
                              this.setState({
                                isPriceModalOpen: true,
								selected_provider: item.id,
								selected_provider_name: item.first_name + ' ' + item.last_name,
								image_url: item.state,
								rating: item.rating
						    })

                
                        }} >
              <Text style={{
                    color: colorWhite,
                    fontSize: 18,
                    letterSpacing: 1
                }}> {this.local.reserve} </Text>
            </TouchableOpacity> }
				</View>
				<AirbnbRating isDisabled defaultRating={item.rating} size={15} />

			</View>
		)
	}

	render() {
   
		return (
			<View style={styles.container}>
				<View style={styles.appBar} androidStatusBarColor={colorDarkText}>
					<View style={styles.header}>
						<TouchableOpacity onPress={() => {
							this.props.navigation.goBack()
						}}>
							<Icon name='ios-arrow-back' color={colorDarkText} />
						</TouchableOpacity>
						<Text style={styles.title}>Professionals</Text>
						<Button transparent>
							<Icon name='menu' color='transparent' />
						</Button>
					</View>
				</View>
				{this.props.service.walkers.length > 0 ? <FlatList
					data={this.props.service.walkers}
					keyExtractor={item => item.id}
					renderItem={({ item, index }) => this.renderCard(item, index)}
					onScroll={(event) => {
						const currentOffset = event.nativeEvent.contentOffset.y
						if (currentOffset > this.offset) {
							this.openFilterView()
						} else {
							this.closeFilterView()
						}
					}}
				/> : <View style={{
					flex: 1,
					justifyContent: 'center',
					alignItems: 'center',
					width: '100%',
				}}>
						<ActivityIndicator size='large' />
					</View> && this.state.isLoading}
				<Animated.View style={{
					alignItems: 'center',
					backgroundColor: colorWhite,
					borderTopLeftRadius: 20,
					borderTopRightRadius: 20,
					elevation: 5,
					paddingBottom: 10,
					position: 'absolute',
					width: '100%',
					bottom: 0,
					transform: [
						{
							translateY: this.transformValue
						}
					]
				}}>
					<TouchableOpacity onPress={() => {
						this.toggleFilterView()
					}} style={{
						width: '100%',
						paddingHorizontal: 10,
						alignItems: 'center'
					}}>
						<Animated.View style={{ transform: [{ rotate: this.rotateValue }] }}>
							<Icon name='ios-arrow-down' />
						</Animated.View>
					</TouchableOpacity>
					<NestedServiceList callbackFromParent={this.myCallback} />
				</Animated.View>
				{this.renderReservation()}
			</View>
		)
	}
}

const mapStateToProps = ({ service, auth }) => {
	return { service, auth }
}

const mapDispatchToProps = (dispatch) => {
	return {
		fetchHistory: () => dispatch(fetchHistory()),
    reservation: (data) => dispatch(reservation(data))

	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfessionalsScreen)

const styles = StyleSheet.create({
	container: {
		flex: 1,
		height: '100%',
	},
	appBar: {
		backgroundColor: colorWhite,
		paddingHorizontal: 20,
		elevation: 1,
	},
	header: {
		width: '100%',
		height: 60,
		flexDirection: 'row',
		alignItems: 'center',
		top:0
	},
	title: {
		fontSize: 16,
		fontWeight: '500',
		letterSpacing: 1,
		color: colorDarkText,
		textAlign: 'center',
		flex: 1
	},
	card: {
		marginVertical: 20,
		marginHorizontal: 10,
		backgroundColor: colorWhite,
		elevation: 2,
		borderRadius: 10,
		paddingVertical: 10,
		paddingHorizontal: 15,
		marginBottom: 10,
	},
	profileImg: {
		width: 70,
		height: 70,
		borderRadius: 35,
	},
	profileName: {
		fontSize: 25,
		color: colorDarkText
	},
	row: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingVertical: 10,
	},
	textSimple: {
		fontSize: 22,
		letterSpacing: 1
	},
	textHighlighted: {
		fontWeight: 'bold',
		fontSize: 30,
		letterSpacing: 5
	},
	reserveNow: {
            height: 50,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 15,
            backgroundColor: colorDark,
            bottom: 0,
            paddingHorizontal: 15,
            borderRadius: 0,
            marginVertical: 15,
            marginHorizontal: 1 ,
            flex:1
	},
	box:{
		paddingHorizontal: 15,
		top: 30,
		paddingVertical: 15,
		flex: 1,
		height: '100%',
	},
	datetext:{
		fontSize: 22,
		textAlign:'center',

	},
	 formContainer: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 10,
    },
    input: {
        marginVertical: 10,
    },
    providername:{
    	fontSize:25,
    	textAlign: 'center',
    	paddingHorizontal: 10,

    	
    },
    TextInputStyle: {  
    textAlign: 'center',  
    height: 40,  
    borderRadius: 10,  
    borderWidth: 2,  
    borderColor: '#009688',  
    marginBottom: 10  
}  
})
