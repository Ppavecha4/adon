import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    ScrollView,
    ToastAndroid,
    Modal,
    WebView,
    Alert,
    BackHandler
} from 'react-native'
// import { startCheckout } from 'react-native-checkout-mercadopago'
import { Header, Left, Body, Button, Right } from 'native-base'
import Icon from 'react-native-ionicons'
import { resetAction, logOutAction, providerAuthSuccess } from '../utils/navigation'
import { connect } from 'react-redux'
import { colorAccent, colorWhite, colorDarkText, colorDark } from '../style/colors'
import { locals } from '../utils/locals'
import axios from '../Server/DefaultServer'
import * as firebase from 'firebase'
import appConfig from '../../app.json'

class InvoiceScreen extends Component {

    constructor(props) {
        super(props)

        this.local = locals[this.props.auth.local].invoice
        const { navigation } = this.props
        this.request = navigation.getParam('request')
        this.requestRef = navigation.getParam('requestRef')

        this.state = {
            showPaymentModal: false,
            paymentUrl: this.request.paymentType === 2 ? 'https://www.paypal.com' : `https://www.appsontechnologies.in/o_project/adon/mercadopago/payment.php?user_id=${this.request.owner.id}&request_id=${this.request.request_id}&name=${this.request.owner.first_name}&email=${this.request.owner.email}&amount=${this.request.price}`,
            paymentStatus: -1 // -1 = Pending & 1 = Success & 0 = Failed
        }
    }

    componentDidUpdate = () => {
        BackHandler.addEventListener('hardwareBackPress', this.handleBack)
        if (this.state.paymentStatus === 1) {
            ToastAndroid.show(this.local.paymentSuccess, ToastAndroid.LONG)
            this.props.navigation.pop()
        }
    }

    componentWillUnmount = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBack)
    }

    handleBack = () => {
        console.log('Back pressed')
        return false
    }

    handleCheckOut = async () => {

        if (this.request.paymentType === 1) {
            Alert.alert(
                this.local.rating,
                this.local.areYouWannaRating,
                [
                    {
                        text: this.local.no,
                        onPress: () => {
                            this.requestComplete()
                        }
                    },
                    {
                        text: this.local.rate,
                        onPress: () => {
                            this.props.navigation.navigate('Review', {
                                onRatingDone: () => {
                                    this.requestComplete()
                                },
                                request: this.request
                            })
                        }
                    }
                ],
                {
                    cancelable: false
                }
            )

        } else if (this.request.paymentType === 2) {
            //ToastAndroid.show("Paypal Payment integration is in under development", ToastAndroid.LONG)
            this.setState({
                showPaymentModal: true
            })
        } else {
            //ToastAndroid.show("Mercadopago Payment integration is in under development", ToastAndroid.LONG)
            if (this.props.auth.userType === 'user') {
                this.setState({
                    showPaymentModal: true
                })
            }else{

                this.handleBack
            }
        }
    }

    requestComplete = async () => {
        if (this.props.auth.userType === 'user') {
            this.requestRef.update({
                isPaid: true,
                isCompleted: true
            })
            this.props.navigation.pop()
        } else {
            const result = await axios.post('/provider/cash_payment_complete', {
                request_id: this.request.request_id,
                walker_id: this.request.walker.id
            })
            console.log('Request Complete Paid', result)

            if (result.data.success) {
                this.requestRef.update({
                    isPaid: true
                })
            }

            this.requestRef.update({
                isCompleted: true
            })
            this.props.navigation.pop()
        }
    }

    handlePaymentResponse = data => {
        console.log(data)
        if (data.title === 'approved') {
            this.setState({
                showPaymentModal: false,
                paymentStatus: 1
            })
            this.requestRef.update({
                isCompleted: true,
                isPaid: true
            })
        } else if (data.title === 'cancel') {
            this.setState({
                showPaymentModal: false,
                paymentStatus: 0
            })
        } else {
            return
        }
    }

    render() {

        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.appBar} androidStatusBarColor={colorDarkText}>
                        <View style={styles.header}>
                            <Text style={styles.title}>{this.local.invoice}</Text>
                        </View>
                        <Icon name='ios-paper' style={{
                            alignSelf: 'center'
                        }} color={colorWhite} size={200} />
                    </View>
                    <View style={styles.card}>
                        <View style={styles.row}>
                            <Text style={styles.textSimple}>{this.local.serviceCharge}</Text>
                            <Text style={styles.textSimple}>0</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.textSimple}>{this.local.basePrice}</Text>
                            <Text style={styles.textSimple}>{`${this.request.price}`}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.textSimple}>{this.local.additionCharge}</Text>
                            <Text style={styles.textSimple}>0</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.textSimple}>{this.local.promocode}</Text>
                            <Text style={styles.textSimple}>XXXXXX</Text>
                        </View>
                        <View style={[styles.row, { borderBottomWidth: 0 }]}>
                            <View />
                            <View style={[styles.row, styles.last, { borderBottomWidth: 0 }]}>
                                <Text style={styles.textHighlighted}>{this.local.total} - </Text>
                                <Text style={styles.textSimple}>{`$ ${this.request.price}.00`}</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={{
                    width: '100%',
                    height: 70,
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    paddingVertical: 10,
                    position: 'absolute',
                    bottom: 0
                }}>
                    {/* <TouchableOpacity style={styles.btnCancel}>
                        <Text style={[styles.title, { color: colorDarkText }]}>{this.local.cancel}</Text>
                    </TouchableOpacity> */}
                    <TouchableOpacity onPress={this.handleCheckOut} style={styles.btnCountinue}>
                        <Text style={styles.title}>{this.local.continue} </Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    visible={this.state.showPaymentModal}
                    onRequestClose={() => {
                        ToastAndroid.show("You have to pay, Now you can't change payment type", ToastAndroid.LONG)
                        this.setState({
                            showPaymentModal: false
                        })
                    }}>

                    <WebView
                        useWebKit={true} 
                        source={{ uri: this.state.paymentUrl }}
                        onNavigationStateChange={this.handlePaymentResponse}
                    />
                </Modal>
            </View>
        )
    }
}

const mapStateToProps = ({ auth, service }) => {
    return {
        auth,
        service
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%'
    },
    appBar: {
        backgroundColor: colorDarkText,
        paddingHorizontal: 20,
    },
    header: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 18,
        fontWeight: '500',
        letterSpacing: 1,
        color: colorWhite
    },
    card: {
        marginVertical: 20,
        marginHorizontal: 10,
        backgroundColor: colorWhite,
        elevation: 5,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 15,
        marginBottom: 70,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10,
        borderBottomWidth: .5,
    },
    textSimple: {
        fontSize: 22,
        letterSpacing: 1
    },
    textHighlighted: {
        fontWeight: 'bold',
        fontSize: 30,
        letterSpacing: 5
    },
    btnCancel: {
        flex: 1,
        backgroundColor: colorWhite,
        borderWidth: 1,
        color: colorWhite,
        fontSize: 18,
        letterSpacing: 2,
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        borderRadius: 10
    },
    btnCountinue: {
        flex: 1,
        backgroundColor: colorDarkText,
        color: colorDarkText,
        fontSize: 18,
        letterSpacing: 2,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        borderRadius: 10
    }
})
