import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    FlatList,
    Modal
} from 'react-native'
import { Button } from 'native-base'
import Icon from 'react-native-ionicons'
import { connect } from 'react-redux'
import * as firebase from 'firebase'
import { colorWhite, colorDark, colorDarkText, colorPrimary, colorLight } from '../style/colors'
import { fetchHistory } from '../store/actions/services'
import ImageView from 'react-native-image-view'
import { isObjectEmpty, formateDate } from '../utils/utils'
import { locals } from '../utils/locals'
import { defaultAvatar } from '../utils/constants';

class ChatHistoryScreen extends Component {

    state = {
        users: [],
        selectedUser: {},
        currentOpendImage: [],
        isImageViewOpen: false,
        elevation: 2
    }

    constructor(props) {
        super(props)

        this.local = locals[this.props.auth.local].chat
    }

    componentDidMount = () => {
        this.userId = this.props.auth.user.id
        const userType = this.props.auth.userType
        this.databaseRef = firebase.database().ref(`chats/${userType}/${this.userId}`)
        // this.databaseRef = firebase.database().ref(`chats/user_id`)
        this.databaseRef.on('child_added', (snapshot) => {
            const user = snapshot.val()
            if (user) {
                console.log('User', user)
                this.setState(prevState => {
                    return {
                        ...prevState,
                        users: [...prevState.users, user]
                    }
                })
            }
        })
    }

    componentDidUpdate = () => {
        console.log('ChatHistory Update', this.state)
    }

    getCurrentMessages = () => {
        let messages = []
        if (!isObjectEmpty(this.state.selectedUser)) {
            messages = Object.values(this.state.selectedUser.messages)
        }
        return messages
    }

    renderCard = (user) => {
        return (
            <TouchableOpacity
                key={user.name}
                onPressIn={() => {
                    this.setState({
                        elevation: 0
                    })
                }}
                onPressOut={() => {
                    this.setState({
                        elevation: 2
                    })
                }}
                onPress={() => {
                    this.setState({
                        selectedUser: user
                    })
                }}>
                <View style={{ ...styles.card, elevation: this.state.elevation }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
                        <Image style={styles.profileImg} source={{ uri: user.avatar !== '' ? user.avatar : defaultAvatar }} />
                        <View style={{ flex: 1, paddingLeft: 10 }}>
                            <Text style={styles.profileName}>{user.name}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderChatBubble = (message) => (
        <View style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: message.author === this.userId ? 'flex-end' : 'flex-start',
            paddingVertical: 10,
            paddingHorizontal: 10,
        }}>
            <View style={{
                backgroundColor: message.author === this.userId ? colorPrimary : colorDarkText,
                maxWidth: '70%',
                paddingVertical: message.mediaType === 'image' ? 0 : 10,
                paddingHorizontal: message.mediaType === 'image' ? 0 : 15,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                borderBottomLeftRadius: message.author === this.userId ? 20 : 0,
                borderBottomRightRadius: message.author === this.userId ? 0 : 20,
                overflow: 'hidden'
            }}>
                {message.mediaType === 'image' ?
                    <TouchableOpacity onPress={() => {
                        this.setState({
                            isImageViewOpen: true,
                            currentOpendImage: [{
                                id: message.id,
                                source: {
                                    uri: message.mediaUrl
                                },
                                width: message.mediaConfig.width,
                                height: message.mediaConfig.height
                            }]
                        })
                    }}>
                        <Image
                            source={{ uri: message.mediaUrl }}
                            style={{
                                width: 200,
                                height: 200
                            }}
                        />
                    </TouchableOpacity> : null}
                {message.msg !== '' ? <Text style={{
                    color: colorWhite,
                    fontSize: 15,
                    marginBottom: 2,
                    textAlign: 'justify',
                    padding: message.mediaType === 'image' ? 10 : 0,
                    paddingBottom: 0
                }}>{message.msg}</Text> : null}
                <Text
                    style={{
                        fontSize: 12,
                        color: colorWhite,
                        alignSelf: 'flex-end',
                        padding: message.mediaType === 'image' ? 10 : 0
                    }}>{formateDate(message.time)}</Text>
            </View>
        </View>
    )

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.appBar} androidStatusBarColor={colorDarkText}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => {
                            this.props.navigation.goBack()
                        }}>
                            <Icon name='ios-arrow-back' color={colorDarkText} />
                        </TouchableOpacity>
                        <Text style={styles.title}>{this.local.chatHistory}</Text>
                        <Button transparent>
                            <Icon name='menu' color='transparent' />
                        </Button>
                    </View>
                </View>
                <FlatList
                    data={this.state.users}
                    keyExtractor={user => user.userId.toString()}
                    extraData={this.state}
                    renderItem={({ item }) => this.renderCard(item)}
                />
                <Modal
                    animationType='slide'
                    visible={!isObjectEmpty(this.state.selectedUser)}
                    onRequestClose={() => {
                        this.setState({
                            selectedUser: {}
                        })
                    }}>
                    <View style={styles.modalAppBar} androidStatusBarColor={colorDarkText}>
                        <View style={styles.modalHeader}>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}>
                                <TouchableOpacity onPress={() => {
                                    this.setState({
                                        selectedUser: {}
                                    })
                                }}>
                                    <Icon name='ios-arrow-back' color={colorDarkText} />
                                </TouchableOpacity>
                                <Image style={{
                                    width: 40,
                                    height: 40,
                                    borderRadius: 100,
                                    borderColor: colorPrimary,
                                    borderWidth: 2,
                                    marginLeft: 10,
                                }} source={{ uri: 'https://randomuser.me/api/portraits/men/32.jpg' }} />
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={{
                                    fontSize: 18,
                                    fontWeight: '500',
                                    letterSpacing: 1,
                                    color: colorDarkText,
                                    alignSelf: 'center',
                                    marginLeft: 10
                                }}>{this.state.selectedUser.name}</Text>
                            </View>
                            <View style={{ flex: 1 }} />
                        </View>
                    </View>
                    <View style={{
                        height: '100%',
                        flex: 1
                    }}>
                        <FlatList
                            inverted
                            style={{
                                flex: 1,
                                backgroundColor: colorLight,
                            }}
                            data={this.getCurrentMessages()}
                            keyExtractor={(msg) => msg.id.toString()}
                            renderItem={({ item }) => this.renderChatBubble(item)}
                        />

                        <TouchableOpacity onPress={() => {
                            this.setState({
                                selectedUser: {}
                            })
                        }}>
                            <View style={{
                                width: '100%',
                                height: 50,
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: colorDark
                            }}>
                                <Text style={{
                                    fontSize: 18,
                                    color: colorWhite,
                                    letterSpacing: 1
                                }}>{this.local.close}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <ImageView
                        images={this.state.currentOpendImage}
                        isVisible={this.state.isImageViewOpen}
                        onClose={() => this.setState({
                            isImageViewOpen: false
                        })}
                        animationType='slide' />
                </Modal>
            </View>
        )
    }
}

const mapStateToProps = ({ service, auth }) => {
    return { service, auth }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchHistory: () => dispatch(fetchHistory())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatHistoryScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%'
    },
    appBar: {
        backgroundColor: colorWhite,
        paddingHorizontal: 20,
        elevation: 1
    },
    header: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        top:30,
    },
    title: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 1,
        color: colorDarkText,
        textAlign: 'center',
        flex: 1
    },
    card: {
        marginVertical: 5,
        marginHorizontal: 10,
        backgroundColor: colorWhite,
        elevation: 2,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 15,
    },
    profileImg: {
        width: 50,
        height: 50,
        borderRadius: 100,
    },
    profileName: {
        fontSize: 20,
        color: colorDarkText
    },
    modalAppBar: {
        backgroundColor: colorWhite,
        paddingHorizontal: 20,
        elevation: 2
    },
    modalHeader: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})
