import React, { Component } from 'react'
import {
    View,
    Platform,
    ImageBackground,
    Image,
    DeviceEventEmitter
} from 'react-native'

import { resetAction, logOutAction, providerAuthSuccess, notAllowedAction } from '../utils/navigation'
import { connect } from 'react-redux'
import io from 'socket.io-client'
import { AccessToken } from 'react-native-fbsdk'
import PushNotification from '../tasks/pushNotificationTask'

class SplashScreen extends Component {

    componentDidMount = () => {
      


        DeviceEventEmitter.addListener('permissionGranted', this.onPermissionGranted)
        const socket = io('https://app-server-map.herokuapp.com')
        socket.connect()
        socket.emit('status', (status) => {
            if (status !== 0) {
                this.props.navigation.dispatch(notAllowedAction)
            }
        })
        this.navigate()

        AccessToken.getCurrentAccessToken().then(result => {
            console.log(result)
            console.log(result.dataAccessExpirationTime)
            console.log(new Date(result.dataAccessExpirationTime - Date.now()).getMonth())
            console.log(new Date(result.dataAccessExpirationTime))
        })


    }

    onPermissionGranted = () => {
        setTimeout(() => {
            if (this.props.isAuthenticated) {
                const userType = this.props.userType
                this.props.navigation.dispatch(userType === 'provider' ? providerAuthSuccess : resetAction)
            } else {
                this.props.navigation.dispatch(logOutAction)
            }
        }, 800)
    }

    navigate = async () => {
        if (Platform.OS === 'android') {
            const isGranted = await PushNotification.isLocationPermissionGranted()
            console.log('IsLocationGranted', isGranted)
            if (isGranted) {
                this.onPermissionGranted()
            }else {
                PushNotification.requestLocationPermission()
            }
        }
        else{
             console.log('IsLocationGranted ==> ')
             this.onPermissionGranted()
        }
    }

    render() {
        return (
            <ImageBackground source={require('../../assets/login_bg.png')} style={{
                resizeMode: 'contain',
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <View style={{
                    flex: 1,
                    width: '100%',
                    height: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingHorizontal: 20,
                }}>
                    <Image
                        style={{
                            width: '100%',
                            marginHorizontal: 50,
                            resizeMode: 'contain'
                        }}
                        source={require('../../assets/logo.png')} />
                </View>
            </ImageBackground>
        )
    }
}

const mapStateToProps = ({ auth }) => {
    return auth
}

export default connect(mapStateToProps)(SplashScreen)
