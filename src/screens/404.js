import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    ImageBackground,
    ScrollView,
} from 'react-native'

import { colorWhite, colorAccent, colorLight } from '../style/colors'


class NotFoundScreen extends Component {

    render() {
        return (
            <ImageBackground source={require('../../assets/login_bg.png')} style={{
                width: '100%',
                height: '100%'
            }}>
                <>
                    <ScrollView style={styles.formContainer}>
                        <View style={styles.container}>

                            <View style={styles.logoContainer}>
                            </View>
                            <View style={styles.formContainer}>
                            <Text style={styles.heading}>Not Allowed</Text>
                            </View>
                        </View>
                    </ScrollView>
                </>
            </ImageBackground>
        )
    }
}

export default NotFoundScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 10,
    },
    logoContainer: {
        position: 'relative',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        fontSize: 50,
        letterSpacing: 5,
        color: colorWhite
    },
    heading: {
        fontSize: 25,
        color: colorAccent,
        marginTop: 30,
        letterSpacing: 5,
        width: '100%',
        textAlign: 'center',
        paddingBottom: 10,
    },
    formContainer: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 10,
    },
    registerBtn: {
        marginBottom: 20,
        marginTop: 10
    },
    textWhite: {
        fontSize: 15,
        color: colorLight,
        paddingVertical: 5
    },
})
