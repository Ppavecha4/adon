import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    Alert,
    ActivityIndicator,
    KeyboardAvoidingView 
} from 'react-native'

import { connect } from 'react-redux'
import {
    loginUser
} from '../store/actions'
import FSDK, { LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk'
import Button from '../components/buttons/Button'
import Logo from '../components/Logo/Logo'
import { colorWhite, colorPrimary, colorAccent, colorLight } from '../style/colors'
import Icon from 'react-native-ionicons';
import TextFieldIcon from '../components/input/TextFieldIcon'
import { facebookService } from '../Server/FacebookAuthService'
import { locals } from '../utils/locals'

class LoginScreen extends Component {

    state = {
        email: '',
        password: '',
        socialUniqueId: ''
    }

    constructor(props) {
        super(props)

        this.local = locals[this.props.auth.local].auth
    }


    componentDidMount = async () => {
        try {
            // const { accessToken } = await AccessToken.getCurrentAccessToken()
            // console.log(accessToken)
        } catch (err) {
            // alert('Facebook authentication failed')
            console.log("Error in facebook authentication", err)
        }
    }

    onValueChange = (data) => {
        this.setState(data)
    }

    render() {
        return (
           <KeyboardAvoidingView > 
            <ImageBackground source={require('../../assets/login_bg.png')} style={{
                width: '100%',
                height: '100%'
            }}>

              <View style={styles.appBar} androidStatusBarColor={colorWhite}>
                    
                       <View style={styles.header}>
                        <TouchableOpacity  style={styles.headerin} onPress={() => {
                            this.props.navigation.goBack()
                        }}>
                            <Icon name='ios-arrow-back' color="#fff" />
                        </TouchableOpacity>
                    </View>
                </View> 

                {this.props.auth.isLogging ? (
                    <View style={styles.indicator}>
                        <ActivityIndicator size='large' color={colorPrimary} />
                    </View>
                ) : null}
                <React.Fragment>
                    <ScrollView style={styles.formContainer}>
                        <View style={styles.container}>

                            <View style={styles.logoContainer}>
                                {/* <Text style={styles.logo}>FixMap</Text> */}
                                <Logo />
                            </View>

                            <Text style={styles.heading}>{this.local.login}</Text>

                            {this.props.auth.error && <View style={{
                                width: '90%',
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: colorWhite,
                                paddingVertical: 10,
                                borderRadius: 10
                            }}>
                                <Text>{this.local.loginFailed}</Text>
                            </View>}

                            <View style={styles.formContainer}>
                                <TextFieldIcon
                                    keyboardType="email-address"
                                    icon='ios-contact'
                                    placeholder={this.local.email}
                                    style={styles.input}
                                    value={this.state.email}
                                    onChange={(value) => this.onValueChange({ email: value })} />
                                <TextFieldIcon
                                    secureTextEntry
                                    icon='ios-lock'
                                    placeholder={this.local.password}
                                    style={styles.input}
                                    secureTextEntry
                                    value={this.state.password}
                                    onChange={(value) => this.onValueChange({ password: value })} />

                                {/* <View style={styles.row}>
                                    <Icon name='ios-checkmark-circle-outline' style={{ marginRight: 10 }} size={20} color={colorLight} />
                                    <Text style={styles.textWhite}>Remember my password</Text>
                                </View> */}

                                <TouchableOpacity onPress={() => {
                                }}>
                                    <View style={styles.row}>
                                        <Icon name='ios-magnet' style={{ marginRight: 10 }} size={20} color={colorLight} />
                                        <Text style={styles.textWhite}>{this.local.forgotPassword}</Text>
                                    </View>
                                </TouchableOpacity>

                                <Button bgColor={colorAccent} title={this.local.login} onPress={() => this.props.loginUser(this.state, this.props.navigation)} style={styles.registerBtn} />
                                <TouchableOpacity>
                                    <Text style={{ ...styles.textWhite, textAlign: 'center', marginTop: 20 }}> {this.local.notMember} <Text style={{
                                        fontWeight: 'bold'
                                    }} onPress={() => this.props.navigation.navigate('Register')}>{this.local.register}</Text></Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </React.Fragment>
            </ImageBackground>
        </KeyboardAvoidingView>  
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginUser: (credentials, navigation) => dispatch(loginUser(credentials, navigation))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 10,
    },
    logoContainer: {
        position: 'relative',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        fontSize: 50,
        letterSpacing: 5,
        color: colorWhite
    },
    heading: {
        fontSize: 25,
        color: colorAccent,
        marginTop: 30,
        letterSpacing: 5,
        width: '100%',
        textAlign: 'center',
        paddingBottom: 10,
    },
    formContainer: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 10,
    },
    input: {
        flex: 1,
        marginVertical: 10,
    },
    marginRight: {
        marginRight: 10
    },
    registerBtn: {
        marginBottom: 20,
        marginTop: 10
    },
    textWhite: {
        fontSize: 15,
        color: colorLight,
        paddingVertical: 5
    },
    cartContainer: {
        width: 160,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        marginRight: 10
    },
    cartImage: {
        width: '100%',
        height: 80,
        flex: 1,
        borderWidth: .8,
        borderColor: colorWhite,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .8)',
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 999,
    },
    appBar: {
        paddingHorizontal: 20,
        elevation: 1
    },
    header: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        top:30
    },
    headerin:{
        width: '100%',
        height: 60,
        flexDirection: 'row',
    }
})
