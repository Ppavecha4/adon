import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Text,
  Image,
  FlatList,
  TouchableOpacity
} from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import GeoCoder from 'react-native-geocoder'
import { createDrawerNavigator } from 'react-navigation'
import { Left, Body, Button, Right } from 'native-base'
import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-ionicons'
import { connect } from 'react-redux'
import { colorWhite, colorDarkText, colorPrimary } from '../style/colors'
import { fetchReservationlist,createRequestResere } from '../store/actions/services'
import { cancelrequest, acceptrequest } from '../store/actions'
import AsyncStorage from '@react-native-community/async-storage'

import {
    REQUEST_SEND,
    REQUEST_DECLINE,
    REQUEST_PROVIDER_NOT_FOUND, 
    REQUEST_PRICE
} from '../store/actions/types'
import io from 'socket.io-client'
import { APP_SERVER_BASE_URL } from '../Server'
import { REQUEST_TIME_OUT_SEC } from '../utils/constants'
import { getRequestRemainingTime } from '../utils/utils'
import { locals } from '../utils/locals'
import PushNotificationIOS from "@react-native-community/push-notification-ios"
import axios from '../Server/DefaultServer'
class Reservation extends Component {
 
  state = {
     pay1: require('../../assets/cash.png'),
     pay2: require('../../assets/credit-card.png'),
     pay3: require('../../assets/2019-06-19.png'),
     region: {
            latitude: 0.0,
            longitude: 0.0,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        },
        userLocation: null,
        localAddress: 'Current location',
        currnttab : 0,
        listItem : this.props.service.reserve
  }

  constructor(props) {
        super(props)
        this.local = locals[this.props.auth.local].resevations
        this.mapView = null
        this.socket = io.connect(APP_SERVER_BASE_URL, {
            query: {
                user: this.props.auth.user.first_name
            }
        })
        this.socket.connect()
        this.socket.on('connect', () => {
            console.log('Connected to socket')
            this.socket.emit('status', (status) => {
                if (status !== 0) {
                    this.props.navigation.dispatch(notAllowedAction)
                }
            })
        })
        this.socket.emit('status', (status) => {
            if (status !== 0) {
                this.props.navigation.dispatch(notAllowedAction)
            }
        })

        this.setState({
          listItem: this.props.service.reserve
        })

      }
  componentDidMount = () => {
    this.openLastRegion()
    this.fetchLocalAddress()
    this.props.fetchReservationlist()

     navigator.geolocation.getCurrentPosition(position => {

      console.log("Location ", position);
            const region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: this.state.region.latitudeDelta,
                longitudeDelta: this.state.region.longitudeDelta
            }
            this.setState({
                userLocation: {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                }
            }, () => {
                //this.mapView.animateToRegion(region, 700)
                this.fetchLocalAddress()
            })
        })
      this.setState({
          listItem: this.props.service.reserve
        })
    
       console.log("hdfjahsdfk", this.props.service.reserve)

    console.log("Location ", this.state.userLocation);

       
   }

   requestView = async () => {
 
       //console.log("did =",this.props.auth.user.id)
        if(this.props.userType !== 'user'){

           const result = await axios.post('/user/reservationlistnotification', {
                 provider_id: this.props.auth.user.id,
           })
            console.log('Request Complete Paid', result)
           }
          }

   componentWillUnmount(){

     this.requestView()

      console.log("its Out");
   }

  requestcancel = (itemid) => {

      console.log("Request Cancel id =" , itemid );
  }

    navigateToScreen = (route) => {
        
            const navigateAction = NavigationActions.navigate({
                routeName: 'Reservation'
              })
      }
  
    openLastRegion = async () => {
        const result = await AsyncStorage.getItem('region')
        if (result) {
            this.setState({
                region: JSON.parse(result)
            })
        }
    }

    fetchLocalAddress = async () => {
        const result = await GeoCoder.geocodePosition({
            lat: this.state.region.latitude,
            lng: this.state.region.longitude
        })
        this.setState({
            localAddress: result[0].formattedAddress
        })
    }

    getMyValue = async () => {
      try {
        const value = await AsyncStorage.getItem('@MyApp_key')

        console.log("AsyncStorage", value)
      } catch(e) {
        // read error
      }

      console.log('Done.')

    }

    setValue = async (id) => {
      try {
        await AsyncStorage.setItem('@MyApp_key', id )
      } catch(e) {
        // save error
      }

      console.log('Done.')
    }

    tabacctions = async (tab) => {
     this.setState({
            currnttab: tab,
            listItem: this.props.service.reserve.filter(item => item.status == tab )
        })
    }
  renderCard = (item) => {

    console.log(item)
    
    const user = item

    console.log(this.state.currnttab, item.status)

    return ( 
     <View style={styles.card}  >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
          <Image style={styles.profileImg} source={{ uri: 'https://randomuser.me/api/portraits/men/32.jpg' }} />
          <View style={{ flex: 1, paddingLeft: 10 }}>
            <Text style={styles.profileName}>{`${user.first_name} ${user.last_name}`}</Text>
            <Text style={styles.phone}><Icon name='ios-location' />{ ` ${item.address}`}</Text>
             
          </View>
        </View>
        <View style={styles.row}>
           <View style={{ alignItems: 'flex-start' }}>
            <Text style={[styles.title, { color: colorDarkText }]}> {this.local.service} : {`${item.service}`}</Text>
            <Text style={[styles.title, { color: colorDarkText }]}> {this.local.budget} : $ {`${item.amount}`}</Text>
            <Text style={[styles.title, { color: colorDarkText }]}> {this.local.paymenttype } : { item.paymethod == 1 && <Text> CASH </Text> }

                                    { item.paymethod == 2 && <Text> CREDIT/DEBIT CARD </Text>  }

                                    { item.paymethod == 3 && <Text> PAYPAL </Text> } </Text>
            <Text style={[styles.title, { color: colorDarkText }]}> {this.local.date }  : {`${item.reservedate}`}</Text>
          </View>
        </View>
        { item.status == 1 && <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          position: 'absolute',
          top: 2,
          right: 10
        }}>

           <View style={{ width: 10, height: 10, borderRadius: 100, backgroundColor: '#f4bc06' }} />
             <Text style={{ color: '#f4bc06', fontSize: 15, marginHorizontal: 10 }}> {this.local.requestpanding }  </Text>
           </View> }
           { item.status == 2 && <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          position: 'absolute',
          top: 2,
          right: 10
        }}>

           <View style={{ width: 10, height: 10, borderRadius: 100, backgroundColor: '#41a853' }} />
             <Text style={{ color: '#41a853', fontSize: 15, marginHorizontal: 10 }}> {this.local.requestAccepted } </Text>
           </View> }
           { item.status == 3 && <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          position: 'absolute',
          top: 2,
          right: 10
        }}>

           <View style={{ width: 10, height: 10, borderRadius: 100, backgroundColor: '#ea4335' }} />
             <Text style={{ color:'#ea4335', fontSize: 15, marginHorizontal: 10 }}> {this.local.requestcancel } </Text>
           </View> }
           { item.status == 4 && <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          position: 'absolute',
          top: 2,
          right: 10
        }}>

           <View style={{ width: 10, height: 10, borderRadius: 100, backgroundColor: '#02fb2e' }} />
             <Text style={{ color: '#02fb2e', fontSize: 15, marginHorizontal: 10 }}> {this.local.requestCompleted } </Text>
           </View> }
           { item.status == 5 && <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          position: 'absolute',
          top: 2,
          right: 10
        }}>

           <View style={{ width: 10, height: 10, borderRadius: 100, backgroundColor: '#2161d6' }} />
             <Text style={{ color: '#2161d6', fontSize: 15, marginHorizontal: 10 }}> {this.local.requestcancel } </Text>
           </View> }

           <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}> 
            { item.status == 1 && this.props.auth.userType === 'provider' ? <TouchableOpacity  style={ styles.acceptbutton } onPress={() => {
                                     
                                    this.props.acceptrequest({acceptid: item.res_id })
                                    this.props.navigation.goBack()
                               }} >
              <Text style={{
                    color: colorWhite,
                    fontSize: 18,
                    letterSpacing: 1
                }}> {this.local.accepttext }  </Text>
            </TouchableOpacity> : <Text></Text> }

            { item.status == 2 && this.props.auth.userType === 'provider' ? <TouchableOpacity  style={ styles.acceptbutton } onPress={() => {
                                     
                                    // this.props.navigation.goBack()
                                     this.props.createRequestResere(this.state.region, item.paymethod, this.socket, this.state.userLocation, item.user_id, item.token, item)
                                      
                                        //AsyncStorage.setItem('@currenRequest', item.res_id )

                                        this.setValue(item.res_id)
                                        this.getMyValue()

                                      const navigateAction = NavigationActions.navigate({
                                      routeName: 'Home'
                                      })
                                      this.props.navigation.dispatch(navigateAction)
                               }} >
                <Text style={{
                    color: colorWhite,
                    fontSize: 18,
                    letterSpacing: 1
                }}>  {this.local.startwork } </Text>
            </TouchableOpacity> : <Text></Text> }

           { item.status == 1 || item.status == 2 ? <TouchableOpacity  style={ styles.reserveNow } onPress={() => {
                                     
                                     this.props.cancelrequest({cancelid: item.res_id })
                                      this.props.navigation.goBack()
                               }} >
              <Text style={{
                    color: colorWhite,
                    fontSize: 18,
                    letterSpacing: 1
                }}> {this.local.canceltext } </Text>
            </TouchableOpacity> : <Text> </Text> }
           </View> 
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.appBar} androidStatusBarColor={colorDarkText}>
          <View style={styles.header}>
            <Left>
              <Button transparent onPress={() => {
                this.props.navigation.goBack()
              }}>
                <Icon name='ios-arrow-back' color={colorDarkText} />
              </Button>
            </Left>
            <Body>
              <Text style={styles.title}>{ this.local.request} </Text>
              
            </Body>
            
            <Right>
              <Button transparent>
                <Icon name='menu' color='transparent' />
              </Button>
            </Right>
          </View>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                
                 <TouchableOpacity  style={ [styles.tab, {  backgroundColor: this.state.currnttab == 2 ? "#41a853":"#000" }]} onPress={() => { this.tabacctions(2) }} >
                    <Text style={styles.tabs}> {this.local.accepted } </Text>
               </TouchableOpacity>
                <TouchableOpacity  style={ [styles.tab, {  backgroundColor: this.state.currnttab == 1 ? "#41a853":"#000" }]} onPress={() => { this.tabacctions(1) }} >
                    <Text style={styles.tabs}> {this.local.panding } </Text>
               </TouchableOpacity>
               <TouchableOpacity  style={ [styles.tab, {  backgroundColor: this.state.currnttab == 5 ? "#41a853":"#000" }]} onPress={() => { this.tabacctions(5) }} >
                    <Text style={styles.tabs}> {this.local.canceled } </Text>
               </TouchableOpacity>
               <TouchableOpacity  style={ [styles.tab, {  backgroundColor: this.state.currnttab == 4 ? "#41a853":"#000" }]} onPress={() => { this.tabacctions(4) }} >
                    <Text style={styles.tabs}> {this.local.completed } </Text>
               </TouchableOpacity>
              </View>
        <FlatList
          data={ data = (this.state.listItem.length == 0 && this.state.currnttab == 0 ? this.props.service.reserve : this.state.listItem).sort((a, b) => a.res_id < b.res_id)}
          keyExtractor={item => item.provider_id}
          renderItem={({ item }) => this.renderCard(item)}
        />
      </View>
    )
  }
}

const mapStateToProps = ({ service, auth }) => {
  return { service, auth }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchReservationlist: () => dispatch(fetchReservationlist()),
    cancelrequest: (data) => dispatch(cancelrequest(data)),
    acceptrequest: (data) => dispatch(acceptrequest(data)),
     createRequestResere: (region, priceType, socket, userLocation,userid, token, user) => dispatch(createRequestResere(region, priceType, socket, userLocation, userid, token, user)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Reservation)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%'
  },
  appBar: {
    backgroundColor: colorWhite,
    paddingHorizontal: 10,
    elevation: 1
  },
  header: {
    width: '100%',
    height: 80,
    flexDirection: 'row',
    top:10,
  },
  title: {
    fontSize: 18,
    fontWeight: '500',
    letterSpacing: 1,
    color: colorDarkText
  },
  card: {
    marginVertical: 20,
    marginHorizontal: 10,
    backgroundColor: colorWhite,
    elevation: 5,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginBottom: 10,
  },
  profileImg: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  profileName: {
    fontSize: 25,
    color: colorDarkText
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  textSimple: {
    fontSize: 22,
    letterSpacing: 1
  },
  textHighlighted: {
    fontWeight: 'bold',
    fontSize: 30,
    letterSpacing: 5
  },
    reserveNow: {
            height: 40,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
            backgroundColor: "#ea4335",
            bottom: 0,
            paddingHorizontal: 10,
            borderRadius: 0,
            marginVertical: 10,
            marginHorizontal: 1 ,
            flex:2,
            top:10,
  },
    acceptbutton:{
      height: 40,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 10,
      backgroundColor: "#41a853",
      bottom: 0,
      paddingHorizontal: 10,
      borderRadius: 0,
      marginVertical: 10,
      marginHorizontal: 1 ,
      flex:2,
      top:10,
    },
    tabs:{
    color: colorWhite,
    fontSize: 15,
    letterSpacing: 1,
    },
    tab:{
            height: 40, 
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
            flex:1

    }
})
