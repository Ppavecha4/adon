import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image,
    Picker,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    FlatList,
    ActivityIndicator,
    ToastAndroid, 
    Platform,
    KeyboardAvoidingView
} from 'react-native'
import GooglePlacesInput from '../components/input/GooglePlacesInput'
import ImagePicker from 'react-native-image-picker'
import _ from 'underscore'
import { colorAccent, colorPrimary, colorWhite, colorDark, colorDarkText } from '../style/colors'
import Icon from 'react-native-ionicons'
import MultiSelectDialog from '../components/dialogs/MultiSelectDialog'
import Button from '../components/buttons/Button'
import TextFieldIcon from '../components/input/TextFieldIcon'
import { LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk'
import { connect } from 'react-redux'
import { registerUser } from '../store/actions/index'
import { fetchServices, fetchSubServices } from '../store/actions/services'
import { isEmailValid, deepCloneState } from '../utils/utils'
import { locals } from '../utils/locals'
import RNFetchBlob from 'react-native-fetch-blob'
import uuid4 from 'uuid/v4'
import * as firebase from 'firebase'
import { defaultAvatar } from '../utils/constants';

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

class RegisterScreen extends Component {

    state = {
        profileImage: '',
        firstName: '',
        lastName: '',
        email: '',
        number: '',
        bio: '',
        address: '',
        password: '',
        confirmPassword: '',
        providerType: -1,
        subscriptionType: 2,
        socialUniqueId: '',

        servicesTypes: [],
        serviceCatTypes: [],

        services: {},

        isMultiSelectOpen: false,
        currentSubServiceId: 1,
        isImageUploading: false,
        imageUploadProgress: null,

        isModalOpen: false,
        x:100,
    }

    options = {
        title: 'Select Profile Photo',
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    }

    constructor(props) {
        super(props)

        this.local = locals[this.props.auth.local].auth
    }

    static navigationOptions = {
        title: 'Register',
    }

    onValueChange = (data) => {
        this.setState(data, () => {
            this.isAllValid()
        })
    }

    componentDidMount = () => {
        this.props.fetchServices()
    }

    openImageChooser = () => {
        ImagePicker.showImagePicker(this.options, (response) => {
            console.log('Response = ', response)
            if (response.didCancel) {
                console.log('User cancelled image picker')
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error)
            } else {
                console.log(response)
                this.uploadImage(response)
            }
        })
    }

    uploadImage = (image) => {
        const imgUri = image.uri
        let uploadBlob = null
        const name = uuid4()
        const mime = 'image/jpeg'
        const uploadUri = Platform.OS === 'ios' ? imgUri.replace('file://', '') : imgUri
        const imageRef = firebase.storage().ref(`/profileAvatar/${name}`)

        this.setState({
            isImageUploading: true
        })

        fs.readFile(uploadUri, 'base64')
            .then(data => {
                return Blob.build(data, { type: `${mime};BASE64` })
            })
            .then(blob => {
                uploadBlob = blob
                console.log('Uploading')
                const task = imageRef.put(blob, { contentType: mime, name: name })
                task.on('state_changed', (snapshot) => {

                    console.log("event",snapshot)
                    var load =  Math.round(snapshot.bytesTransferred)
                    var total =  Math.round(snapshot.totalBytes)
                    var progress = ( load / total ) * 100
                
                   this.setState({
                        imageUploadProgress: progress.toFixed(0)
                    })
                })
                return task
            })
            .then(() => {
                uploadBlob.close()
                return imageRef.getDownloadURL()
            })
            .then(url => {
                console.log('Upload success', url)
                image.url = url
                this.setState({
                    isImageUploading: false,
                    profileImage: url,
                    imageUploadProgress: null
                })
            })
            .catch(error => {
                console.log('Upload failed', error)
                alert('Enable to upload image')
                this.setState({
                    isImageUploading: false,
                    imageUploadProgress: null
                })
            })
    }

    onSubServiceSelected = (services) => {
        if (services.length > 0) {
            const newState = deepCloneState(this.state)
            newState.isMultiSelectOpen = false
            newState.services[this.state.currentSubServiceId] = services
            this.setState(newState, () => {
                console.log(services, "are selected", this.state.services)
            })
        } else {
            const newState = deepCloneState(this.state)
            newState.isMultiSelectOpen = false
            delete newState.services[this.state.currentSubServiceId]
            this.setState(newState, () => {
                console.log("No service are selected", this.state.services)
            })
        }
    }

    isAllValid = () => {
        let isValid = false
        if (this.props.auth.userType === 'user') {
            isValid = this.validateUser()
        } else {
            isValid = this.validateProvider()
        }
        return isValid
    }

    validateUser = () => {
        this.resetValidationError()
        const {
            firstName,
            lastName,
            email,
            password,
            confirmPassword,
            address,
            bio,
            number,
            socialUniqueId
        } = this.state
        if (firstName === '' && !(firstName.length >= 3)) {
            this.setState({ firstNameError: this.local.errorFirstName })
        } else if (lastName === '' && !(lastName.length >= 2)) {
            this.setState({ lastNameError: this.local.errorLastName })
        } else if (!isEmailValid(email)) {
            this.setState({ emailError: this.local.errorEmail })
        } else if (number.length > 11 || number.length < 10  ) {
            this.setState({ numberError: this.local.errorNumber })
        } else if (bio === '') {
            this.setState({ bioError: this.local.errorBio })
        } else if (address === '') {
            this.setState({ addressError: this.local.errorAddress })
        } else if (socialUniqueId === '' && password.length < 6) {
            this.setState({ passwordError: this.local.errorPassword })
        } else if (confirmPassword !== password) {
            this.setState({ confirmPasswordError: this.local.errorRePassword })
        } else {
            this.resetValidationError()
            return true
        }
    }

    validateProvider = () => {
        this.resetValidationError()
        const {
            firstName,
            lastName,
            email,
            password,
            confirmPassword,
            address,
            bio,
            number,
            subscriptionType,
            providerType,
            services,
            socialUniqueId
        } = this.state
        if (firstName === '' && !(firstName.length >= 3)) {
            this.setState({ firstNameError: this.local.errorFirstName })
        } else if (lastName === '' && !(lastName.length >= 2)) {
            this.setState({ lastNameError: this.local.errorLastName })
        } else if (!isEmailValid(email)) {
            this.setState({ emailError: this.local.errorEmail })
        } else if(number.length > 11 || number.length < 10  ) {
            this.setState({ numberError: this.local.errorNumber })
        } else if (bio === '') {
            this.setState({ bioError: this.local.errorBio })
        } else if (address === '') {
            this.setState({ addressError: this.local.errorAddress })
        } else if (socialUniqueId === '' && password.length < 6) {
            this.setState({ passwordError: this.local.errorPassword })
        } else if (confirmPassword !== password) {
            this.setState({ confirmPasswordError: this.local.errorRePassword })
        } else if (providerType === -1) {
            this.setState({ providerTypeError: this.local.errorProviderType })
        } else if (providerType === 1 && subscriptionType === -1) {
            this.setState({ subscriptionTypeError: this.local.errorSubscriptionType })
        } else if (_.isEmpty(services)) {
            this.setState({ serviceError: this.local.errorService })
        } else {
            this.resetValidationError()
            return true
        }
    }

    leftscroll = () => {

        this.setState({ x: this.state.x + 50 })

        console.log( "left ===> ", this.state.x)

        this.flatList.scrollToOffset({ offset: this.state.x,animated: true })
    }

    rightscroll = () => {

        this.flatList.scrollToOffset({ offset: 0, animated: true })
      
    }

    resetValidationError = () => {
        this.setState({
            firstNameError: undefined,
            lastNameError: undefined,
            emailError: undefined,
            numberError: undefined,
            bioError: undefined,
            addressError: undefined,
            passwordError: undefined,
            confirmPasswordError: undefined,
            providerTypeError: undefined,
            subscriptionTypeError: undefined,
            serviceError: undefined
        })
    }

    renderOtherView = () => {
        return (
            <React.Fragment>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                   
                    borderRadius: 50,
                    paddingVertical: 0,
                    paddingHorizontal: 0,
                    marginVertical: 0
                }}>
                    <Icon name='ios-keypad' size={20} color="#000" style={{
                        padding: 10,
                        color: colorWhite,
                        marginRight: 10,
                        marginLeft: 10,
                    }} />
                    <Picker
                        style={{ flex: 1, color: colorWhite, marginVertical: 0 }}
                        onValueChange={(itemValue) =>
                            this.setState({ providerType: itemValue }, this.isAllValid)
                        }
                        selectedValue={this.state.providerType}>
                        <Picker.Item label={this.local.providerType} value={-1} />
                        <Picker.Item label={this.local.individual} value={2} />
                        <Picker.Item label={this.local.business} value={1} />
                    </Picker>
                    <Icon name='ios-arrow-down' size={20} color="#000" style={{
                        padding: 10,
                        color: colorWhite,
                        marginRight: 10,
                        marginLeft: 10,
                    }} />
                </View>
                {this.state.providerTypeError && <View style={styles.errorContainer}>
                    <Text style={styles.errorMsg}>{this.state.providerTypeError}</Text>
                </View>}
                {this.state.providerType === 1 ? <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderRadius: 50
                    
                }}>
                    <Icon name='ios-card' size={20} color="#000" style={{
                        padding: 10,
                        color: colorWhite,
                        marginRight: 10,
                        marginLeft: 10,
                    }} />

                    <Picker
                        style={{ flex: 1, color: colorWhite, marginVertical: 5 }}
                        onValueChange={(itemValue) =>
                            this.setState({ subscriptionType: itemValue }, this.isAllValid)
                        }
                        selectedValue={this.state.subscriptionType}>
                        <Picker.Item label={this.local.subscriptionType} value={-1} />
                        <Picker.Item label={this.local.premium} value={1} />
                        <Picker.Item label={this.local.normal} value={2} />
                    </Picker>
                    <Icon name='ios-arrow-down' size={20} color="#000" style={{
                        padding: 10,
                        color: colorWhite,
                        marginRight: 10,
                        marginLeft: 10,
                    }} />
                </View> : null}

                {this.state.subscriptionTypeError && <View style={styles.errorContainer}>
                    <Text style={styles.errorMsg}>{this.state.subscriptionTypeError}</Text>
                </View>}

                {this.state.serviceError ? <View style={styles.errorContainer}>
                    <Text style={styles.errorMsg}>{this.state.serviceError}</Text>
                </View> : <Text
                    style={styles.textWhite}>
                        {this.local.selectServiceType.toUpperCase()}
                    </Text>}

                {this.props.service.services.length > 0 ?
                    <FlatList
                        extraData={this.state}
                        showsHorizontalScrollIndicator={true}
                        horizontal
                        ref={flatList => { this.flatList = flatList }}
                        data={this.props.service.services}
                        keyExtractor={(item) => `${item.id}`}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => {
                                this.setState({
                                    isMultiSelectOpen: true,
                                    currentSubServiceId: item.id
                                }, this.isAllValid)
                            }}>
                                <View style={styles.cartContainer} key={`${item.id}`} >
                                    <Image source={{ uri: item.icon }} style={styles.cartImage} />
                                    <Text style={{
                                        color: colorWhite,
                                        fontSize: 12,
                                        marginTop: 10,
                                        textAlign: 'center'
                                    }}>{item.name}</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    /> : <ActivityIndicator size='large' color={colorPrimary} />}
                     <View>
                   <TouchableOpacity onPress={() => this.leftscroll()} style={{ position: 'absolute', right: 0, bottom:35, height:100,width:30,backgroundColor:'#ccccccc7', justifyContent: 'center',
    alignItems: 'center' }}>
                      <Icon name='ios-arrow-forward' color="#000" />
                   </TouchableOpacity>
                   <TouchableOpacity onPress={() => this.rightscroll()} style={{ position: 'absolute', left: 0, bottom:35, height:100,width:30,backgroundColor:'#ccccccc7', justifyContent: 'center',
    alignItems: 'center' }}>
                    <Icon name='ios-arrow-back' color="#000" />
                   </TouchableOpacity>
                </View>
            </React.Fragment>
        )
    }

    render() {
        return (
        <KeyboardAvoidingView> 
            <ImageBackground source={require('../../assets/login_bg.png')} style={{
                width: '100%',
                height: '100%'
            }}>
           
               <View style={styles.appBar} androidStatusBarColor={colorWhite}>
                    
                       <View style={styles.header}>
                        <TouchableOpacity  style={styles.headerin} onPress={() => {
                            this.props.navigation.goBack()
                        }}>
                            <Icon name='ios-arrow-back' color="#fff" />
                        </TouchableOpacity>
                    </View>
                </View>
                {this.props.auth.isLogging || this.state.isImageUploading ? (
                    <View style={styles.indicator}>
                        <ActivityIndicator size='large' color={colorPrimary} />
                        {this.state.imageUploadProgress !== null && <Text style={{
                            fontSize: 30,
                            textAlign: 'center',
                            paddingVertical: 10
                        }}>Uploading...</Text>}
                    </View>
                ) : null}
                
                <React.Fragment>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.formContainer}>
                        <View style={styles.container}>
                            <View style={styles.profileImageContainer}>
                                <Image
                                    style={styles.profileImage}
                                    source={{ uri: this.state.profileImage === '' ? defaultAvatar : this.state.profileImage }} />
                                <TouchableOpacity style={styles.iconBtn} onPress={this.openImageChooser}>
                                    <Icon name='camera'  color={colorWhite} size={18}/>
                                </TouchableOpacity>
                            </View>

                            <Text style={styles.heading}>{this.local.register}</Text>
                            <View style={styles.formContainer}>
                                <TextFieldIcon
                                    error={this.state.firstNameError}
                                    icon='ios-contact'
                                    placeholder={this.local.firstName}
                                    style={styles.input}
                                    value={this.state.firstName}
                                    onChange={(event) => this.onValueChange({ firstName: event })} />

                                <TextFieldIcon
                                    error={this.state.lastNameError}
                                    icon='ios-contact'
                                    placeholder={this.local.lastName}
                                    style={styles.input}
                                    value={this.state.lastName}
                                    onChange={(event) => this.onValueChange({ lastName: event })} />

                                <TextFieldIcon
                                    error={this.state.emailError}
                                    icon='ios-mail'
                                    placeholder={this.local.email}
                                    keyboardType='email-address'
                                    style={styles.input}
                                    value={this.state.email}
                                    onChange={(event) => this.onValueChange({ email: event })} />

                                <TextFieldIcon
                                    error={this.state.numberError}
                                    icon='ios-call'
                                    placeholder={this.local.number}
                                    keyboardType='numeric'
                                    style={styles.input}
                                    value={this.state.number}
                                    onChange={(event) => this.onValueChange({ number: event })} />

                                <TextFieldIcon
                                    error={this.state.bioError}
                                    icon='ios-document'
                                    placeholder={this.local.bio}
                                    style={styles.input}
                                    value={this.state.bio}
                                    onChange={(event) => this.onValueChange({ bio: event })} />

                                <TouchableOpacity onPress={() => {
                                    this.setState({
                                        isModalOpen: true
                                    })
                                }}>
                                    <TextFieldIcon
                                        editable={false}
                                        error={this.state.addressError}
                                        icon='locate'
                                        placeholder={this.local.address}
                                        style={styles.input}
                                        value={this.state.address}
                                        onChange={(event) => this.onValueChange({ address: event })} />
                                </TouchableOpacity>

                                <GooglePlacesInput show={this.state.isModalOpen} onDone={(query) => {
                                    this.setState({
                                        isModalOpen: false,
                                        address: query
                                    }, this.isAllValid)
                                }} />

                                {this.state.socialUniqueId === '' && <>
                                    <TextFieldIcon
                                        error={this.state.passwordError}
                                        secureTextEntry
                                        icon='ios-lock'
                                        placeholder={this.local.password}
                                        style={styles.input}
                                        secureTextEntry
                                        value={this.state.password}
                                        onChange={(event) => this.onValueChange({ password: event })} />

                                    <TextFieldIcon
                                        error={this.state.confirmPasswordError}
                                        secureTextEntry
                                        icon='ios-lock'
                                        placeholder={this.local.rePassword}
                                        style={styles.input}
                                        secureTextEntry
                                        value={this.state.confirmPassword}
                                        onChange={(event) => this.onValueChange({ confirmPassword: event })} />
                                </>}

                                {this.props.auth.userType === 'provider' ? this.renderOtherView() : null}
                                
                                {this.props.auth.error && <View style={{ 
                                    width: '100%',
                                    paddingVertical: 10,
                                    backgroundColor: '#000',
                                    borderRadius: 10,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginVertical: 10,
                                    color: "#fff",
                                }}>
                                    { this.props.auth.error == "email_error" && <Text style={{color:"#fff",}}>{this.local.emailError}</Text> }
                                    { this.props.auth.error == "phone_error" && <Text style={{color:"#fff",}}>{this.local.phoneError}</Text> }
                                </View>}

                                <Button bgColor={colorAccent} onPress={() => {
                                    if (this.isAllValid()) {
                                        const {
                                            firstName,
                                            lastName,
                                            email,
                                            password,
                                            address,
                                            bio,
                                            number,
                                            subscriptionType,
                                            providerType,
                                            services,
                                            socialUniqueId,
                                            profileImage
                                        } = this.state
                                        this.props.registerUser({
                                            firstName,
                                            lastName,
                                            email,
                                            password,
                                            address,
                                            bio,
                                            number,
                                            subscriptionType,
                                            providerType,
                                            services,
                                            socialUniqueId,
                                            profileImage
                                        }, this.props.navigation )
                                    }
                                }} title={this.local.register} style={styles.registerBtn} />
                            </View>
                        </View>
                    </ScrollView>
                    {this.state.isMultiSelectOpen ? <MultiSelectDialog serviceId={this.state.currentSubServiceId} servicesTypes={this.state.services[this.state.currentSubServiceId]} onDone={this.onSubServiceSelected} /> : null}
                </React.Fragment>
            </ImageBackground>
          </KeyboardAvoidingView>  
        );
    }
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchServices: () => dispatch(fetchServices()),
        fetchSubServices: (id) => dispatch(fetchSubServices(id)),
        registerUser: (cred, nav) => dispatch(registerUser(cred, nav))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 10,
    },
    profileImageContainer: {
        position: 'relative'
    },
    profileImage: {
        width: 150,
        height: 150,
        borderRadius: 75,
    },
    iconBtn: {
        position: 'absolute',
        top: '70%',
        right: 0,
        padding: 10,
        backgroundColor: colorAccent,
        borderRadius: 50,
        color: colorDark,
        fontSize: 15
    },
    heading: {
        fontSize: 25,
        color: colorAccent,
        marginTop: 30,
        letterSpacing: 5,
        width: '100%',
        textAlign: 'center',
        paddingBottom: 10,
    },
    formContainer: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 10,
    },
    input: {
        flex: 1,
        marginVertical: 10,
    },
    marginRight: {
        marginRight: 10
    },
    registerBtn: {
        marginTop: 20,
        marginBottom: 20
    },
    textWhite: {
        fontSize: 18,
        color: colorWhite,
        paddingVertical: 20
    },
    cartContainer: {
        width: 160,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        marginRight: 10
    },
    cartImage: {
        width: '100%',
        height: 80,
        flex: 1,
        borderWidth: .8,
        borderColor: colorWhite,
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .8)',
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 999,
    },
    errorContainer: {
        backgroundColor: colorWhite,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 100,
        opacity: .9,
        marginVertical: 5
    },
    errorMsg: {
        fontSize: 16,
        color: 'red',
        textAlign: 'center'
    },
    appBar: {
        paddingHorizontal: 20,
        elevation: 1
    },
    header: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        top:30
    },
    headerin:{
        width: '100%',
        height: 60,
        flexDirection: 'row',
    }
})
