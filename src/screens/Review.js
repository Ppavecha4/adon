import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Keyboard,
    Image,
    ToastAndroid
} from 'react-native'
import { Header, Left, Body, Button, Right } from 'native-base'
import { Rating } from 'react-native-ratings'
import Icon from 'react-native-ionicons'
import axios from '../Server/DefaultServer'
import { resetAction, logOutAction, providerAuthSuccess } from '../utils/navigation'
import { connect } from 'react-redux'
import { colorAccent, colorWhite, colorDarkText } from '../style/colors'
import { sendReview } from '../store/actions/services'
import {locals} from '../utils/locals'
import { defaultAvatar } from '../utils/constants'

class ReviewScreen extends Component {

    state = {
        bottomViewShown: true,
        comment: '',
        rating: 0
    }

    constructor(props) {
        super(props)

        this.local = locals[this.props.auth.local].review
        this.request = this.props.navigation.getParam('request')
    }

    componentDidMount = () => {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener.remove()
    }

    _keyboardDidShow = () => {
        this.setState({
            bottomViewShown: false
        })
    }

    _keyboardDidHide = () => {
        this.setState({
            bottomViewShown: true
        })
    }

    getProfileImage = () => this.props.auth.userType === 'user' ? this.request.walker.state : this.request.owner.state

    getNameFromRequest = () => this.props.auth.userType === 'user' ? `${this.request.walker.first_name} ${this.request.walker.last_name}` : `${this.request.owner.first_name} ${this.request.owner.last_name}`

    getPhoneNumberFromRequest = () => this.props.auth.userType === 'user' ? this.request.walker.phone : this.request.owner.phone

    sendReview = async () => {
        const { id, token } = this.props.auth.user
        const userType = this.props.auth.userType
        const body = {
            id,
            token,
            request_id: this.request.request_id,
            comment: this.state.comment,
            rating: this.state.rating
        }
        const response = await axios.post(`/${userType}/rating`, body)
        console.log('review done', response.data)
        if (response.data.success) {
            console.log('review done', response.data)
            ToastAndroid.show(this.local.done, ToastAndroid.LONG)
        } else {
            console.log('Rating failed', response.data)
            ToastAndroid.show(this.local.failed, ToastAndroid.LONG)
        }
        this.props.navigation.goBack()
        this.props.navigation.state.params.onRatingDone()
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.appBar} androidStatusBarColor={colorDarkText}>
                        <View style={styles.header}>
                            <Left>
                                <Button transparent onPress={() => {
                                    this.props.navigation.goBack()
                                }}>
                                    <Icon name='ios-arrow-back' color={colorWhite} />
                                </Button>
                            </Left>
                            <Body>
                                <Text style={styles.title}>{this.local.review}</Text>
                            </Body>
                            <Right>
                                <Button transparent>
                                    <Icon name='menu' color='transparent' />
                                </Button>
                            </Right>
                        </View>
                        <Icon android='clipboard' style={{
                            alignSelf: 'center'
                        }} color={colorWhite} size={200} />
                    </View>
                    <View style={[styles.card, { marginBottom: 0 }]}>
                        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
                            <Image style={styles.profileImg} source={{ uri: this.getProfileImage() === '' ? defaultAvatar : this.getProfileImage() }} />
                            <View style={{ flex: 1, paddingLeft: 10 }}>
                                <Text style={styles.profileName}>{this.getNameFromRequest()}</Text>
                                <Text style={styles.phone}>{this.getPhoneNumberFromRequest()}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.card}>
                        <Text>{this.local.now}</Text>
                        <Rating
                            startingValue={0}
                            onFinishRating={rating => this.setState({
                                rating
                            })}
                        />
                        <Text>{this.local.feedback}</Text>
                        <TextInput
                            value={this.state.comment}
                            onChangeText={text => this.setState({
                                comment: text
                            })}
                            multiline
                            style={{
                                borderBottomWidth: .5
                            }} />
                    </View>
                </ScrollView>
                {this.state.bottomViewShown ? <View style={{
                    width: '100%',
                    height: 70,
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    paddingVertical: 10,
                    position: 'absolute',
                    bottom: 0
                }}>
                    <TouchableOpacity onPress={() => this.sendReview()} style={styles.btnCountinue}>
                    <Text style={styles.title}>{this.local.doneBtn}</Text>
                    </TouchableOpacity>
                </View> : null}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

export default connect(mapStateToProps, null)(ReviewScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%'
    },
    appBar: {
        backgroundColor: colorDarkText,
        paddingHorizontal: 20,
    },
    header: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
    },
    title: {
        fontSize: 18,
        fontWeight: '500',
        letterSpacing: 1,
        color: colorWhite
    },
    card: {
        marginVertical: 20,
        marginHorizontal: 10,
        backgroundColor: colorWhite,
        elevation: 5,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 15,
        marginBottom: 70,
    },
    profileImg: {
        width: 70,
        height: 70,
        borderRadius: 100,
    },
    profileName: {
        fontSize: 25,
        color: colorDarkText
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10,
        borderBottomWidth: .5,
    },
    textSimple: {
        fontSize: 22,
        letterSpacing: 1
    },
    textHighlighted: {
        fontWeight: 'bold',
        fontSize: 30,
        letterSpacing: 5
    },
    btnCancel: {
        flex: 1,
        backgroundColor: colorWhite,
        borderWidth: 1,
        color: colorWhite,
        fontSize: 18,
        letterSpacing: 2,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        borderRadius: 10
    },
    btnCountinue: {
        flex: 1,
        backgroundColor: colorDarkText,
        color: colorDarkText,
        fontSize: 18,
        letterSpacing: 2,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        borderRadius: 10
    }
})
