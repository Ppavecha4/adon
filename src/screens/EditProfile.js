import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    ImageBackground,
    FlatList,
    ActivityIndicator,
    ScrollView,
    Platform,
    ImageEditor
} from 'react-native'
import GooglePlacesInput from '../components/input/GooglePlacesInput'
import ImagePicker from 'react-native-image-picker'
import _ from 'underscore'
import { colorAccent, colorPrimary, colorWhite, colorDark } from '../style/colors'
import Icon from 'react-native-ionicons'
import MultiSelectDialog from '../components/dialogs/MultiSelectDialog'
import Button from '../components/buttons/Button'
import TextFieldIcon from '../components/input/TextFieldIcon'
import { connect } from 'react-redux'
import { registerUser, updateProfile } from '../store/actions/index'
import { fetchServices, fetchSubServices } from '../store/actions/services'
import { isEmailValid, deepCloneState, composeServices } from '../utils/utils'
import { locals } from '../utils/locals'
import RNFetchBlob from 'react-native-fetch-blob'
import uuid4 from 'uuid/v4'
import * as firebase from 'firebase'
import { defaultAvatar } from '../utils/constants';

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

class EditProfileScreen extends Component {

    constructor(props) {
        super(props)

        this.local = locals[this.props.auth.local].auth

        this.state = {
            profileImage: '',
            firstName: '',
            lastName: '',
            email: '',
            number: '',
            bio: '',
            address: '',
            password: '',
            confirmPassword: '',
            providerType: -1,
            subscriptionType: 2,

            services: this.props.auth.userType === 'user' ? {} : composeServices(this.props.auth.user.main_type_id, this.props.auth.user.sub_type_id),

            isMultiSelectOpen: false,
            currentSubServiceId: 1,
            isImageUploading: false,
            imageUploadProgress: null,
            isModalOpen: false,
            x:100,
        }

    }

    options = {
        title: 'Select Profile Photo',
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    }

    onValueChange = (data) => {
        this.setState(data)
    }

    componentDidMount = () => {
        this.populateInputFields()
        this.props.fetchServices()
    }

    populateInputFields = () => {
        const {
            first_name,
            last_name,
            phone,
            email,
            state,
            bio,
            address
        } = this.props.auth.user
        this.setState({
            firstName: first_name,
            lastName: last_name,
            email,
            number: phone,
            profileImage: state,
            bio,
            address
        })
    }

    openImageChooser = () => {
        ImagePicker.showImagePicker(this.options, (response) => {
            console.log('Response = ', response)
            if (response.didCancel) {
                console.log('User cancelled image picker')
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error)
            } else {
                this.uploadImage(response)
                // cropData = {
                //     offset: { x: 0, y: 0 },
                //     size: { width: response.width, height: response.height },
                //     displaySize: { width: response.width / 3, height: response.height / 3 },
                //     // resizeMode: 'contain/cover/stretch',
                // }
                // ImageEditor.cropImage(response.uri, cropData, (result) => {
                //     console.log('Image croped', result)
                //     this.setState({
                //         profileImage: result
                //     })
                // }, (error) => {
                //     console.log("Error in crop image")
                // })
            }
        })
    }

    uploadImage = (image) => {
        const imgUri = image.uri
        let uploadBlob = null
        const name = uuid4()
        const mime = 'image/jpeg'
        const uploadUri = Platform.OS === 'ios' ? imgUri.replace('file://', '') : imgUri
        const imageRef = firebase.storage().ref(`/profileAvatar/${name}`)

        this.setState({
            isImageUploading: true
        })

        fs.readFile(uploadUri, 'base64')
            .then(data => {
                return Blob.build(data, { type: `${mime};BASE64` })
            })
            .then(blob => {
                uploadBlob = blob
                console.log('Uploading')
                const task = imageRef.put(blob, { contentType: mime, name: name })
                task.on('state_changed', (snapshot) => {
                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    this.setState({
                        imageUploadProgress: progress
                    })
                })
                return task
            })
            .then(() => {
                uploadBlob.close()
                return imageRef.getDownloadURL()
            })
            .then(url => {
                console.log('Upload success', url)
                image.url = url
                this.setState({
                    isImageUploading: false,
                    profileImage: url,
                    imageUploadProgress: null
                })
            })
            .catch(error => {
                console.log('Upload failed', error)
                alert('Enable to upload image')
                this.setState({
                    isImageUploading: false,
                    imageUploadProgress: null
                })
            })
    }

    leftscroll = () => {

        this.setState({ x: this.state.x + 50 })

        console.log( "left ===> ", this.state.x)

        this.flatList.scrollToOffset({ offset: this.state.x,animated: true })
    }

    rightscroll = () => {

        this.flatList.scrollToOffset({ offset: 0, animated: true })
      
    }


    onSubServiceSelected = (services) => {
        if (services.length > 0) {
            const newState = deepCloneState(this.state)
            newState.isMultiSelectOpen = false
            newState.services[this.state.currentSubServiceId] = services
            this.setState(newState, () => {
                console.log(services, "are selected", this.state.services)
            })
        } else {
            const newState = deepCloneState(this.state)
            newState.isMultiSelectOpen = false
            delete newState.services[this.state.currentSubServiceId]
            this.setState(newState, () => {
                console.log("No service are selected", this.state.services)
            })
        }
    }

    isAllValid = () => {
        let isValid = false
        if (this.props.auth.userType === 'user') {
            isValid = this.validateUser()
        } else {
            isValid = this.validateProvider()
        }
        return isValid
    }

    validateUser = () => {
        this.resetValidationError()
        const {
            firstName,
            lastName,
            email,
            password,
            confirmPassword,
            address,
            bio,
            number
        } = this.state
        if (firstName === '' && !(firstName.length >= 3)) {
            this.setState({ firstNameError: this.local.errorFirstName })
        } else if (lastName === '' && !(lastName.length >= 2)) {
            this.setState({ lastNameError: this.local.errorLastName })
        } else if (!isEmailValid(email)) {
            this.setState({ emailError: this.local.errorEmail })
        } else if (number.length > 11 || number.length < 10  ) {
            this.setState({ numberError: this.local.errorNumber })
        } else if (bio === '') {
            this.setState({ bioError: this.local.errorBio })
        } else if (address === '') {
            this.setState({ addressError: this.local.errorAddress })
        } else if (confirmPassword.length < 6 && password.length >= 6) {
            this.setState({ confirmPasswordError: this.local.errorPassword })
        } else {
            this.resetValidationError()
            return true
        }
    }

    validateProvider = () => {
        this.resetValidationError()
        const {
            firstName,
            lastName,
            email,
            password,
            confirmPassword,
            address,
            bio,
            number,
            subscriptionType,
            providerType,
            services
        } = this.state
        if (firstName === '' && !(firstName.length >= 3)) {
            this.setState({ firstNameError: this.local.errorFirstName })
        } else if (lastName === '' && !(lastName.length >= 2)) {
            this.setState({ lastNameError: this.local.errorLastName })
        } else if (!isEmailValid(email)) {
            this.setState({ emailError: this.local.errorEmail })
        } else if  (number.length > 11 || number.length < 10  ) {
            this.setState({ numberError: this.local.errorNumber })
        } else if (bio === '') {
            this.setState({ bioError: this.local.errorBio })
        } else if (address === '') {
            this.setState({ addressError: this.local.errorAddress })
        } else if (confirmPassword.length < 6 && password.length >= 6) {
            this.setState({ confirmPasswordError: this.local.errorPassword })
        } else if (_.isEmpty(services)) {
            this.setState({ serviceError: this.local.errorService })
        } else {
            this.resetValidationError()
            return true
        }
    }

    resetValidationError = () => {
        this.setState({
            firstNameError: undefined,
            lastNameError: undefined,
            emailError: undefined,
            numberError: undefined,
            bioError: undefined,
            addressError: undefined,
            passwordError: undefined,
            confirmPasswordError: undefined,
            providerTypeError: undefined,
            subscriptionTypeError: undefined,
            serviceError: undefined
        })
    }

    renderOtherView = () => {
        return (
            <React.Fragment>

                {this.state.serviceError ? <View style={styles.errorContainer}>
                    <Text style={styles.errorMsg}>{this.state.serviceError}</Text>
                </View> : <Text
                    style={styles.textWhite}>
                        {this.local.selectServiceType.toUpperCase()}
                    </Text>}

                {this.props.service.services.length > 0 ?
                    <FlatList
                        extraData={this.state}
                        showsHorizontalScrollIndicator={false}
                        horizontal
                        ref={flatList => { this.flatList = flatList }}
                        data={this.props.service.services}
                        keyExtractor={(item) => `${item.id}`}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => {
                                this.setState({
                                    isMultiSelectOpen: true,
                                    currentSubServiceId: item.id
                                })
                            }}>
                                <View style={styles.cartContainer} key={`${item.id}`} >
                                    <Image source={{ uri: item.icon }} style={styles.cartImage} />
                                    <Text style={{
                                        color: colorWhite,
                                        fontSize: 12,
                                        marginTop: 10,
                                        textAlign: 'center'
                                    }}>{item.name}</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    /> : <ActivityIndicator size='large' color={colorPrimary} />}
                     <View>
                   <TouchableOpacity onPress={() => this.leftscroll()} style={{ position: 'absolute', right: 0, bottom:35, height:100,width:30,backgroundColor:'#ccccccc7', justifyContent: 'center',
    alignItems: 'center' }}>
                      <Icon name='ios-arrow-forward' color="#000" />
                   </TouchableOpacity>
                   <TouchableOpacity onPress={() => this.rightscroll()} style={{ position: 'absolute', left: 0, bottom:35, height:100,width:30,backgroundColor:'#ccccccc7', justifyContent: 'center',
    alignItems: 'center' }}>
                    <Icon name='ios-arrow-back' color="#000" />
                   </TouchableOpacity>
                </View>
            </React.Fragment>
        )
    }

    render() {
        return (
            <ImageBackground source={require('../../assets/login_bg.png')} style={{
                width: '100%',
                height: '100%'
            }}>
               <View style={styles.appBar} androidStatusBarColor={colorWhite}>
                    
                       <View style={styles.header}>
                        <TouchableOpacity  style={styles.headerin} onPress={() => {
                            this.props.navigation.goBack()
                        }}>
                            <Icon name='ios-arrow-back' color="#fff" />
                        </TouchableOpacity>
                    </View>
                </View> 
                {this.props.auth.isLogging || this.state.isImageUploading ? (
                    <View style={styles.indicator}>
                        <ActivityIndicator size='large' color={colorPrimary} />
                        {this.state.imageUploadProgress !== null && <Text style={{
                            fontSize: 30,
                            textAlign: 'center',
                            paddingVertical: 10
                        }}> Uploading... </Text>}
                    </View>
                ) : null}
                <React.Fragment>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.formContainer}>
                        <View style={styles.container}>
                            <View style={styles.profileImageContainer}>
                                <Image
                                    style={styles.profileImage}
                                    source={{ uri: this.state.profileImage !== '' ? this.state.profileImage : this.props.auth.user.state === '' ? defaultAvatar : this.props.auth.user.state }} />
                                <TouchableOpacity style={styles.iconBtn} onPress={this.openImageChooser}>
                                    <Icon name='camera' color={colorWhite} size={18} />
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.heading}>{this.local.editProfile}</Text>
                            <View style={styles.formContainer}>
                                <TextFieldIcon
                                    error={this.state.firstNameError}
                                    icon='ios-contact'
                                    placeholder={this.local.firstName}
                                    style={styles.input}
                                    value={this.state.firstName}
                                    onChange={(event) => this.onValueChange({ firstName: event })} />

                                <TextFieldIcon
                                    error={this.state.lastNameError}
                                    icon='ios-contact'
                                    placeholder={this.local.lastName}
                                    style={styles.input}
                                    value={this.state.lastName}
                                    onChange={(event) => this.onValueChange({ lastName: event })} />

                                <TextFieldIcon
                                    error={this.state.emailError}
                                    icon='ios-mail'
                                    placeholder={this.local.email}
                                    keyboardType='email-address'
                                    style={styles.input}
                                    value={this.state.email}
                                    onChange={(event) => this.onValueChange({ email: event })} />

                                <TextFieldIcon
                                    error={this.state.numberError}
                                    icon='ios-call'
                                    placeholder={this.local.number}
                                    keyboardType='numeric'
                                    style={styles.input}
                                    value={this.state.number}
                                    onChange={(event) => this.onValueChange({ number: event })} />

                                <TextFieldIcon
                                    error={this.state.bioError}
                                    icon='ios-document'
                                    placeholder={this.local.bio}
                                    style={styles.input}
                                    value={this.state.bio}
                                    onChange={(event) => this.onValueChange({ bio: event })} />
                               <TouchableOpacity onPress={() => {
                                    this.setState({
                                        isModalOpen: true
                                    })
                                }}>
                                <TextFieldIcon
                                    error={this.state.addressError}
                                    icon='locate'
                                    placeholder={this.local.address}
                                    style={styles.input}
                                    value={this.state.address}
                                    onChange={(event) => this.onValueChange({ address: event })} />
                                 </TouchableOpacity>

                                 <GooglePlacesInput show={this.state.isModalOpen} onDone={(query) => {
                                    this.setState({
                                        isModalOpen: false,
                                        address: query
                                    }, this.isAllValid)
                                }} />

                                <TextFieldIcon
                                    error={this.state.passwordError}
                                    secureTextEntry
                                    icon='ios-lock'
                                    placeholder={this.local.oldPassword}
                                    style={styles.input}
                                    secureTextEntry
                                    value={this.state.password}
                                    onChange={(event) => this.onValueChange({ password: event })} />

                                <TextFieldIcon
                                    error={this.state.confirmPasswordError}
                                    secureTextEntry
                                    icon='ios-lock'
                                    placeholder={this.local.newPassword}
                                    style={styles.input}
                                    secureTextEntry
                                    value={this.state.confirmPassword}
                                    onChange={(event) => this.onValueChange({ confirmPassword: event })} />

                                {this.props.auth.userType === 'provider' ? this.renderOtherView() : null}

                                {this.props.auth.updateStatus !== null && <View style={{
                                    width: '100%',
                                    paddingVertical: 10,
                                    backgroundColor: colorWhite,
                                    borderRadius: 10,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginVertical: 10
                                }}>
                                    <Text>{this.props.auth.updateStatus ? this.local.updateSuccess : this.local.updateFailed}</Text>
                                </View>}

                                <Button bgColor={colorAccent} onPress={() => {
                                    if (this.isAllValid()) {
                                        const {
                                            firstName,
                                            lastName,
                                            email,
                                            password,
                                            confirmPassword,
                                            address,
                                            bio,
                                            number,
                                            subscriptionType,
                                            providerType,
                                            services,
                                            profileImage
                                        } = this.state
                                        this.props.updateProfile({
                                            firstName,
                                            lastName,
                                            email,
                                            new_password: confirmPassword,
                                            old_password: password,
                                            address,
                                            bio,
                                            number,
                                            subscriptionType,
                                            providerType,
                                            services,
                                            profileImage
                                        }, this.props.navigation)
                                    }
                                }} title={this.local.updateProfile} style={styles.registerBtn} />
                            </View>
                        </View>
                    </ScrollView>
                    {this.state.isMultiSelectOpen && <MultiSelectDialog serviceId={this.state.currentSubServiceId} servicesTypes={this.state.services[this.state.currentSubServiceId]} onDone={this.onSubServiceSelected} />}
                </React.Fragment>
            </ImageBackground>
        );
    }
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchServices: () => dispatch(fetchServices()),
        fetchSubServices: (id) => dispatch(fetchSubServices(id)),
        registerUser: (cred, nav) => dispatch(registerUser(cred, nav)),
        updateProfile: (cred, nav) => dispatch(updateProfile(cred, nav))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 10,
    },
    profileImageContainer: {
        position: 'relative'
    },
    profileImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    iconBtn: {
        position: 'absolute',
        top: '70%',
        right: 0,
        padding: 10,
        backgroundColor: colorDark,
        borderRadius: 50,
        color: colorWhite,
        fontSize: 15
    },
    heading: {
        fontSize: 25,
        color: colorAccent,
        marginTop: 30,
        letterSpacing: 5,
        width: '100%',
        textAlign: 'center',
        paddingBottom: 10,
    },
    formContainer: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 10,
    },
    input: {
        flex: 1,
        marginVertical: 10,
    },
    marginRight: {
        marginRight: 10
    },
    registerBtn: {
        marginBottom: 20
    },
    textWhite: {
        fontSize: 18,
        color: colorWhite,
        paddingVertical: 20
    },
    cartContainer: {
        width: 160,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        marginRight: 10
    },
    cartImage: {
        width: '100%',
        height: 80,
        flex: 1,
        borderWidth: .8,
        borderColor: colorWhite,
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .8)',
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 999,
    },
    errorContainer: {
        backgroundColor: colorWhite,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 100,
        opacity: .9,
        marginVertical: 5
    },
    errorMsg: {
        fontSize: 16,
        color: 'red',
        textAlign: 'center'
    },
    appBar: {
        paddingHorizontal: 20,
        elevation: 1
    },
    header: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        top:30
    },
    headerin:{
        width: '100%',
        height: 60,
        flexDirection: 'row',
    }
})
