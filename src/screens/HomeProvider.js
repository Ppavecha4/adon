import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    ToastAndroid,
    TextInput,
    PanResponder
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import Icon from 'react-native-ionicons'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import GeoCoder from 'react-native-geocoder'
import { createDrawerNavigator } from 'react-navigation'
import { colorPrimary, colorWhite, colorAccent, colorDarkText, colorDark } from '../style/colors'
import HomeNavigationDrawer from '../components/drawer/HomeNavigationDrawer'
import { connect } from 'react-redux'
import {
    createRequest,
    updateLatLog,
    acceptRequest,
    declineRequest,
    checkForPendingRequests,
    clearPendingRequest,
    initPendingRequestProvider,
    rejectRequest
} from '../store/actions/services'
import { restoreLoginSession, logOut } from '../store/actions/index'
import { workComplete } from '../store/actions/transaction'
import { updateCurrentLocation } from '../store/actions/location'
import { logOutAction } from '../utils/navigation'
import HistoryScreen from './History'
import axios from '../Server/DefaultServer'
import * as firebase from 'firebase'
import PriceDialog from '../components/dialogs/PriceDialog'
import ActionButton from '../components/buttons/ActionButton'
import _ from 'underscore'
import { isRequestExpired, getRequestRemainingTime } from '../utils/utils'
import { REQUEST_TIME_OUT_SEC } from '../utils/constants'
import { locals } from '../utils/locals'
import { fetchReservationlist } from '../store/actions/services'

class HomeProvider extends Component {

    state = {
        region: {
            latitude: 0.0,
            longitude: 0.0,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        },
        localAddress: 'Current location',
        searchQuery: '',
        isPriceEntered: false,

        request: {},
        requestTimeOut: REQUEST_TIME_OUT_SEC,
        newRequest : 0,
    }

    isTimerRunning = false

    constructor(props) {
        super(props)
        
        this.local = locals[this.props.auth.local].providerHome
        this.toastLocal = locals[this.props.auth.local].toast
        this.mapView = null

        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onStartShouldSetPanResponderCapture: () => true,
            onMoveShouldSetPanResponder: () => true,
            onMoveShouldSetPanResponderCapture: () => true,
            onPanResponderRelease: () => {
                this.fetchLocalAddress()
            }
        })
    }

    componentDidMount() {

        
        
        this.openLastRegion()
        this.fetchLocalAddress()
        
         this.props.fetchReservationlist()
        this.userId = this.props.auth.user.id

        this.databaseRef = firebase.database().ref(`requests/${this.userId}`)
        this.databaseRef.on('child_added', snapshot => {
            const request = snapshot.val()
            console.log('Got request from firebase', request)
            if (!request.isCompleted && !request.isRejected) {
                if (!isRequestExpired(request)) {
                    this.setState({
                        request
                    }, () => {
                        this.startRequestTimer()
                    })
                }
                this.requestRef = firebase.database().ref(`requests/${this.userId}/${request.request_id}`)
                console.log('listening for changes')
                this.requestRef.on('value', snapshot => {
                    const request = snapshot.val()
                    if (request) {
                        console.log('request changed')
                        if (isRequestExpired(request) && !request.isAccepted) {
                            this.props.declineRequest(request.request_id)
                            this.props.rejectRequest(request.request_id)
                            this.requestRef.update({
                                isRejected: true
                            })
                            this.setState({
                                request: {}
                            })
                        } else {
                            if (request.isRejected) {
                                this.setState({
                                    request: {}
                                })
                            } else if (request.isCompleted) {
                                ToastAndroid.show(this.toastLocal.workCompleted, ToastAndroid.LONG)
                                this.setState({
                                    request: {}
                                })
                            } else {
                                this.setState({
                                    request
                                })
                            }
                        }
                    }
                })
            }

            this.checkRequest()
        })

        this.isPriceEntered = false

        navigator.geolocation.getCurrentPosition(
            position => {
                const region = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: this.state.region.latitudeDelta,
                    longitudeDelta: this.state.region.longitudeDelta
                }
                this.setState({
                    region
                }, () => {
                    this.mapView.animateToRegion(region, 700)
                    this.fetchLocalAddress()
                })
                this.props.updateLatLog(position.coords)
                this.props.updateCurrentLocation(region)
                console.log('Current Lat Lng', position)
            },
            error => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 })
      
       
       
    }


    componentDidUpdate() {
        if (!this.props.auth.isAuthenticated) {
            this.props.navigation.dispatch(logOutAction)
        }
        if (this.props.service.pendingRequest !== null) {
            this.props.initPendingRequestProvider()
            console.log('Got a pending request')
            this.props.clearPendingRequest()
        }
    }

    componentWillUnmount = () => {
        if (this.requestRef && this.databaseRef) {
            this.requestRef.off('value')
            this.databaseRef.off('child_added')
        }
        this.checkRequest()
    }

    openDrawer = () => {
        this.props.navigation.openDrawer()
        this.checkRequest()

    }

    onRegionChange = (region) => {
        AsyncStorage.setItem('region', JSON.stringify(region))
        this.setState({
            region
        })
        this.checkRequest()
    }

    openLastRegion = async () => {
        let result = await AsyncStorage.getItem('region')
        result = JSON.parse(result)
        if (result) {
            this.props.updateCurrentLocation(result)
            this.setState({
                region: result
            })
        }
        this.checkRequest()
    }

    openUserLocation = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                const region = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: this.state.region.latitudeDelta,
                    longitudeDelta: this.state.region.longitudeDelta
                }
                this.setState({
                    region
                }, () => {
                    this.fetchLocalAddress()
                })
                this.mapView.animateToRegion(region, 700)
                console.log('Current Lat Lng', position)
            },
            error => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 })
    }

    openDestination = () => {
        this.mapView.animateToRegion({
            latitude: this.state.request.destination.latitude,
            longitude: this.state.request.destination.longitude,
            latitudeDelta: this.state.region.latitudeDelta,
            longitudeDelta: this.state.region.longitudeDelta
        })
    }

    fetchLocalAddress = async () => {
        const result = await GeoCoder.geocodePosition({
            lat: this.state.region.latitude,
            lng: this.state.region.longitude
        })
        this.setState({
            localAddress: result[0].formattedAddress
        })
    }

    fetchRegionFromAddress = async () => {
        console.log(this.state)
        const result = await GeoCoder.geocodeAddress(this.state.searchQuery)
        console.log(result)
    }

    completeWalk = async () => {
       
      //  await axios.post('provider/request_all_status_1', { })
         console.log( "Current request == >", this.state.request)

        const response = await axios.post('provider/request_all_status_1', {
            walker_id: this.props.auth.user.id,
            request_id: this.state.request.request_id,
            total: this.state.request.price,
            is_walker_started: 1,
            is_walker_arrived: 1,
            is_started: 1,
            is_completed: 1
        })
        if (response.data.success) {
            this.requestRef.update({
                isWorkDone: true
            })
        } else {
            ToastAndroid.show(this.toastLocal.enableToSendRequest, ToastAndroid.LONG)
        }
        console.log('Response of walk completed', response)
    }

    renderWalkThrough = () => {
        console.log("RenderWalkThrough")
        if (this.state.request.isAccepted && !this.state.request.price) {
            ToastAndroid.show(this.toastLocal.requestAccepted, ToastAndroid.LONG)
            let dialog = null
            if (!this.state.isPriceEntered) {
                console.log("Inside Dialog")
                dialog = <PriceDialog
                    show
                    onDone={(price) => {
                        this.setState({
                            isPriceEntered: true
                        })
                        this.requestRef.update({
                            price
                        })
                    }} />
            }
            return dialog
        }
    }

    startRequestTimer = () => {
        if (!this.isTimerRunning) {
            this.timerInterval = setInterval(() => {
                const time = getRequestRemainingTime(this.state.request)
                this.setState({
                    requestTimeOut: time
                })
                if (this.state.requestTimeOut <= 0) {
                    clearInterval(this.timerInterval)
                    this.isTimerRunning = false
                    this.props.declineRequest(this.state.request.request_id)
                    this.requestRef.update({
                        isRejected: true
                    })
                    this.setState({
                        request: {}
                    })
                }
            }, 1000)
            this.isTimerRunning = true
        }
    }

    renderCallToAction = () => {
        if (this.state.request.isWorkDone) {
            this.props.navigation.navigate('Invoice', {
                request: this.state.request,
                requestRef: this.requestRef
            })
            return null
        } else if (this.state.request.isWorkStarted) {
            return (
                <ActionButton title={this.local.workDone} onPress={() => {
                    this.completeWalk()
                }} />
            )
        } else if (this.state.request.isWalkerReached) {
            return (
                <ActionButton title={this.local.workStart} onPress={() => {
                    this.requestRef.update({
                        isWorkStarted: true
                    })
                }} />
            )
        } else if (this.state.request.isUserAccepted) {
            return (
                <ActionButton title={this.local.reached} onPress={() => {
                    this.requestRef.update({
                        isWalkerReached: true
                    })
                }} />
            )
        }
        // <ActionButton title="Work Finished" onPress={() => this.props.workDone(this.state.region, this.props.navigation)} />
    }

    checkRequest = async () => {
         
        const data = this.props.service.reserve.filter(item => item.viewstatus == 0)
        console.log("Home-> ", data.length)

        this.setState ({
            newRequest : data.length, 
        })  

    }

    render() {
        return (
            <View style={styles.container}>
                <MapView
                    {...this.panResponder.panHandlers}
                    ref={(mapView) => { this.mapView = mapView }}
                    showsUserLocation={true}
                    style={styles.map}
                    onRegionChange={this.onRegionChange}
                    initialRegion={this.state.region}>
                    {!_.isEmpty(this.state.request.destination) && <Marker
                        title={this.local.destination}
                        coordinate={{
                            latitude: parseFloat(this.state.request.destination.latitude),
                            longitude: parseFloat(this.state.request.destination.longitude)
                        }}>
                        <View style={{ backgroundColor: "rgba(255, 255, 255, .8)", borderRadius: 10, padding: 10, alignItems: 'center' }}>
                            <Icon name='ios-pin' color={colorPrimary} size={40} />
                        </View>
                    </Marker>}
                </MapView>

                {!_.isEmpty(this.state.request) ? <View style={styles.userRequestContainer}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.liveDot}>
                        </View>
                        <Text>{`${this.state.request.owner.first_name} ${this.state.request.owner.last_name}`}</Text>
                        {!this.state.request.isAccepted && <Text style={{ marginLeft: 10 }}>{`${this.state.requestTimeOut} ${this.local.secondLeft}`}</Text>}
                    </View>
                    <Text>X</Text>
                </View> : null}

                <View style={styles.toolbar}>
                    <TouchableOpacity onPress={this.openDrawer} >
                        <Icon name="menu" color={colorDarkText} />
                         { this.state.newRequest > 0 &&  <View style={styles.redlight }></View> }
                    </TouchableOpacity>
                    <TextInput
                        value={this.state.localAddress}
                        onChange={(e) => {
                            // console.log(e)
                            // this.setState({
                            //     searchQuery: e.value
                            // })
                        }}
                        style={{ flex: 1, paddingHorizontal: 10 }}
                        placeholder='Search here'
                        onSubmitEditing={this.fetchRegionFromAddress} />
                </View>

                <View style={{
                    position: 'absolute',
                    bottom: 0,
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                }}>
                    {this.state.request.isAccepted && <TouchableOpacity style={{
                        flexDirection: 'row',
                        alignSelf: 'flex-end'
                    }} onPress={() => {
                        this.props.navigation.navigate('Chat', {
                            request: this.state.request
                        })
                    }}>
                        <View
                            style={{
                                width: 60,
                                height: 60,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 100,
                                backgroundColor: colorDark,
                                margin: 8
                            }}>
                            <Icon name="ios-chatbubbles" color={colorWhite} />
                        </View>
                    </TouchableOpacity>}

                    <TouchableOpacity style={{
                        flexDirection: 'row',
                        alignSelf: 'flex-end'
                    }} onPress={() => {
                        this.openUserLocation()
                    }}>
                        <View
                            style={{
                                width: 60,
                                height: 60,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 100,
                                backgroundColor: colorDark,
                                margin: 8,
                            }}>
                            <Icon name="locate" color={colorWhite} />
                        </View>
                    </TouchableOpacity>

                    {this.state.request.isAccepted && <TouchableOpacity style={{
                        flexDirection: 'row',
                        alignSelf: 'flex-end'
                    }} onPress={() => {
                        this.openDestination()
                    }}>
                        <View
                            style={{
                                width: 60,
                                height: 60,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 100,
                                backgroundColor: colorDark,
                                margin: 8,
                            }}>
                            <Icon name="navigate" color={colorWhite} />
                        </View>
                    </TouchableOpacity>}

                    {!_.isEmpty(this.state.request) && <View style={{
                        width: '98%',
                        flexWrap: 'wrap',
                        justifyContent: 'space-around',
                        paddingHorizontal: 10,
                        alignItems: 'center',
                        paddingVertical: 10,
                        marginBottom: 10,
                        backgroundColor: colorWhite,
                        elevation: 2,
                        borderRadius: 10
                    }}>
                        {this.state.request.isAccepted ? <View style={{
                            flexDirection: 'row',
                            width: '100%',
                            justifyContent: 'space-around'
                        }}>
                            {this.renderCallToAction()}
                            {!this.state.request.isWorkDone && <ActionButton title={this.local.reject} onPress={() => {
                                this.props.rejectRequest(this.state.request.request_id)
                                this.requestRef.update({
                                    isRejected: true
                                })
                            }} />}
                        </View> : <View style={{
                            flexDirection: 'row',
                            width: '100%',
                            justifyContent: 'space-around'
                        }}>
                                <TouchableOpacity onPress={() => {
                                    clearInterval(this.timerInterval)
                                    this.isPriceEntered = false
                                    this.isTimerRunning = false
                                    this.requestRef.update({
                                        isAccepted: true
                                    })
                                    this.props.acceptRequest(this.state.request.request_id)
                                }}>
                                    <View style={styles.btnAccept}>
                                        <Text style={styles.title}>{this.local.accept}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => {
                                    clearInterval(this.timerInterval)
                                    this.isTimerRunning = false
                                    this.props.rejectRequest(this.state.request.request_id)
                                    this.requestRef.update({
                                        isRejected: true
                                    })
                                    this.setState({
                                        request: {}
                                    })
                                    this.props.declineRequest(this.state.request.request_id)
                                }}>
                                    <View style={styles.btnDecline}>
                                        <Text style={styles.title}>{this.local.decline}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>}
                    </View>}
                </View>
                {this.renderWalkThrough()}
            </View>
        )
    }
}

const mapStateToProps = ({ auth, service }) => {
    return {
        auth,
        service
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        
        createRequest: (region) => dispatch(createRequest(region)),
        restoreLoginSession: () => dispatch(restoreLoginSession()),
        logOut: () => dispatch(logOut()),
        updateLatLog: (coords) => dispatch(updateLatLog(coords)),
        acceptRequest: (id) => dispatch(acceptRequest(id)),
        declineRequest: (id) => dispatch(declineRequest(id)),
        rejectRequest: (requestId) => dispatch(rejectRequest(requestId)),
        workDone: (region, nav, ) => dispatch(workComplete(region, nav)),
        checkForPendingRequests: () => dispatch(checkForPendingRequests()),
        clearPendingRequest: () => dispatch(clearPendingRequest()),
        initPendingRequestProvider: () => dispatch(initPendingRequestProvider()),
        updateCurrentLocation: (region) => dispatch(updateCurrentLocation(region)),
        fetchReservationlist: () => dispatch(fetchReservationlist())

    }
}

const drawerNavigation = createDrawerNavigator({
    Home: {
        screen: connect(mapStateToProps, mapDispatchToProps)(HomeProvider)
    }
}, {
        contentComponent: HomeNavigationDrawer
    })

export default drawerNavigation

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    toolbar: {
        backgroundColor: 'rgba(255, 255, 255, .9)',
        height: 50,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '95%',
        flexDirection: 'row',
        color: colorWhite,
        paddingHorizontal: 10,
        elevation: 3,
        position: 'absolute',
        top: 30,
        borderRadius: 10
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    markerContainer: {
        alignSelf: 'center',
        top: '45%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    userRequestContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        zIndex: 9999999,
        width: '95%',
        backgroundColor: 'rgba(255, 255, 255, .8)',
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 20,
        elevation: 3,
        position: 'absolute',
        top: 60,
        borderRadius: 10
    },
    liveDot: {
        width: 20,
        height: 20,
        backgroundColor: colorDark,
        borderRadius: 100,
        marginRight: 10,
    },
    title: {
        fontSize: 18,
        color: colorWhite,
        letterSpacing: 1
    },
    btnAccept: {
        backgroundColor: colorPrimary,
        color: colorWhite,
        fontSize: 18,
        letterSpacing: 2,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        borderRadius: 10,
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    btnDecline: {
        backgroundColor: 'red',
        color: colorWhite,
        fontSize: 18,
        letterSpacing: 2,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        borderRadius: 10,
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    redlight:{
        backgroundColor: 'red',
        height:8,
        width:8,
        borderRadius:50,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: 'red',
        shadowOffset: { height: 0, width: 0 },
        position: 'absolute',
        top:20,
        left:20,

    }
})
