import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import { } from '../store/actions/index'
import { UPDATE_LOCALS } from '../store/actions/types'
import { locals } from '../utils/locals'
import AsyncStorage from '@react-native-community/async-storage'
import { colorWhite, colorDarkText } from '../style/colors'

class Settings extends Component {
    constructor(props) {
        super(props)
    }


    render() {
        return (
            <ScrollView style={{
                flex: 1
            }}>
                <Text style={{
                    textAlign: 'center',
                    paddingVertical: 10,
                    fontSize: 18,
                    fontWeight: 'bold',
                    backgroundColor: colorWhite,
                    elevation: 2
                }}> Settings </Text>

                {/* <View style={{
                    flex: 1,
                    width: '100%',
                    alignItems: 'center',
                    paddingVertical: 10,
                    paddingHorizontal: 10,
                    marginVertical: 10
                }}>
                    <Text style={{
                        fontSize: 16,
                        marginBottom: 5,
                        textAlign: 'left',
                        width: '100%'
                    }}>{this.local.selectLanguage}</Text>
                    {locals.Languages.map(local => (
                        <TouchableOpacity
                            key={local}
                            onPress={() => this.setDefaultLanguage(local)}
                            style={{
                                backgroundColor: colorWhite,
                                elevation: 2,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-around',
                                paddingVertical: 10,
                                paddingHorizontal: 10,
                                borderRadius: 10,
                                marginBottom: 20
                            }}>
                            <Text style={{
                                flex: 1,
                                fontSize: 18,
                                color: colorDarkText,
                                marginLeft: 10
                            }}>{local}</Text>
                            {this.state.selectedLocal === local ? <Image source={require('../../assets/selected-icon.png')} style={{
                                width: 30,
                                height: 30
                            }} /> : null}
                        </TouchableOpacity>
                    ))}
                </View> */}
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => state

const mapDispatchToProps = (dispatch) => {
    return {
        updateLocal: (local) => dispatch({
            type: UPDATE_LOCALS,
            local: local
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)