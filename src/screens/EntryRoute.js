import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    ImageBackground,
    ScrollView,
    TouchableOpacity,
    Image
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import {
    setUserType
} from '../store/actions'
import Button from '../components/buttons/Button'
import Logo from '../components/Logo/Logo'
import { colorWhite, colorDarkText, colorPrimary, colorAccent, colorLight } from '../style/colors'
import { UPDATE_LOCALS } from '../store/actions/types'
import { locals } from '../utils/locals'

class EntryRoute extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedLocal: this.props.local
        }
        this.local = locals.English.entryRoute
    }

    componentDidMount = () => {
        this.setDefaultLanguage('English')

        console.log("i am in entery Route");
    }

    componentDidUpdate = () => {
          console.log(this.props.local);
        this.local = locals[this.props.local].entryRoute
    }

    setDefaultLanguage = async (local) => {
        if (local !== this.state.selectedLocal) {
            await AsyncStorage.setItem('Local', local)
            this.props.updateLocal(local)
            this.setState({
                selectedLocal: local
            })
        }
    }

    render() {
        return (
            <ImageBackground source={require('../../assets/login_bg.png')} style={{
                width: '100%',
                height: '100%'
            }}>
                <React.Fragment>
                    <ScrollView style={styles.formContainer}>
                        <View style={styles.container}>

                            <View style={styles.logoContainer}>
                                {/* <Text style={styles.logo}>Logo</Text> */}
                                <Logo />
                            </View>

                            <View style={styles.formContainer}>
                                <Text style={{
                                    fontSize: 20,
                                    marginBottom: 20,
                                    color: colorWhite,
                                    textAlign: 'center'
                                }}>{this.local.selectLanguage}</Text>
                                {locals.Languages.map(local => (
                                    <TouchableOpacity
                                        key={local}
                                        onPress={() => this.setDefaultLanguage(local)}>
                                        <View
                                            style={{
                                                backgroundColor: colorWhite,
                                                elevation: 2,
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                justifyContent: 'space-around',
                                                paddingVertical: 10,
                                                paddingHorizontal: 10,
                                                borderRadius: 10,
                                                marginBottom: 20
                                            }}>
                                            <Text style={{
                                                flex: 1,
                                                fontSize: 18,
                                                color: colorDarkText,
                                                marginLeft: 10
                                            }}>{local}</Text>
                                            {this.state.selectedLocal === local ? <Image source={require('../../assets/selected-icon.png')} style={{
                                                width: 30,
                                                height: 30
                                            }} /> : null}
                                        </View>
                                    </TouchableOpacity>
                                ))}
                                <Button bgColor={colorAccent} title={this.local.imUser} onPress={() => this.props.setUserType('user', this.props.navigation)} style={styles.registerBtn} />
                                <Button bgColor={colorPrimary} title={this.local.imProvider} onPress={() => this.props.setUserType('provider', this.props.navigation)} style={styles.registerBtn} />
                            </View>
                        </View>
                    </ScrollView>
                </React.Fragment>
            </ImageBackground>
        )
    }
}

const mapStateToProps = ({ auth }) => {
    return auth
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateLocal: (local) => dispatch({
            type: UPDATE_LOCALS,
            local: local
        }),
        setUserType: (userType, nav) => dispatch(setUserType(userType, nav))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EntryRoute)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 10,
    },
    logoContainer: {
        position: 'relative',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        fontSize: 50,
        letterSpacing: 5,
        color: colorWhite
    },
    heading: {
        fontSize: 25,
        color: colorAccent,
        marginTop: 30,
        letterSpacing: 5,
        width: '100%',
        textAlign: 'center',
        paddingBottom: 10,
    },
    formContainer: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 10,
    },
    registerBtn: {
        marginBottom: 20,
        marginTop: 10
    },
    textWhite: {
        fontSize: 15,
        color: colorLight,
        paddingVertical: 5
    },
})
