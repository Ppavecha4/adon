import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Text,
  Image,
  FlatList
} from 'react-native'
import { Left, Body, Button, Right } from 'native-base'
import Icon from 'react-native-ionicons'
import { connect } from 'react-redux'
import { colorWhite, colorDarkText, colorPrimary } from '../style/colors'
import { fetchHistory } from '../store/actions/services'

class HistoryScreen extends Component {

  state = {
  }

  componentDidMount = () => {
    this.props.fetchHistory()
  }

  renderCard = (item) => {
    const user = this.props.auth.userType === 'user' ? item.walker : item.owner
    console.log(item)
    return (
      <View style={styles.card}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
          <Image style={styles.profileImg} source={{ uri: 'https://randomuser.me/api/portraits/men/32.jpg' }} />
          <View style={{ flex: 1, paddingLeft: 10 }}>
            <Text style={styles.profileName}>{`${user.first_name} ${user.last_name}`}</Text>
            <Text style={styles.phone}><Icon name='ios-location' /> location</Text>
          </View>
        </View>
        <View style={styles.row}>
          <Image style={{ width: 60, height: 60 }} source={require('../../assets/cash.png')} />
          <View style={{ alignItems: 'center' }}>
            <Text style={[styles.title, { color: colorDarkText }]}>{`${item.currency} ${item.main_total}`}</Text>
            <Text>Cash</Text>
          </View>
        </View>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          position: 'absolute',
          top: 5,
          right: 10
        }}>
          <View style={{ width: 10, height: 10, borderRadius: 100, backgroundColor: item.is_paid > 0 ? colorPrimary : colorDarkText }} />
          <Text style={{ color: item.is_paid > 0 ? colorPrimary : colorDarkText, fontSize: 15, marginHorizontal: 10 }}>{item.is_paid > 0 ? 'Success' : 'Payment Pending'}</Text>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.appBar} androidStatusBarColor={colorDarkText}>
          <View style={styles.header}>
            <Left>
              <Button transparent onPress={() => {
                this.props.navigation.goBack()
              }}>
                <Icon name='ios-arrow-back' color={colorDarkText} />
              </Button>
            </Left>
            <Body>
              <Text style={styles.title}>History</Text>
            </Body>
            <Right>
              <Button transparent>
                <Icon name='menu' color='transparent' />
              </Button>
            </Right>
          </View>
        </View>
        <FlatList
          data={this.props.service.histories}
          keyExtractor={item => item.id}
          renderItem={({ item }) => this.renderCard(item)}
        />
      </View>
    )
  }
}

const mapStateToProps = ({ service, auth }) => {
  return { service, auth }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchHistory: () => dispatch(fetchHistory())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%'
  },
  appBar: {
    backgroundColor: colorWhite,
    paddingHorizontal: 20,
    elevation: 1
  },
  header: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    top:30,
  },
  title: {
    fontSize: 18,
    fontWeight: '500',
    letterSpacing: 1,
    color: colorDarkText
  },
  card: {
    marginVertical: 20,
    marginHorizontal: 10,
    backgroundColor: colorWhite,
    elevation: 5,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginBottom: 10,
  },
  profileImg: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  profileName: {
    fontSize: 25,
    color: colorDarkText
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  textSimple: {
    fontSize: 22,
    letterSpacing: 1
  },
  textHighlighted: {
    fontWeight: 'bold',
    fontSize: 30,
    letterSpacing: 5
  },
})
