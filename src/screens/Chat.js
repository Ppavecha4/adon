import React, {Component} from 'react'
import {
    Image, 
    Platform, 
    StyleSheet, 
    Text, 
    TouchableOpacity, 
    View,
    FlatList,
    TextInput,
} from 'react-native'
import Icon from 'react-native-ionicons'
import {connect} from 'react-redux'
import {colorDarkText, colorLight, colorPrimary, colorWhite} from '../style/colors'
import * as firebase from 'firebase'
import ImagePicker from 'react-native-image-picker'
import ImageView from 'react-native-image-view'
import RNFetchBlob from 'react-native-fetch-blob'
import uuid4 from 'uuid/v4'
import {formateDate} from '../utils/utils'
import {locals} from '../utils/locals'
import { defaultAvatar } from '../utils/constants'


const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

class ChatScreen extends Component {

    state = {
        messages: [],
        msgInput: '',
        currentOpendImage: 0,
        isImageViewOpen: false,
        whoisTyping: null
    }
    options = {
        title: 'Select Image',
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    }
    images = []

    isTyping = false


    constructor(props) {
        super(props)
        const { navigation } = this.props
        this.local = locals[this.props.auth.local].chat
        this.request = navigation.getParam('request')
    }


    componentDidMount = () => {
        this.userId = this.props.auth.user.id
        const convId = this.request.request_id
        const userType = this.props.auth.userType
        this.databaseRef = firebase.database().ref(`chats/${userType}/${this.userId}/${this.getReceiverId()}`)
        this.databaseRef.update({
            avatar: this.getReceiverAvatar(),
            name: this.getReceiverName(),
            userId: this.getReceiverId()
        })
        this.userRef = firebase.database().ref(`chats/${userType}/${this.userId}/${this.getReceiverId()}/messages`)
        this.receiverRef = firebase.database().ref(`chats/${this.getReceiverTag()}/${this.getReceiverId()}/${this.userId}/messages`)
        this.typingRef = firebase.database().ref(`chats/typing/${convId}/whoisTyping`)
        this.userRef.on('child_added', (snapshot) => {
            const message = snapshot.val()
            this.addMessage(message)
            if (message.mediaType === 'image') {
                this.images.push({
                    id: message.id,
                    source: {
                        uri: message.mediaUrl
                    },
                    width: message.mediaConfig.width,
                    height: message.mediaConfig.height
                })
            }
        })
        this.typingRef.on('value', (snapshot) => {
            const whoisTyping = snapshot.val()
            this.setState({
                whoisTyping
            })
        })
    }

    componentWillUnmount = () => {
        if (this.typingRef && this.userRef && this.receiverRef) {
            this.typingRef.off('value')
            this.userRef.off('child_added')
        }
    }

    handleAppStateChange(appState){

        console.log("app State", appState )
    }

    getReceiverId = () => this.props.auth.userType === 'user' ? this.request.walker.id : this.request.owner.id

    getReceiverTag = () => this.props.auth.userType === 'user' ? 'provider' : 'user'

    getReceiverAvatar = () => this.props.auth.userType === 'user' ? this.request.walker.picture : this.request.owner.picture

    getReceiverName = () => this.props.auth.userType === 'user' ? this.request.walker.first_name : this.request.owner.first_name

    openImageChooser = () => {
        ImagePicker.showImagePicker(this.options, (response) => {
            console.log('Response = ', response)
            if (response.didCancel) {
                console.log('User cancelled image picker')
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error)
            } else {
                const source = {
                    uri: response.uri,
                    config: {
                        width: response.width,
                        height: response.height
                    }
                }
                this.uploadImage(source)
            }
        })
    }

    uploadImage = (image) => {
        const imgUri = image.uri
        let uploadBlob = null
        const name = uuid4()
        const mime = 'image/jpeg'
        const uploadUri = Platform.OS === 'ios' ? imgUri.replace('file://', '') : imgUri
        const imageRef = firebase.storage().ref(`/chatingImages/${name}`)

        fs.readFile(uploadUri, 'base64')
            .then(data => {
                return Blob.build(data, { type: `${mime};BASE64` })
            })
            .then(blob => {
                uploadBlob = blob
                console.log('Uploading')
                return imageRef.put(blob, { contentType: mime, name: name })
            })
            .then(() => {
                uploadBlob.close()
                return imageRef.getDownloadURL()
            })
            .then(url => {
                console.log('Upload success', url)
                image.url = url
                this.sendImage(image)
            })
            .catch(error => {
                console.log('Upload failed', error)
                alert('Enable to send image')
            })
    }

    onTyping = (text) => {
        this.setState({ msgInput: text })
        if (!this.isTyping) {
            this.isTyping = true
            this.typingRef.set(this.userId)
            setTimeout(() => {
                this.isTyping = false
                this.typingRef.remove()
            }, 3000)
        }
    }

    addMessage = (msg) => {
        this.setState(prevState => {
            prevState.messages.unshift(msg)
            return {
                messages: [...prevState.messages],
            }
        })
    }

    sendMessage = () => {
        if (this.state.msgInput !== '') {
            const lastMsg = this.state.messages[0]
            const msgId = this.state.messages.length > 0 ? lastMsg.id + 1 : 0
            const msg = {
                id: msgId,
                msg: this.state.msgInput,
                author: this.props.auth.user.id,
                time: Date.now()
            }
            this.userRef.push().set(msg)
            this.receiverRef.push().set(msg)
            this.setState({
                msgInput: ''
            })
        }
    }

    sendImage = (image) => {
        const url = image.url
        const lastMsg = this.state.messages[0]
        const msgId = lastMsg !== undefined ? lastMsg.id + 1 : 0
        const msg = {
            id: msgId,
            msg: this.state.msgInput,
            author: this.props.auth.user.id,
            time: Date.now(),
            mediaType: 'image',
            mediaUrl: url,
            mediaConfig: image.config
        }
        this.userRef.push().set(msg)
        this.receiverRef.push().set(msg)
        this.setState({
            msgInput: ''
        })
    }

    renderChatBubble = (message) => (
        <View style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: message.author === this.userId ? 'flex-end' : 'flex-start',
            paddingVertical: 10,
            paddingHorizontal: 10,
        }}>
            <View style={{
                backgroundColor: message.author === this.userId ? colorPrimary : colorDarkText,
                maxWidth: '70%',
                paddingVertical: message.mediaType === 'image' ? 0 : 10,
                paddingHorizontal: message.mediaType === 'image' ? 0 : 15,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                borderBottomLeftRadius: message.author === this.userId ? 20 : 0,
                borderBottomRightRadius: message.author === this.userId ? 0 : 20,
                overflow: 'hidden'
            }}>
                {message.mediaType === 'image' ?
                    <TouchableOpacity onPress={() => {
                        this.setState({
                            isImageViewOpen: true,
                            currentOpendImage: this.images.findIndex(image => image.id === message.id)
                        })
                    }}>
                        <Image
                            source={{ uri: message.mediaUrl }}
                            style={{
                                width: 200,
                                height: 200
                            }}
                        />
                    </TouchableOpacity> : null}
                {message.msg !== '' ? <Text style={{
                    color: colorWhite,
                    fontSize: 15,
                    marginBottom: 2,
                    textAlign: 'justify',
                    padding: message.mediaType === 'image' ? 10 : 0,
                    paddingBottom: 0
                }}>{message.msg}</Text> : null}
                <Text
                    style={{
                        fontSize: 12,
                        color: colorWhite,
                        alignSelf: 'flex-end',
                        padding: message.mediaType === 'image' ? 10 : 0
                    }}>{formateDate(message.time)}</Text>
            </View>
        </View>
    )


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.appBar} androidStatusBarColor={colorDarkText}>
                    <View style={styles.header}>
                        <View style={styles.row}>
                            <TouchableOpacity onPress={() => {
                                this.props.navigation.goBack()
                            }}>
                                <Icon name='ios-arrow-back' color={colorDarkText} />
                            </TouchableOpacity>
                            <Image style={styles.imageThumb} source={{ uri: this.getReceiverAvatar() !== '' ? this.getReceiverAvatar() : defaultAvatar }} />
                        </View>
                        <View style={styles.userInfo}>
                            <Text style={styles.title}>{this.props.auth.userType === 'user' ? this.request.walker.first_name : this.request.owner.first_name}</Text>
                            {this.state.whoisTyping !== this.userId && this.state.whoisTyping !== null ? <Text style={styles.subTitle}>{this.local.typing}</Text> : null}
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                </View>

                <FlatList
                    inverted
                    style={{
                        flex: 1,
                        backgroundColor: colorLight,
                    }}
                    extraData={this.state}
                    data={this.state.messages}
                    keyExtractor={(msg) => msg.id.toString()}
                    renderItem={({ item }) => this.renderChatBubble(item)}
                />
                <View style={
                    [styles.row,
                    {
                        justifyContent: 'space-between',
                        paddingHorizontal: 10,
                        elevation: 2,
                        backgroundColor: colorWhite,
                        marginHorizontal: 5,
                        marginVertical: 5,
                        borderRadius: 20,
                    }]
                }>
                    <TouchableOpacity onPress={this.openImageChooser}>
                        <Icon name='ios-camera' />
                    </TouchableOpacity>
                    <TextInput multiline style={{
                        flex: 1,
                        paddingHorizontal: 10,
                        fontSize: 18,
                        maxHeight: 100
                    }}
                        placeholder={this.local.typeMessage}
                        value={this.state.msgInput}
                        onChangeText={this.onTyping} />
                    <TouchableOpacity onPress={this.sendMessage}>
                        <Icon name='send' />
                    </TouchableOpacity>
                </View>

                <ImageView
                    images={this.images}
                    imageIndex={this.state.currentOpendImage}
                    isVisible={this.state.isImageViewOpen}
                    onClose={() => this.setState({
                        isImageViewOpen: false
                    })}
                 animationType='slide'/>
            </View>
        )
    }
}

const mapStateToProps = ({ auth, service }) => {
    return {
        auth,
        service
    }
}

export default connect(mapStateToProps)(ChatScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colorLight
    },
    appBar: {
        backgroundColor: colorWhite,
        paddingHorizontal: 20,
        elevation: 2,
        top:30,
    },
    header: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        top:30,
    },
    userInfo: {
        alignItems: 'center',
        marginLeft: 5
    },
    title: {
        fontSize: 18,
        fontWeight: '500',
        letterSpacing: 1,
        color: colorDarkText,
        textAlign: 'center',
        marginLeft: 10
    },
    subTitle: {
        fontSize: 12,
        color: colorPrimary,
        letterSpacing: 1,
        textAlign: 'center'
    },
    imageThumb: {
        width: 40,
        height: 40,
        borderRadius: 100,
        borderColor: colorPrimary,
        borderWidth: 2,
        marginLeft: 10,
    },
    card: {
        marginVertical: 20,
        marginHorizontal: 10,
        backgroundColor: colorWhite,
        elevation: 5,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 15,
        marginBottom: 70,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
})
