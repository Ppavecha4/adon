import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native'
import Icon from 'react-native-ionicons'
import { connect } from 'react-redux'
import { colorWhite, colorDark, colorDarkText, colorPrimary, colorLight } from '../style/colors'
import { fetchHistory } from '../store/actions/services'
import { locals } from '../utils/locals'
import ActionButton from '../components/buttons/ActionButton';

class UpgradeScreen extends Component {

    constructor(props) {
        super(props)

        this.local = locals[this.props.auth.local].upgrade
    }

    render() {
        const bulletPoint = <View style={{
            width: 10,
            height: 10,
            backgroundColor: colorDark,
            borderRadius: 100,
            marginRight: 5
        }} />
        return (
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <View style={{
                    width: '90%',
                    alignItems: 'center',
                    elevation: 2,
                    paddingVertical: 30,
                    paddingHorizontal: 20,
                    backgroundColor: colorWhite,
                    borderRadius: 10
                }}>
                    <Image source={require('../../assets/premium.png')} style={{
                        resizeMode: 'contain',
                        width: 150,
                        height: 150
                    }} />
                    <Text style={{
                        fontSize: 30,
                        fontWeight: 'bold',
                        letterSpacing: 5,
                        color: colorDark
                    }}>{this.local.premium}</Text>

                    <Text style={{
                        fontSize: 20,
                        marginTop: 10,
                        textAlign: 'center'
                    }}>{this.local.experince}</Text>

                    <View style={{
                        marginLeft: 10,
                        width: '70%',
                        marginTop: 10
                    }}>

                        {this.local.features.map(feature => <View style={{
                            flexDirection: 'row',
                            alignItems: 'center'
                        }}>
                            {bulletPoint}
                            <Text style={{
                                fontSize: 15,
                                textAlign: 'center'
                            }}>{feature}</Text>
                        </View>)}
                    </View>

                    <ActionButton title={this.local.upgrade} style={{ width: '100%', marginTop: 20 }} />

                    <TouchableOpacity style={{
                        width: '100%',
                        alignItems: 'center',
                        marginTop: 10
                    }} onPress={() => {
                        this.props.navigation.pop()
                    }}>
                        <Text style={{
                            fontSize: 18
                        }}>{this.local.noThanks}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}

const mapStateToProps = ({ service, auth }) => {
    return { auth }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpgradeScreen)
