import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    Animated,
    TouchableOpacity,
    Button,
    Easing,
    ToastAndroid,
    PanResponder,
    TextInput,
    Modal,
    Image
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import Icon from 'react-native-ionicons'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import GeoCoder from 'react-native-geocoder'
import { createDrawerNavigator } from 'react-navigation'
import { colorPrimary, colorWhite, colorAccent, colorDarkText, colorLight, colorDark } from '../style/colors'
import HomeNavigationDrawer from '../components/drawer/HomeNavigationDrawer'
import ServiceStatusProgressView from '../components/progress/ServiceStatusProgressView'
import NestedServiceList from '../components/nestedList/NestedServiceList'
import { connect } from 'react-redux'
import ActionButton from '../components/buttons/ActionButton'
import _ from 'underscore'
import { createRequest, listenForRequestProposal, clearPendingRequest, initPendingRequest } from '../store/actions/services'
import { restoreLoginSession, logOut } from '../store/actions/index'
import { logOutAction, notAllowedAction } from '../utils/navigation'
import HistoryScreen from './History'
import ProgressDialog from '../components/dialogs/ProgressDialog'
import io from 'socket.io-client'
import { locals } from '../utils/locals'

import {
    REQUEST_SEND,
    REQUEST_DECLINE,
    REQUEST_PROVIDER_NOT_FOUND,
    REQUEST_PRICE
} from '../store/actions/types'
import ProviderDetails from '../components/card/ProviderDetails'
import PriceAcceptReject from '../components/dialogs/PriceAcceptReject'
import * as firebase from 'firebase'
import { APP_SERVER_BASE_URL } from '../Server'
import { REQUEST_TIME_OUT_SEC } from '../utils/constants'
import { getRequestRemainingTime } from '../utils/utils'

class HomeScreen extends Component {

    state = {
        region: {
            latitude: 0.0,
            longitude: 0.0,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        },
        userLocation: null,
        localAddress: 'Current location',
        searchQuery: '',
        isUserRequestStatusOpen: false,
        priceType: 1,
        promoCode: '',
        isPriceModalOpen: false,
        paymentMethods: [
            {
                id: 1,
                title: 'CASH',
                icon: require('../../assets/cash.png')
            },
            // {
            //     id: 3,
            //     title: 'MERCADO PAGO',
            //     icon: require('../../assets/marcadopago.png')
            // },
            {
                id: 3,
                title: 'CREDIT/DEBIT CARD',
                icon: require('../../assets/credit-card.png')
            },
            {
                id: 2,
                title: 'PAYPAL',
                icon: require('../../assets/2019-06-19.png')
            }
              
        ],

        request: {},
        requestTimeOut: REQUEST_TIME_OUT_SEC
    }

    isTimerRunning = false

    constructor(props) {
        super(props)

        this.mapView = null

        this.local = locals[this.props.auth.local].userHome
        this.toastLocal = locals[this.props.auth.local].toast

        this.openAnimation = new Animated.Value(-100)

        this.isListeningForRequest = false

        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onStartShouldSetPanResponderCapture: () => true,
            onMoveShouldSetPanResponder: () => true,
            onMoveShouldSetPanResponderCapture: () => true,
            onPanResponderRelease: () => {
                this.fetchLocalAddress()
            }
        })

        this.socket = io.connect(APP_SERVER_BASE_URL, {
            query: {
                user: this.props.auth.user.first_name
            }
        })
        this.socket.connect()
        this.socket.on('connect', () => {
            console.log('Connected to socket')
            this.socket.emit('status', (status) => {
                if (status !== 0) {
                    this.props.navigation.dispatch(notAllowedAction)
                }
            })
        })
        this.socket.emit('status', (status) => {
            if (status !== 0) {
                this.props.navigation.dispatch(notAllowedAction)
            }
        })
    }

    componentDidMount() {

        console.log("data--",this.props.service.walkers)
        this.openLastRegion()
        this.fetchLocalAddress()
        this.checkForPendingRequests()

        navigator.geolocation.getCurrentPosition(position => {
            const region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: this.state.region.latitudeDelta,
                longitudeDelta: this.state.region.longitudeDelta
            }
            this.setState({
                userLocation: {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                }
            }, () => {
                this.mapView.animateToRegion(region, 700)
                this.fetchLocalAddress()
            })
        })
    }

    openUserRequestContainer = () => {
        Animated.spring(
            this.openAnimation,
            {
                toValue: 60,
                duration: 700,
                easing: Easing.inOut,
            }
        ).start(() => {
            this.setState({
                isUserRequestStatusOpen: true
            })
        })
    }

    closeUserRequestContainer = () => {
        Animated.spring(
            this.openAnimation,
            {
                toValue: -100,
                duration: 700,
                easing: Easing.inOut,
            }
        ).start(() => {
            this.setState({
                isUserRequestStatusOpen: false
            })
        })
    }

    componentDidUpdate() {
        if (!this.props.auth.isAuthenticated) {
            this.props.navigation.dispatch(logOutAction)
        }
        // if (this.props.service.pendingRequest !== null) {
        //     this.props.listenForRequestProposal()
        //     this.openUserRequestContainer()
        //     console.log('Got a pending request')
        //     this.props.clearPendingRequest()
        // }
    }

    componentWillUnmount = () => {
        if (this.requestRef) {
            this.requestRef.off('value')
        }
    }

    openDrawer = () => {
        this.props.navigation.openDrawer()
        

    }

    onRegionChange = (region) => {
        AsyncStorage.setItem('region', JSON.stringify(region))
        this.setState({
            region
        })
    }

    openLastRegion = async () => {
        const result = await AsyncStorage.getItem('region')
        if (result) {
            this.setState({
                region: JSON.parse(result)
            })
        }
    }

    fetchLocalAddress = async () => {
        const result = await GeoCoder.geocodePosition({
            lat: this.state.region.latitude,
            lng: this.state.region.longitude
        })
        this.setState({
            localAddress: result[0].formattedAddress
        })
    }

    fetchRegionFromAddress = async () => {
        console.log(this.state)
        const result = await GeoCoder.geocodeAddress(this.state.searchQuery)
        console.log(result)
    }

    openUserLocation = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                const region = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: this.state.region.latitudeDelta,
                    longitudeDelta: this.state.region.longitudeDelta
                }
                this.setState({
                    region
                })
                this.mapView.animateToRegion(region, 700)
                console.log('Current Lat Lng', position)
            },
            error => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 })
    }

    startRequestTimer = () => {
        if (!this.isTimerRunning) {
            this.timerInterval = setInterval(() => {
                if (!_.isEmpty(this.state.request)) {
                    const time = getRequestRemainingTime(this.state.request)
                    this.setState({
                        requestTimeOut: time
                    })
                    if (this.state.requestTimeOut <= 0) {
                        this.props.requestCancel()
                        this.isTimerRunning = false
                        clearInterval(this.timerInterval)
                    }
                }
            }, 1000)
            this.isTimerRunning = true
        }
    }

    listenForRequestProposal = async () => {
        if (!this.isListeningForRequest) {
            console.log('Listening for request', this.props.service )
            this.requestRef = firebase.database().ref(`requests/${this.props.service.walker.id}/${this.props.service.requestId}`)
            this.requestRef.on('value', snapshot => {
                const request = snapshot.val()
                console.log('Child Changed at HomeScreen')
                if (request) {
                    if (request.isRejected) {
                        ToastAndroid.show(this.toastLocal.requestRejected, ToastAndroid.LONG)
                        clearInterval(this.timerInterval)
                        this.isTimerRunning = false
                        this.props.requestCancel()
                        this.requestRef.off('value')
                        this.isListeningForRequest = false
                        this.closeUserRequestContainer()
                        this.setState({
                            request: {}
                        })
                    } else if (request.isCompleted) {
                        ToastAndroid.show(this.toastLocal.workCompleted, ToastAndroid.LONG)
                        this.setState({
                            request: {}
                        })
                        this.closeUserRequestContainer()
                        this.props.requestCancel()
                    } else {
                        console.log("Got Request", request)
                        this.setState({
                            request: request
                        })
                        if (request.isUserAccepted) {
                            this.openUserRequestContainer()
                        }
                    }
                }
            })
            this.isListeningForRequest = true
        }
    }

    checkForPendingRequests() {
        const pendingRef = firebase.database().ref(`pendingRequests/${this.props.auth.user.id}`)
        pendingRef.on('value', (snapshot) => {
            const pendingRequest = snapshot.val()
            if (pendingRequest) {
                const pendingRequestRef = firebase.database().ref(`requests/${pendingRequest.walkerId}/${pendingRequest.request_id}`)
                pendingRequestRef.on('value', snapshot => {
                    const request = snapshot.val()
                    if (request) {
                        if (request.isRejected) {
                            pendingRef.remove()
                            pendingRef.off('value')
                            pendingRequestRef.off('value')
                        } else {
                            if (!request.isCompleted) {
                                console.log("Got pending request")
                                this.props.initPendingRequest(request.request_id, request.walker)
                                this.setState({
                                    request: request
                                })
                                if (request.isUserAccepted) {
                                    this.openUserRequestContainer()
                                }
                            }
                        }
                    } else {
                        pendingRef.remove()
                        pendingRef.off('value')
                        pendingRequestRef.off('value')
                    }
                })
            }
        })
    }

    getCurrentRequestStatus = () => {
        if (this.state.request.isWorkDone) {
            return 5
        } else if (this.state.request.isWorkStarted) {
            return 4
        } else if (this.state.request.isWalkerReached) {
            return 3
        } else if (this.state.request.isUserAccepted) {
            return 2
        } else if (this.state.request.isAccepted) {
            return 1
        } else {
            return 0
        }
    }
    myCallback = (dataFromChild) => {
            
             this.setState({
                    selectedSubService: dataFromChild
                  })
           // console.log("Service Name ===> ", dataFromChild);
        }

    renderWalkThrough = () => {
        const requestStatus = this.props.service.requestStatus

        if (this.state.request.isWorkDone) {
            this.props.navigation.navigate('Invoice', {
                request: this.state.request,
                requestRef: this.requestRef
            })
            return null
        } else if (this.state.request.price && !this.state.request.isUserAccepted) {
            return <PriceAcceptReject
                show
                price={this.state.request.price}
                onDone={() => {
                    this.requestRef.update({
                        isUserAccepted: true
                    })
                }}
                onCancel={() => {
                    this.requestRef.update({
                        isUserAccepted: false
                    })
                }} />
        } else if (this.state.request.isAccepted) {
            clearInterval(this.timerInterval)
            this.isTimerRunning = false
            if (!this.state.isUserRequestStatusOpen) {
                return <ProviderDetails
                    onChat={() => this.props.navigation.navigate('Chat', {
                        request: this.state.request
                    })}
                    onClose={() => {
                        this.setState({
                            isUserRequestStatusOpen: true
                        })
                    }} />
            }
            return null
        } else if (requestStatus === REQUEST_PROVIDER_NOT_FOUND) {
            return (
                <>
                    <View style={styles.userRequestContainer}>
                        <Text>{this.local.noProvider}</Text>
                    </View>
                    <View style={styles.markerContainer}>
                        {/* {this.props.service.userSelectedService && <Button title={this.local.bookNow} onPress={() => {
                            this.setState({
                                isPriceModalOpen: true
                            })
                        }} />} */}
                        {this.props.service.userSelectedService ? <ActionButton title={this.local.bookNow} onPress={() => {
                            this.setState({
                                isPriceModalOpen: true
                            })
                        }} /> : <Icon name='locate' size={45} color={colorPrimary} />}
                    </View>
                </>
            )
        } else if (requestStatus === REQUEST_SEND) {
            this.listenForRequestProposal()
            this.startRequestTimer()
            return (
                <ProgressDialog show title={this.local.contactingProvider} timeLeft={this.state.requestTimeOut} />
            )
        } else {
            return (
                <View style={styles.markerContainer}>
                    {/* {this.props.service.userSelectedService && <Button title={this.local.bookNow} onPress={() => {
                            this.setState({
                                isPriceModalOpen: true
                            })
                        }} />} */}
                    {this.props.service.userSelectedService ? <ActionButton title={this.local.bookNow} onPress={() => {
                        this.setState({
                            isPriceModalOpen: true
                        })
                    }} /> : <Icon name='locate' size={45} color={colorPrimary} />}
                </View>
            )
        }
    }

    renderPriceModal = () => {
        return (
            <Modal
                animationType='slide'
                visible={this.state.isPriceModalOpen}
                onRequestClose={() => {
                    this.setState({
                        isPriceModalOpen: false
                    })
                }}>
                <View style={{
                    width: '100%',
                    height: 50,
                    backgroundColor: colorWhite,
                    justifyContent: 'center',
                    alignItems: 'center',
                    elevation: 2,
                }}>
                    <Text style={{
                        fontSize: 18,
                        letterSpacing: 1
                    }}>{this.local.choosePaymentMethod}</Text>
                </View>
                <View
                    style={{
                        width: '100%',
                        backgroundColor: colorLight,
                        paddingHorizontal: 5,
                        paddingVertical: 10
                    }}>
                    <TextInput placeholder={this.local.promocode} />
                </View>
                <View style={{
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    height: '100%',
                    flex: 1,
                    justifyContent: 'space-between'
                }}>
                    <View style={{
                        justifyContent: 'space-between'
                    }}>
                        <Text style={{
                            color: colorDarkText,
                            fontSize: 18,
                            marginBottom: 20
                        }}>{this.local.selectPayment}</Text>
                        {this.state.paymentMethods.map(method => (
                            <TouchableOpacity
                                key={method.id}
                                onPress={() => this.setState({
                                    priceType: method.id
                                })}>
                                <View
                                    style={{
                                        backgroundColor: colorWhite,
                                        elevation: 2,
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-around',
                                        paddingVertical: 10,
                                        paddingHorizontal: 10,
                                        borderRadius: 10,
                                        marginBottom: 20
                                    }}>
                                    <Image source={method.icon} style={{
                                        width: 50,
                                        height: 60,
                                        resizeMode: 'center'
                                    }} />
                                    <Text style={{
                                        flex: 1,
                                        fontSize: 18,
                                        color: colorDarkText,
                                        marginLeft: 10
                                    }}>{method.title}</Text>
                                    {this.state.priceType === method.id ? <Image source={require('../../assets/selected-icon.png')} style={{
                                        width: 30,
                                        height: 30
                                    }} /> : null}
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                    <TouchableOpacity onPress={() => {
                        this.setState({
                            isPriceModalOpen: false,
                            requestTimeOut: REQUEST_TIME_OUT_SEC
                        })
                        console.log('Creating Request')
                        this.props.createRequest(this.state.region, this.state.priceType, this.socket, this.state.userLocation)
                    }}>
                        <View style={{
                            width: '100%',
                            height: 50,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: colorDark
                        }}>
                            <Text style={{
                                fontSize: 18,
                                color: colorWhite,
                                letterSpacing: 1
                            }}>{this.local.done}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </Modal>
        )
    }

    render() {
        const opacity = this.openAnimation.interpolate({
            inputRange: [-100, 60],
            outputRange: [0, 1]
        })
        const opacityReverse = this.openAnimation.interpolate({
            inputRange: [-100, 60],
            outputRange: [1, 0]
        })
        const iconPositionY = this.openAnimation.interpolate({
            inputRange: [-100, 60],
            outputRange: [0, 60]
        })
        return (
            <View style={styles.container}>
                <MapView
                    {...this.panResponder.panHandlers}
                    ref={(mapView) => { this.mapView = mapView }}
                    showsUserLocation={true}
                    style={styles.map}
                    onRegionChange={this.onRegionChange}
                    initialRegion={ this.state.region}>
                    {!_.isEmpty(this.state.request) && <Marker
                        title={this.state.request.walker.first_name}
                        coordinate={{
                            latitude: parseFloat(this.state.request.walker.latitude ? this.state.request.walker.latitude:this.state.region.latitudeDelta),
                            longitude: parseFloat(this.state.request.walker.longitude ? this.state.request.walker.longitude:this.state.region.longitude)
                        }}>
                        <View style={{ backgroundColor: 'rgba(255, 255, 255, .8)', borderRadius: 10, padding: 10, alignItems: 'center' }}>
                            <Icon name='ios-pin' color={colorPrimary} size={40} />
                        </View>
                    </Marker>}
                    {this.props.service.walkers.map(walker => (
                        <Marker
                            key={walker.id}
                            title={walker.first_name}
                            coordinate={{
                                latitude: parseFloat(walker.latitude),
                                longitude: parseFloat(walker.longitude)
                            }}>
                            <View style={{ backgroundColor: "white", borderRadius: 10, padding: 10, alignItems: 'center' }}>
                                <Text>{walker.first_name}</Text>
                                <Icon name='ios-pin' color={colorPrimary} size={20} />
                            </View>
                        </Marker>
                    ))}
                </MapView>

                {this.renderWalkThrough()}

                <Animated.View style={[styles.userRequestContainer, { top: this.openAnimation, opacity }]}>
                    <ServiceStatusProgressView
                        status={this.getCurrentRequestStatus()}
                        onClose={() => this.closeUserRequestContainer()} />
                </Animated.View>

                <View style={styles.toolbar}>
                    <TouchableOpacity onPress={this.openDrawer} >
                        <Icon name="menu" color={colorDarkText} />
                    </TouchableOpacity>
                    <TextInput
                        value={this.state.localAddress}
                        onChange={(e) => {
                            // console.log(e)
                            // this.setState({
                            //     searchQuery: e.value
                            // })
                        }}
                        style={{ flex: 1, paddingHorizontal: 10 }}
                        placeholder='Search here'
                        onSubmitEditing={this.fetchRegionFromAddress} />
                    <Animated.View style={{ top: iconPositionY, opacity: opacityReverse }}>
                        <TouchableOpacity onPress={this.openUserRequestContainer} >
                            <Icon name='ios-arrow-down' />
                        </TouchableOpacity>
                    </Animated.View>
                </View>

                <View style={{
                    position: 'absolute',
                    bottom: 0,
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                }}>
                    {this.state.request.isAccepted && <TouchableOpacity style={{
                        flexDirection: 'row',
                        alignSelf: "flex-end"
                    }} onPress={() => {
                        this.props.navigation.navigate('Chat', {
                            request: this.state.request
                        })
                    }}>
                        <View
                            style={{
                                width: 60,
                                height: 60,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 100,
                                backgroundColor: colorDark,
                                margin: 8
                            }}>
                            <Icon name="ios-chatbubbles" color={colorWhite} />
                        </View>
                    </TouchableOpacity>}

                    <TouchableOpacity style={{
                        flexDirection: 'row',
                        alignSelf: "flex-end",
                    }} onPress={() => {
                        this.openUserLocation()
                    }}>
                        <View
                            style={{
                                width: 60,
                                height: 60,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 100,
                                backgroundColor: colorDark,
                                margin: 8
                            }}>
                            <Icon name="locate" color={colorWhite} />
                        </View>
                    </TouchableOpacity>

                    {_.isEmpty(this.state.request) && <View style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'center',
                        paddingVertical: 10,
                        backgroundColor: colorLight,
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15
                    }}>
                        <NestedServiceList callbackFromParent={this.myCallback} />
                    </View>}
                </View>
                {this.renderPriceModal()}
            </View>
        )
    }
}

const mapStateToProps = ({ auth, service }) => {
    return {
        auth,
        service
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createRequest: (region, priceType, socket, userLocation) => dispatch(createRequest(region, priceType, socket, userLocation)),
        requestCancel: () => dispatch({
            type: REQUEST_DECLINE
        }),
        initPendingRequest: (requestId, walker) => dispatch(initPendingRequest(requestId, walker)),
        restoreLoginSession: () => dispatch(restoreLoginSession()),
        logOut: () => dispatch(logOut()),
        listenForRequestProposal: () => dispatch(listenForRequestProposal()),
        clearPendingRequest: () => dispatch(clearPendingRequest()),
        showServicePrice: (price) => dispatch({
            type: REQUEST_PRICE,
            price: price,
            serviceStatus: 2
        })
    }
}

const drawerNavigation = createDrawerNavigator({
    Home: {
        screen: connect(mapStateToProps, mapDispatchToProps)(HomeScreen),
    }}, {
        contentComponent: HomeNavigationDrawer
    })

export default drawerNavigation

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    toolbar: {
        backgroundColor: 'rgba(255, 255, 255, .9)',
        height: 50,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '95%',
        flexDirection: 'row',
        color: colorWhite,
        paddingHorizontal: 10,
        elevation: 3,
        position: 'absolute',
        top: 30,
        borderRadius: 10
    },
    title: {
        fontSize: 18,
        color: colorDarkText,
        letterSpacing: 1
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    markerContainer: {
        alignSelf: 'center',
        top: '45%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    userRequestContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '95%',
        backgroundColor: 'rgba(255, 255, 255, .9)',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10,
        elevation: 1,
        position: 'absolute',
        top: 60,
        borderRadius: 10
    },
    liveDot: {
        width: 20,
        height: 20,
        backgroundColor: colorAccent,
        borderRadius: 100,
        marginRight: 10,
    }
})
