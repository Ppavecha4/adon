import React from 'react'
import { View, StyleSheet, TextInput, Text } from 'react-native'
import Icon from 'react-native-ionicons'
import { colorDark, colorWhite, colorDarkText } from '../../style/colors'

const TextFieldIcon = (props) => {
    return (
        <View>
            <View style={{ ...styles.searchSection, ...props.style }}>
                <Icon style={styles.searchIcon} name={props.icon} size={20} color="#000" />
                <TextInput
                    editable={props.editable == undefined}
                    secureTextEntry={props.secureTextEntry}
                    keyboardType={props.keyboardType}
                    value={props.value}
                    style={styles.input}
                    placeholder={props.placeholder}
                    underlineColorAndroid="transparent"
                    placeholderTextColor={colorDarkText}
                    onChangeText={props.onChange}
                />
            </View>
            {props.error && <View style={styles.errorContainer}>
                <Text style={styles.errorMsg}>{props.error}</Text>
            </View>}
        </View>
    )
}

const styles = StyleSheet.create({
    searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colorDark,
        borderRadius: 50,
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
    searchIcon: {
        padding: 10,
        color: colorWhite,
        marginRight: 20,
        marginLeft: 10,
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        color: colorWhite,
        fontSize: 15,
        letterSpacing: 1
    },
    errorContainer: {
        backgroundColor: colorWhite,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 100,
        opacity: .9
    },
    errorMsg: {
        fontSize: 16,
        color: 'red',
        textAlign: 'center'
    }
})

export default TextFieldIcon
