import React from 'react'
import {
    View,
    TextInput,
    Text,
    Modal,
    StyleSheet,
    TouchableOpacity,
    FlatList,
    ActivityIndicator,
    Keyboard
} from 'react-native'
import Icon from 'react-native-ionicons'
import { colorWhite } from '../../style/colors'
import {locals} from '../../utils/locals'
import { connect } from 'react-redux'
import axios from 'axios'
import { GOOGLE_PLACE_API_KEY } from '../../utils/constants'

class GooglePlacesInput extends React.Component {
    state = {
        results: [],
        query: '',
        isLoading: false
    }

    loadAddresses = async (query) => {
        this.setState({
            isLoading: true
        })
         const response = await axios.get(`https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${GOOGLE_PLACE_API_KEY}&input=${query}`)
        //const response = await axios.get(`https://maps.googleapis.com/maps/api/js?key=${GOOGLE_PLACE_API_KEY}&libraries=places`)
       
        const results = response.data.predictions
        console.log(response)
        this.setState({
            results,
            isLoading: false
        })
    }

    render() {
        return (
            <Modal
                animationType='slide'
                onRequestClose={() => this.props.onDone('')}
                visible={this.props.show}>
                <View style={{
                    width: '100%',
                    height: 50,
                    backgroundColor: colorWhite,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    elevation: 3,
                    paddingHorizontal: 10
                }}>
                    <TouchableOpacity onPress={() => this.props.onDone('')}>
                        <Icon name="ios-arrow-back" />
                    </TouchableOpacity>
                    <TextInput
                        value={this.state.query}
                        onChangeText={query => {
                            this.setState({ query })
                            this.loadAddresses(query)
                        }}
                        autoFocus
                        placeholder={locals[this.props.local].auth.address}
                        style={{
                            flex: 1,
                            paddingHorizontal: 10,
                            textAlign: 'center'
                        }} />
                    <TouchableOpacity onPress={() => this.props.onDone(this.state.query)}>
                        <Icon name="checkmark" />
                    </TouchableOpacity>
                </View>
                <View style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    {this.state.isLoading ?
                        <ActivityIndicator size='large' /> : <FlatList
                            extraData={this.state}
                            data={this.state.results}
                            keyExtractor={item => item.id}
                            renderItem={({ item }) => (
                                <TouchableOpacity
                                    onPress={() => this.props.onDone(item.description)}
                                    style={{
                                        paddingVertical: 15,
                                        paddingHorizontal: 10,
                                        borderBottomWidth: .2
                                    }}>
                                    <Text numberOfLines={1}>{item.description}</Text>
                                </TouchableOpacity>
                            )} />}
                </View>
            </Modal>
        )
    }
}

const mapStateToProps = ({auth}) => {
    return auth
}

export default connect(mapStateToProps, null)(GooglePlacesInput)