import React from 'react'
import { View, Image } from 'react-native'

const logo = (props) => (
    <View>
        <Image
            source={require('../../../assets/logo.png')}
            style={{
                width: 200,
                resizeMode: 'contain'
            }}
        />
    </View>
)

export default logo