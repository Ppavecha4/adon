import React, { Component } from 'react'
import { Text, View, FlatList, Image, ActivityIndicator, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import { connect } from 'react-redux'
import {
    fetchServices,
    fetchSubServices,
    selectService,
    toggleServiceSelection
} from '../../store/actions/services'
import { colorPrimary, colorWhite, colorAccent } from '../../style/colors'
import Icon from 'react-native-ionicons'
import { locals } from '../../utils/locals'

class NestedServiceList extends Component {
    state = {
        selectedService: null,
        selectedSubService: null,
        x:100,
        y:0,
    }

    constructor(props) {
        super(props)

        this.local = locals[this.props.auth.local].auth
    }

    componentDidMount() {
        this.props.fetchServices()
    }

    toggleServiceSelection = (item) => {
        this.props.toggleServiceSelection(item.id)

        console.log("item.sub_type_name", item.sub_type_name)
        this.props.callbackFromParent(item);
    }

    leftscroll = () => {

        this.setState({ x: this.state.x + 50 })

        console.log( "left ===> ", this.state.x)

        this.flatList.scrollToOffset({ offset: this.state.x,animated: true })
    }

    rightscroll = () => {

        this.flatList.scrollToOffset({ offset: 0, animated: true })
      
    }
    
    renderView = () => {
        if (this.state.selectedService) {
            return (this.props.service.subServices[this.state.selectedService] ?

                
                <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={true}
                    extraData={this.state}
                    ref={flatList => { this.flatList = flatList }}
                    data={this.props.service.subServices[this.state.selectedService]}
                    keyExtractor={(item) => `${item.id}`}

                    renderItem={({ item }) => (
                        <TouchableWithoutFeedback onPress={() => this.toggleServiceSelection(item)}>
                            <View
                                style={{
                                    alignItems: 'center',
                                    width: 100,
                                    marginTop: 10,
                                    justifyContent: 'space-around'
                                }}>
                                <View style={{
                                    padding: 10,
                                    backgroundColor: this.props.service.userSelectedService === item.id ? colorAccent : colorWhite,
                                    borderRadius: 100,
                                    elevation: 2
                                }}>
                                    <Image tintColor={this.props.service.userSelectedService !== item.id ? colorAccent : colorWhite} source={{ uri: item.icon }}
                                        style={{
                                            width: 70,
                                            height: 70,
                                        }} />

                                </View>
                                <Text style={{
                                    textAlign: 'center',
                                    fontSize: 12
                                }}>{item.sub_type_name}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    )} /> : <ActivityIndicator size='large' color={colorPrimary} />
            )
        } else {
            return (
                this.props.service.services.length > 0 ?
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        extraData={this.state}
                         ref={flatList => { this.flatList = flatList }}
                        scrollToOffset = {{ animated: true, offset: this.state.x }}
                        data={this.props.service.services}
                        keyExtractor={(item) => `${item.id}`}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.fetchSubServices(item.id)
                                    this.setState({
                                        selectedService: item.id
                                    })
                                }}>
                                <View
                                    style={{
                                        alignItems: 'center',
                                        width: 100,
                                        marginTop: 5,
                                        justifyContent: 'space-around'
                                    }}>
                                    <Image source={{ uri: item.icon }} style={{
                                        width: 90,
                                        height: 90,
                                        borderRadius: 45,
                                        borderColor: colorWhite,
                                        borderWidth: 2
                                    }} />
                                    <Text style={{
                                        textAlign: 'center',
                                        fontSize: 12,
                                        marginTop: 10
                                    }}>{item.name}</Text>
                                </View>
                            </TouchableOpacity>
                        )} /> : <ActivityIndicator size='large' color={colorPrimary} />
            )
        }
    }

    render() {
        return (
            <View style={{
                width: '100%'
            }}>
                <View style={{
                    paddingHorizontal: 10
                }}>
                    {this.state.selectedService ?
                        <TouchableOpacity onPress={() => this.setState({ selectedService: null })}>
                            <Icon name='ios-arrow-back' />
                        </TouchableOpacity>
                        : <Text style={{
                            fontSize: 18,
                            fontWeight: 'bold',
                            textAlign: 'center'
                        }}> {this.local.selectServiceType} </Text>}
                </View>
                
                {this.renderView()}
                <View>
                   <TouchableOpacity onPress={() => this.leftscroll()} style={{ position: 'absolute', right: 0, bottom:30, height:80,width:30,backgroundColor:'#ccccccc7', justifyContent: 'center',
    alignItems: 'center' }}>
                      <Icon name='ios-arrow-forward' color="#000" />
                   </TouchableOpacity>
                   <TouchableOpacity onPress={() => this.rightscroll()} style={{ position: 'absolute', left: 0, bottom:30, height:80,width:30,backgroundColor:'#ccccccc7', justifyContent: 'center',
    alignItems: 'center' }}>
                    <Icon name='ios-arrow-back' color="#000" />
                   </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const { service, auth } = state
    return {
        service,
        auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchServices: () => dispatch(fetchServices()),
        fetchSubServices: (id) => dispatch(fetchSubServices(id)),
        selectService: (id) => dispatch(selectService(id)),
        toggleServiceSelection: (id) => dispatch(toggleServiceSelection(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NestedServiceList)
