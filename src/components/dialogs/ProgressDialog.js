import React from 'react'
import { View, StyleSheet, ActivityIndicator, Text } from 'react-native'
import { colorPrimary, colorDarkText } from '../../style/colors'

const ProgressDialog = (props) => {
    let dialog = null
    if (props.show) {
        dialog = (<View style={styles.indicator}>
            <ActivityIndicator size='large' color={colorPrimary} />
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.timer}>{props.timeLeft}</Text>
        </View>)
    }
    return dialog
}

const styles = StyleSheet.create({
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .8)',
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 99999999,
    },
    title: {
        fontSize: 22,
        color: colorDarkText,
        letterSpacing: 2,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    timer: {
        fontSize: 50,
        color: 'rgba(0, 0, 0, .8)',
        fontWeight: 'bold',
        textAlign: 'center',
        letterSpacing: 5
    }
})



export default ProgressDialog