import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { colorPrimary, colorDarkText, colorWhite, colorDark } from '../../style/colors'
import { connect } from 'react-redux'
import { rejectRequest } from '../../store/actions/services'
import { locals } from '../../utils/locals'

const PriceAcceptReject = (props) => {
    let dialog = null
    if (props.show) {
        const local = locals[props.local].priceAcceptReject
        dialog = (
            <View style={styles.backdrop}>
                <View style={styles.indicator}>
                    <Text style={{
                        width: '100%',
                        textAlign: 'center',
                        fontSize: 30,
                        color: colorPrimary,
                        letterSpacing: 2,
                    }}>{local.serviceCharge}</Text>
                    <Text style={{
                        fontSize: 35,
                        fontWeight: 'bold',
                        textAlign: 'center',
                    }}>{`$${props.price}`}</Text>

                    <TouchableOpacity
                        style={[styles.button, styles.buttonAccept]}
                        onPress={props.onDone}>
                        <Text style={styles.buttonText}>{local.accept}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={[styles.button, styles.buttonReject]}
                        onPress={props.onCancel}>
                        <Text style={styles.buttonTextBlack}>{local.reject}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    return dialog
}

const styles = StyleSheet.create({
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, .2)',
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colorWhite,
        width: '95%',
        paddingHorizontal: 20,
        paddingVertical: 10,
        elevation: 3,
        borderRadius: 10,
    },
    title: {
        fontSize: 22,
        color: colorDarkText,
        letterSpacing: 2,
        fontWeight: 'bold',
    },
    input: {
        width: '100%',
        paddingHorizontal: 20,
        borderWidth: 0.5,
        borderRadius: 10,
        marginVertical: 10,
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        paddingVertical: 10,
        borderRadius: 50,
        marginTop: 10,
    },
    buttonAccept: {
        backgroundColor: colorDark,
    },
    buttonReject: {
        backgroundColor: colorWhite,
        borderWidth: 1
    },
    buttonText: {
        color: colorWhite,
        fontSize: 20,
    },
    buttonTextBlack: {
        color: colorDark,
        fontSize: 20
    }
})

const mapStateToProps = ({ auth }) => {
    return auth
}

const mapDispatchToProps = (dispatch) => {
    return {
        rejectRequest: () => dispatch(rejectRequest())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PriceAcceptReject)