import React from 'react'
import { View, StyleSheet, TextInput, KeyboardAvoidingView, Text } from 'react-native'
import { colorPrimary, colorDarkText, colorWhite } from '../../style/colors'
import Button from '../buttons/Button'
import { connect } from 'react-redux'
import { locals } from '../../utils/locals'

class PriceDialog extends React.Component {
    state = {
        price: 0
    }
    render() {
        let dialog = null
        if (this.props.show) {
            const local = locals[this.props.local].priceDialog
            dialog = (<View style={styles.backdrop}><View style={styles.indicator}>
                <KeyboardAvoidingView behavior='padding' style={{ flex: 1, width: '100%' }}>
                    <Text style={{
                        fontSize: 50,
                        color: colorDarkText,
                        textAlign: 'center'
                    }}>$</Text>
                    <TextInput
                        keyboardType='numeric'
                        style={styles.input}
                        onChangeText={(text) => this.setState({ price: text })}
                        placeholder={local.enterPrice} />
                    <Button title={local.confirm} bgColor={colorPrimary} onPress={() => {
                        this.props.onDone(this.state.price)
                    }} />
                </KeyboardAvoidingView>
            </View></View>)
        }

        return dialog
    }
}

const styles = StyleSheet.create({
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, .5)',
        flex: 1,
        zIndex: 9999999,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    indicator: {
        backgroundColor: colorWhite,
        width: '95%',
        height: 200,
        paddingHorizontal: 20,
        paddingVertical: 10,
        elevation: 3,
        borderRadius: 10
    },
    title: {
        fontSize: 22,
        color: colorDarkText,
        letterSpacing: 2,
        fontWeight: 'bold',
    },
    input: {
        width: '100%',
        paddingHorizontal: 20,
        borderWidth: 0.5,
        borderRadius: 10,
        marginVertical: 10,
    }
})

const mapStateToProps = ({auth}) => {
    return auth
}

export default connect(mapStateToProps, null)(PriceDialog)