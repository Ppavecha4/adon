import React, { Component } from 'react'
import { Text, View, StyleSheet, FlatList, TouchableWithoutFeedback, ActivityIndicator, Image } from 'react-native'
import { Button } from 'native-base'
import { colorAccent, colorWhite, colorDarkText, colorPrimary } from '../../style/colors'
import { connect } from 'react-redux'
import { fetchSubServices } from '../../store/actions/services'

class MultiSelectDialog extends Component {

    constructor(props) {
        super(props)

        this.state = {
            selectedServices: props.servicesTypes ? [...props.servicesTypes] : []
        }
    }

    componentDidMount() {
        this.props.fetchSubServices(this.props.serviceId)
    }

    onServiceSelect = (id) => {
        let ids = []
        if (this.isSelected(id)) {
            ids = this.state.selectedServices.filter(i => i !== id)
        } else {
            ids = this.state.selectedServices.concat(id)
        }
        this.setState({
            selectedServices: ids
        })
    }

    isSelected = (id) => this.state.selectedServices.findIndex((_, i) => _ === id) !== -1

    renderListItem = (service) => (
        <TouchableWithoutFeedback onPress={() => this.onServiceSelect(service.id)}>
            <View style={styles.itemContainer}>
                <View style={{
                    padding: 10,
                    backgroundColor: this.isSelected(service.id) ? colorAccent : colorWhite,
                    borderRadius: 100,
                    elevation: 2
                }}>
                    <Image tintColor={!this.isSelected(service.id) ? colorAccent : colorWhite} source={{ uri: service.icon }}
                        style={{
                            width: 70,
                            height: 70,
                        }} />

                </View>
                <Text style={{
                    color: service.isSelected ? colorAccent : colorDarkText
                }}>{service.sub_type_name}</Text>
            </View>
        </TouchableWithoutFeedback>
    )


    render() {
        return (
            <View style={styles.backdrop} >
                <View style={styles.model}>
                    <Text style={styles.heading}>{this.props.services.find(service => service.id === this.props.serviceId).name}</Text>
                    {this.props.subServices[this.props.serviceId] ?
                        <FlatList
                            extraData={this.state}
                            numColumns={3}
                            style={styles.listView}
                            data={this.props.subServices[this.props.serviceId]}
                            renderItem={(item) => this.renderListItem(item.item)}
                            keyExtractor={item => item.id.toString()}
                        /> : <View style={styles.indicator}>
                            <ActivityIndicator size='large' color={colorPrimary} />
                        </View>}
                    <Button
                        rounded
                        style={{
                            width: '100%',
                            color: colorWhite,
                            backgroundColor: colorAccent,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }} onPress={() => this.props.onDone(this.state.selectedServices)}>
                        <Text style={{
                            color: colorWhite,
                            fontSize: 18
                        }}>Done</Text>
                    </Button>
                </View>
            </View>
        )
    }
}

const mapStateToProps = ({ service }) => {
    return service
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSubServices: (id) => dispatch(fetchSubServices(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MultiSelectDialog)

const styles = StyleSheet.create({
    backdrop: {
        position: 'absolute',
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    model: {
        width: '80%',
        height: '70%',
        backgroundColor: colorWhite,
        borderRadius: 20,
        elevation: 10
    },
    heading: {
        marginVertical: 20,
        color: colorAccent,
        letterSpacing: 2,
        fontSize: 20,
        textAlign: 'center'
    },
    listView: {
        flex: 1,
        marginBottom: 10
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'column',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .8)',
        width: '100%',
        height: '100%',
        zIndex: 999,
        borderRadius: 10,
    }
})
