import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-ionicons'
import { colorPrimary, colorDarkText } from '../../style/colors'
import { locals } from '../../utils/locals'

class ServiceStatusProgressView extends Component {
  constructor(props) {
    super(props)
    this.local = locals[this.props.auth.local].requestStatus
  }

  renderHeader = () => (
    <View style={{
      flexDirection: 'row',
      justifyContent: 'space-between',
      flex: 1,
      alignItems: 'center'
    }}>
      <Text>{this.local.status}</Text>
      <TouchableOpacity onPress={this.props.onClose}>
        <Icon name='ios-arrow-up' />
      </TouchableOpacity>
    </View>
  )

  renderDot = (i) => {
    return (
      <View style={{
        width: 20,
        height: 20,
        backgroundColor: this.getHighlightColor(i),
        borderRadius: 100
      }} />
    )
  }

  getHighlightColor = (id) => {
    return this.props.status >= id ? colorPrimary : 'rgba(0, 0, 0, .3)'
  }

  renderStatusView = (id, title, notSmallDots) => {
    return (
      <View key={id}>
        <View style={{flexDirection: 'row'}}>
          {this.renderDot(id)}
          <Text style={{marginLeft: 10}}>{title}</Text>
        </View>
        {!notSmallDots ? <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View>
          {[1, 2, 3, 4, 5].map(i => (
            <View key={i} style={{backgroundColor: this.getHighlightColor(id), borderRadius: 100, width: 5, height: 5, marginLeft: 7.5}} />
          ))}
          </View>
          <View style={{borderBottomColor: colorDarkText, borderBottomWidth: .5, height: 2, flex: 1, marginLeft: 10}}>
          </View>
        </View> : null}
      </View>
    )
  }


  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderHeader()}
        {this.renderStatusView(1, this.local.accept)}
        {this.renderStatusView(2, this.local.coming)}
        {this.renderStatusView(3, this.local.arrived)}
        {this.renderStatusView(4, this.local.started)}
        {this.renderStatusView(5, this.local.accomplish, true)}
      </View>
    );
  }
}

const mapStateToProsp = ({ service, auth }) => {
  return {
    service,
    auth
  }
}

export default connect(mapStateToProsp)(ServiceStatusProgressView)
