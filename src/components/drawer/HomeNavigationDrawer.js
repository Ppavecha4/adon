import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    ScrollView,
    Platform,
    StyleSheet,
    Switch,
    TouchableOpacity
} from 'react-native'
import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-ionicons'
import { connect } from 'react-redux'
import { logOut } from '../../store/actions/index'
import {
    colorDarkText,
    colorPrimary
} from '../../style/colors'
import axios from '../../Server/DefaultServer'
import ProgressDialog from '../dialogs/ProgressDialog'
import PushNotification from '../../tasks/pushNotificationTask'
import { locals } from '../../utils/locals'
import { defaultAvatar } from '../../utils/constants'
import { fetchReservationlist } from '../../store/actions/services'
class HomeNavigationDrawer extends Component {
    state = {
        isUserOnline: false,
        loading: false,
        newRequest:0
    }

    constructor(props) {
        super(props)
        console.log("Locals",locals)
        this.local = locals[this.props.local].navDrawer
        const {
            home,
            chats,
            help,
            history,
            logout,
            settings,
            professionals,
            reserve,
            upgrade
        } = this.local
        this.navItems = [
            {
                title: professionals,
                icon: 'ios-people'
            },
            {
                title: chats,
                icon: 'ios-chatbubbles'
            },
            {
                title: history,
                icon: 'ios-undo'
            },
             {
                title: reserve,
                icon: 'ios-list-box'
            },
            {
                title: upgrade,
                icon: 'ios-unlock'
            },
            {
                title: help,
                icon: 'ios-help-circle'
            },
            {
                title: logout,
                icon: 'ios-exit'
            }
        ]
    }

    componentDidMount = () => {

   
        if (this.props.userType !== 'user') {
            this.initState()
        }
        this.props.fetchReservationlist()

         console.log("Component diid Moubnt ")

      
        
    }
     componentWillUnmount(){
      
       console.log("Component will unmount ")
         
     }

    initState = async () => {


        try {
            const {
                id,
                token
            } = this.props.user
            const response = await axios.get(`/provider/checkstate?id=${id}&token=${token}`)
            console.log('Response of provider state', response)
              this.checkRequest()
            if (response.data.success) {

             console.log("I am step 1")

                if (response.data.is_active === 1) {

                    console.log("I am step 2")
                    PushNotification.start(id)

                }else {

                    console.log("I am step 3")
                    PushNotification.stop()

                }
                this.setState({
                    isUserOnline: response.data.is_active === 1
                })
            }
        }catch(err) {
            console.log("I am step 3")
            console.log("Error in init state", err)
        }
    }

    toggleState = async () => {

       console.log("");

        try {
            this.setState(prevState => {
                return ({
                    isUserOnline: !prevState.isUserOnline
                })
            })
            const {
                id,
                token
            } = this.props.user
            const response = await axios.post(`/provider/togglestate?id=${id}&token=${token}`)
            console.log('Response of toggle state', response)
            if (response.data.success) {
                this.initState()
            }
        }catch(err) {
            console.log('Error in toggle state', err) 
        }
    }

    navigateToScreen = (route) => {
        console.log(route)
        if (route === this.local.logout) {
            if (Platform.OS === 'android') {
                PushNotification.stop()
            }
            this.props.logOut()
            return
        }else if (route === this.local.chats) {
            const navigateAction = NavigationActions.navigate({
                routeName: 'ChatHistory'
            })
            this.props.navigation.dispatch(navigateAction)
            return
        }else if (route === this.local.history) {
            const navigateAction = NavigationActions.navigate({
                routeName: 'History'
            })
            this.props.navigation.dispatch(navigateAction)
            return
        }
        else if (route === this.local.reserve) {
            const navigateAction = NavigationActions.navigate({
                routeName: 'Reservation'
            })
            this.props.navigation.dispatch(navigateAction)
            return
        }

        else if (route === this.local.home) {
            const navigateAction = NavigationActions.navigate({
                routeName: this.props.userType === 'user' ? 'Home' : 'HomeProvider'
            })
            this.props.navigation.dispatch(navigateAction)
            return
        }else if (route === this.local.upgrade) {
            const navigateAction = NavigationActions.navigate({
                routeName: 'Upgrade'
            })
            this.props.navigation.dispatch(navigateAction)
            return
        }else if (route === this.local.professionals) {
            const navigateAction = NavigationActions.navigate({
                routeName: 'Professionals'
            })
            this.props.navigation.dispatch(navigateAction)
            return
        }else if (route === 'EditProfile') {
            const navigateAction = NavigationActions.navigate({
                routeName: 'EditProfile'
            })
            this.props.navigation.dispatch(navigateAction)
            return
        }else if (route === this.local.settings) {
            const navigateAction = NavigationActions.navigate({
                routeName: 'Settings'
            })
            this.props.navigation.dispatch(navigateAction)
            return
        }
        const navigateAction = NavigationActions.navigate({
            routeName: route
        })
    }

      checkRequest = async () => {

           
             const {
                id
                
            } = this.props.user

             if(this.props.userType !== 'user'){

              const result = await axios.get(`/user/reservationlist/get/${id}`)

              const data = result.data.data.filter(item => item.viewstatus == 0)

               this.setState ({
                    newRequest : data.length, 
                })  
            }
               //console.log('Request Complete Paid', await axios.get(`/user/reservationlist/get/${id}`))
          }  

    


    render() {
        const { user } = this.props
        if (!user) {
            return (
                <View>
                    <ProgressDialog title="Logging out" show />
                </View>
            )
        }
        return (
            <ScrollView>
                <ProgressDialog title="Please Wait" show={this.state.loading} />
                  
                <View style={{ flex: 1, backgroundColor: '#fff' }}>
                    <View style={styles.profileContainer}>
                        <Image style={styles.profilePic} source={{ uri: user.state !== '' ? user.state : defaultAvatar }} />
                        <Text style={styles.profileName}>{user ? `${user.first_name} ${user.last_name}` : 'Username'}</Text>
                        <TouchableOpacity style={{
                            width: '100%',
                            height: 30,
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'row'
                        }} 
                        onPress={() => {
                            this.navigateToScreen('EditProfile')
                        }}>
                            <Icon name='ios-create' />
                            <Text style={styles.text}>{this.local.editProfile}</Text>
                        </TouchableOpacity>
                        {this.props.userType !== 'user' ? <View style={{
                            marginTop: 10
                        }}>
                            <Text>{this.state.isUserOnline ? "Online" : "Offline"}</Text>
                            <Switch value={this.state.isUserOnline} onValueChange={(value) => this.toggleState()} />
                        </View> : null}
                    </View>
                    <View style={{ flex: 5 }} showsHorizontalScrollIndicator={false}>
                        {this.navItems.map((item, index) => {
                            let style = {
                                ...styles.listTile,
                            }
                            if (item.icon === 'ios-unlock' && this.props.userType === 'user') {
                                return null
                            }
                            return (
                                <TouchableOpacity
                                    key={index}
                                    onPress={() => {
                                        this.navigateToScreen(item.title)
                                        this.checkRequest()
                                    }}>
                                    <View style={style}>
                                        <Icon name={item.icon} />
                                        <Text style={styles.text}>{item.title}</Text>
                                       
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                     { this.state.newRequest > 0 && this.props.userType !== "user" ? <View style={styles.redlight }></View> :<Text></Text>   }
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = ({ auth }) => {
    return auth
}


const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => dispatch(logOut()),
        fetchReservationlist: () => dispatch(fetchReservationlist())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeNavigationDrawer)

const styles = StyleSheet.create({
    profileContainer: {
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0, 0, 0, .2)',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 30,
        paddingBottom: 10
    },
    profilePic: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 1
    },
    profileName: {
        fontSize: 30,
        color: colorPrimary,
        letterSpacing: 3,
        textAlign: 'center'
    },
    text: {
        fontSize: 15,
        color: colorDarkText,
        letterSpacing: 1,
        textAlign: 'left',
        marginLeft: 10,
    },
    listTile: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingHorizontal: 30,
        paddingVertical: 20
    },
    redlight:{
        backgroundColor: 'red',
        height:8,
        width:8,
        borderRadius:50,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: 'red',
        shadowOffset: { height: 0, width: 0 },
        position: 'absolute',
        top:510,
        right:20,

    }
})

