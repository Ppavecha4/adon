import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { colorWhite, colorDarkText, colorDark } from '../../style/colors'
import { AirbnbRating } from 'react-native-ratings'
import { Button } from 'native-base'
import {connect} from 'react-redux'
import { defaultAvatar } from '../../utils/constants'

const ProviderDetails = (props) => (
    <View style={styles.container}>
        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center', width: '100%', borderBottomWidth: .5, borderBottomColor: 'rgba(0, 0, 0, .2)', paddingBottom: 5 }}>
            <Image style={styles.profileImg} source={{ uri: props.walker.state !== '' ? props.walker.state : defaultAvatar }} />
            <View style={{ flex: 1, paddingLeft: 10 }}>
                <Text style={styles.profileName}>{props.walker.first_name}</Text>
                <Text style={styles.phone}>{props.walker.phone}</Text>
            </View>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', width: '100%' }}>
            <AirbnbRating showRating defaultRating={props.walker.rating} size={20} isDisabled/>
            <Button style={{ backgroundColor: colorDark, alignSelf: 'center', borderRadius: 10 }} onPress={props.onChat}>
                <Text style={{ color: colorWhite, paddingHorizontal: 30 }}>CHAT</Text>
            </Button>
        </View>
    </View>
)

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 60,
        backgroundColor: colorWhite,
        borderRadius: 10,
        width: '95%',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-around',
        elevation: 5,
    },
    profileImg: {
        width: 70,
        height: 70,
        borderRadius: 100,
    },
    profileName: {
        fontSize: 25,
        color: colorDarkText
    },
    phone: {

    }
})

const mapStateToProps = ({service}) => {
    return service
}

export default connect(mapStateToProps)(ProviderDetails)
