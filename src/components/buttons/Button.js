import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { colorAccent, colorWhite } from '../../style/colors'

const Button = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress} >
            <View style={{...styles.button, backgroundColor: props.bgColor, ...props.style}}>
                <Text style={styles.btnText}>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    button: {
        paddingVertical: 12,
        paddingHorizontal: 20,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colorAccent,
        color: colorWhite
    },
    btnText: {
        color: colorWhite,
        letterSpacing: 2,
        fontSize: 18
    },
})

export default Button
