import React from 'react'
import { View, Text, TouchableOpacity} from 'react-native'
import { colorWhite, colorDark } from '../../style/colors'

const ActionButton = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress} style={{
            height: 50,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
            backgroundColor: colorDark,
            bottom: 0,
            paddingHorizontal: 15,
            borderRadius: 50,
            marginVertical: 5,
            ...props.style
        }}>
            <Text style={{
                    color: colorWhite,
                    fontSize: 18,
                    letterSpacing: 1
                }}>{props.title}</Text>
        </TouchableOpacity>
    )
}

export default ActionButton
