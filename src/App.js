import { createStackNavigator, createAppContainer } from 'react-navigation'
import HomeScreen from './screens/Home'
import SplashScreen from './screens/Splash'
import RegisterScreen from './screens/Register'
import LoginScreen from './screens/Login'
import EntryRoute from './screens/EntryRoute'
import HomeProvider from './screens/HomeProvider'
import InvoiceScreen from './screens/Invoice'
import ReviewScreen from './screens/Review'
import ChatScreen from './screens/Chat'
import History from './screens/History'
import ProfessionalsScreen from './screens/Professionals'
import ChatHistoryScreen from './screens/ChatHistory'
import EditProfileScreen from './screens/EditProfile'
import UpgradeScreen from './screens/Upgrade'
import SettingsScreen from './screens/Settings'
import NotFoundScreen from './screens/404'
import Reservation from './screens/Reservation'

console.disableYellowBox = true
window.navigator.userAgent = 'ReactNative'

const MainNavigator = createStackNavigator({
  Splash: { screen: SplashScreen },
  EntryRoute: { screen: EntryRoute },
  Login: { screen: LoginScreen },
  Register: { screen: RegisterScreen },
  Home: { screen: HomeScreen },
  HomeProvider: { screen: HomeProvider },
  Chat: { screen: ChatScreen },
  ChatHistory: { screen: ChatHistoryScreen },
  Review: { screen: ReviewScreen },
  Invoice: { screen: InvoiceScreen },
  History: { screen: History },
  Professionals: { screen: ProfessionalsScreen },
  EditProfile: { screen: EditProfileScreen },
  NotFound: { screen: NotFoundScreen },
  Upgrade: { screen: UpgradeScreen },
  Settings: { screen: SettingsScreen },
  Reservation: { screen: Reservation }
}, {
    headerMode: 'none',
    // initialRouteName: 'Login'
  });

const App = createAppContainer(MainNavigator)

export default App
