import enLocals from './locals/en'
import esLocals from './locals/es'
import ptLocals from './locals/pt'
export const locals = {
    English: enLocals, // English Language
    Español: esLocals, // Spanish Language
    Portuguesa: ptLocals, // German Language
    Languages: [
        'English',
        'Español',
        'Portuguesa'
    ]
}
