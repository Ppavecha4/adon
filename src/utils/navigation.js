import { StackActions, NavigationActions } from 'react-navigation'

/**
 * This const holds the action for navigating user to home screen
 */
export const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Home' })]
})

export const providerAuthSuccess = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({routeName: 'HomeProvider'})]
})

export const providerRegistrationSuccess = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({routeName: 'Login'})]
})

export const logOutAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'EntryRoute'})]
})

export const notAllowedAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'NotFound'})]
})
