
const requestTime = new Date().setTime(Date.now() + (30 * 1000))
const timeOut = (requestTime - Date.now()) / 1000

/**
 * @param {Object} request - the request object
 * @return {boolean} - return after checking the request is expired or not
 */
const isRequestExpired = (request) => request.timeOut < Date.now()
/**
 * Checking the remaining time
 * @param {Object} request - request object
 * @return {number} - return the remaining time in seconds
 */
const getRequestRemainingTime = (request) => Math.floor(((request.timeOut - Date.now()) / 1000))


console.log(timeOut)
setInterval(() => {
    console.log("isExpired", requestTime < Date.now())
}, 1000)

