import React from 'react'
import Button from '../components/buttons/Button'
import { LoginButton, GraphRequest, GraphRequestManager, AccessToken, LoginManager } from 'react-native-fbsdk'


class FacebookAuthService {
    constructor() {
        this.requestManager = new GraphRequestManager()
    }

    makeLoginButton(callback) {
        return (
            <LoginButton
                readPermissions={["public_profile"]}
                onLoginFinished={(error, result) => {
                    if (error) {
                        console.log('Error facebookAuthService', error)
                    } else if (result.isCancelled) {
                        console.log('Login Cancel')
                    } else {
                        AccessToken.getCurrentAccessToken()
                            .then((data) => {
                                callback(data.accessToken)
                            })
                            .catch(error => {
                                console.log(error)
                            })
                    }
                }} />
            // <Button
            //     title='Continue with Facebook'
            //     bgColor='#4267B2'
            //     onPress={() => {
                    
            //     }} />
        )
    }

    makeLogoutButton(callback) {
        return (
            <LoginButton onLogoutFinished={() => {
                callback()
            }} />
        )
    }

    async fetchProfile(callback) {
        return new Promise((resolve, reject) => {
            const request = new GraphRequest(
                '/me',
                null,
                (error, result) => {
                    if (result) {
                        const profile = result
                        profile.avatar = `https://graph.facebook.com/${result.id}/picture`
                        resolve(profile)
                    } else {
                        reject(error)
                    }
                }
            )

            this.requestManager.addRequest(request).start()
        })
    }
}

export const facebookService = new FacebookAuthService()
