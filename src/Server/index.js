
const mode = 'prod'

export const APP_SERVER_BASE_URL = mode === 'prod' ? 'https://app-server-map.herokuapp.com/' : 'http://192.168.1.7:3000'
